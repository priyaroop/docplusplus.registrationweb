﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects;

//using System.Data.Objects;

namespace Onactuate.Common.DataAccess
{
    public interface IObjectContextStorage
    {
        IEnumerable<ObjectContext> GetAllObjectContexts();
        ObjectContext GetObjectContextForKey(string key);
        void SetObjectContextForKey(string key, ObjectContext objectContext);
    }
}

