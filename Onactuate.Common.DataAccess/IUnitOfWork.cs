﻿using System;
using System.Data;
using System.Data.Entity.Core.Objects;
//using System.Data.Objects;

namespace Onactuate.Common.DataAccess
{
    public interface IUnitOfWork : IDisposable
    {
        void BeginTransaction();
        void BeginTransaction(IsolationLevel isolationLevel);
        void CommitTransaction();
        void RollBackTransaction();
        void SaveChanges();
        void SaveChanges(SaveOptions saveOptions);

        bool IsInTransaction { get; }
    }
}

