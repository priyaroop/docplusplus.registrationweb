﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Onactuate.Common.DataAccess
{
    public static class DomainExtensionMethods
    {
        public static PagingResultset<T> GetPagedResultset<T>(this IQueryable<T> source, PagingParameters<T> pagingParameters) where T : class
        {
            if (((!string.IsNullOrEmpty(pagingParameters.QueryType) && !string.IsNullOrEmpty(pagingParameters.QueryOperator.ToString())) && !string.IsNullOrEmpty(pagingParameters.Query)) && (source != null))
            {
                source = source.Where<T>(pagingParameters.QueryType, pagingParameters.QueryOperator.ToString(), pagingParameters.Query);
            }
            if (!(string.IsNullOrEmpty(pagingParameters.SortBy) || string.IsNullOrEmpty(pagingParameters.SortOrder.ToString())))
            {
                source = source.OrderBy<T>(pagingParameters.SortBy, pagingParameters.SortOrder.Equals(SortOrder.Ascending));
            }
            else if (source != null)
            {
                source = Queryable.OrderBy<T, bool>(source, pagingParameters.DefaultSortOrder);
            }
            int totalRecords = source.Count<T>();
            PagingResultset<T> resultset = new PagingResultset<T>(null, 0);
            if (source != null)
            {
                resultset = new PagingResultset<T>(source.Skip<T>(pagingParameters.SkipRecords).Take<T>(pagingParameters.TakeRecords), totalRecords);
            }
            return resultset;
        }

        public static IQueryable<T> Like<T>(this IQueryable<T> source, string propertyName, string keyword)
        {
            Type type = typeof(T);
            PropertyInfo property = type.GetProperty(propertyName);
            ParameterExpression expression = Expression.Parameter(type, "p");
            MemberExpression expression2 = Expression.MakeMemberAccess(expression, property);
            ConstantExpression expression3 = Expression.Constant("%" + keyword + "%");
            Expression<System.Func<T, bool>> expression5 = Expression.Lambda<System.Func<T, bool>>(Expression.Call(null, typeof(Queryable).GetMethod("Contains", new Type[] { typeof(string), typeof(string) }), expression2, expression3), new ParameterExpression[] { expression });
            return Queryable.Where<T>(source, expression5);
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, string propertyName, bool asc)
        {
            Type type = typeof(T);
            string methodName = asc ? "OrderBy" : "OrderByDescending";
            PropertyInfo property = type.GetProperty(propertyName);
            ParameterExpression expression = Expression.Parameter(type, "p");
            LambdaExpression expression3 = Expression.Lambda(Expression.MakeMemberAccess(expression, property), new ParameterExpression[] { expression });
            MethodCallExpression expression4 = Expression.Call(typeof(Queryable), methodName, new Type[] { type, property.PropertyType }, new Expression[] { source.Expression, Expression.Quote(expression3) });
            return source.Provider.CreateQuery<T>(expression4);
        }

        public static IQueryable<TEntity> Where<TEntity>(this IQueryable<TEntity> source, string searchProperty, string searchOper, string searchString) where TEntity : class
        {
            PropertyInfo info2;
            Expression expression7;
            BinaryExpression expression8;
            Type type = typeof(TEntity);
            ConstantExpression right = Expression.Constant(searchString);
            ConstantExpression expression2 = null;
            ConstantExpression expression3 = null;
            ConstantExpression expression4 = Expression.Constant(StringComparison.InvariantCultureIgnoreCase);
            ParameterExpression expression = Expression.Parameter(type, "p");
            PropertyInfo property = type.GetProperty(searchProperty);
            Expression expression6 = Expression.MakeMemberAccess(expression, property);
            if (property.PropertyType == typeof(int?))
            {
                info2 = typeof(int?).GetProperty("Value");
                expression6 = Expression.MakeMemberAccess(expression6, info2);
                int? nullable = new int?(int.Parse(searchString));
                right = Expression.Constant(nullable);
            }
            if (property.PropertyType == typeof(decimal?))
            {
                info2 = typeof(decimal?).GetProperty("Value");
                expression6 = Expression.MakeMemberAccess(expression6, info2);
                decimal? nullable2 = new decimal?(decimal.Parse(searchString));
                right = Expression.Constant(nullable2);
            }
            if (expression6.Type == typeof(int))
            {
                right = Expression.Constant(int.Parse(searchString));
            }
            if (expression6.Type == typeof(decimal))
            {
                right = Expression.Constant(decimal.Parse(searchString));
            }
            if (expression6.Type == typeof(DateTimeOffset))
            {
                right = Expression.Constant(DateTimeOffset.Parse(searchString));
                if ((searchOper == "eq") || (searchOper == "ne"))
                {
                    searchOper = (searchOper == "eq") ? "gtAndLt" : "ltAndGt";
                    string format = "{0} 00:00:00 +00:00";
                    DateTimeOffset offset = DateTimeOffset.Parse(string.Format(format, searchString));
                    expression2 = Expression.Constant(DateTimeOffset.Parse(string.Format(format, offset.Date.ToString("yyyy-MM-dd"))));
                    expression3 = Expression.Constant(DateTimeOffset.Parse(string.Format(format, offset.Date.AddDays(1.0).ToString("yyyy-MM-dd"))));
                }
            }
            //MethodInfo method = typeof(string).GetMethod("StartsWith", new Type[] { typeof(string), typeof(StringComparison) });
            //MethodInfo info4 = typeof(string).GetMethod("EndsWith", new Type[] { typeof(string), typeof(StringComparison) });
            //MethodInfo info5 = typeof(string).GetMethod("Contains", new Type[] { typeof(string) });
            //MethodInfo info6 = typeof(string).GetMethod("Equals", new Type[] { typeof(string), typeof(string), typeof(StringComparison) });


            MethodInfo method = typeof(string).GetMethod("StartsWith", new Type[] { typeof(string) });
            MethodInfo info4 = typeof(string).GetMethod("EndsWith", new Type[] { typeof(string) });
            MethodInfo info5 = typeof(string).GetMethod("Contains", new Type[] { typeof(string) });
            MethodInfo info6 = typeof(string).GetMethod("Equals", new Type[] { typeof(string), typeof(string), typeof(StringComparison) });
            MethodInfo info8 = typeof(string).GetMethod("Equals", new Type[] { typeof(string), typeof(string) }); // String.Equals method with two string parameters
            MethodInfo info7 = (from mi in typeof(StringExtensions).GetMethods(BindingFlags.Public | BindingFlags.Static)
                                where mi.Name == "Contains"
                                select mi).FirstOrDefault<MethodInfo>();
            MethodInfo info9 = (from mi in typeof(StringExtensions).GetMethods(BindingFlags.Public | BindingFlags.Static)
                                where mi.Name == "Contains"
                                select mi).LastOrDefault();
            switch (searchOper)
            {
                case "ne":
                    if (!(property.PropertyType == typeof(string)))
                    {
                        expression7 = Expression.NotEqual(expression6, right);
                    }
                    else
                    {
                        //expression7 = Expression.Not(Expression.Call(info6, expression6, right, expression4));
                        expression7 = Expression.Not(Expression.Call(info8, expression6, right));
                    }
                    break;

                case "lt":
                    expression7 = Expression.LessThan(expression6, right);
                    break;

                case "le":
                    expression7 = Expression.LessThanOrEqual(expression6, right);
                    break;

                case "gt":
                    expression7 = Expression.GreaterThan(expression6, right);
                    break;

                case "ge":
                    expression7 = Expression.GreaterThanOrEqual(expression6, right);
                    break;

                case "gtAndLt":
                    expression7 = Expression.GreaterThan(expression6, expression2);
                    expression8 = Expression.LessThan(expression6, expression3);
                    expression7 = Expression.And(expression7, expression8);
                    break;

                case "ltAndGt":
                    expression7 = Expression.LessThan(expression6, expression2);
                    expression8 = Expression.GreaterThan(expression6, expression3);
                    expression7 = Expression.Or(expression7, expression8);
                    break;

                case "bw":
                    //expression7 = Expression.Call(expression6, method, right, expression4); //Original Code
                    expression7 = Expression.Call(expression6, method, right);
                    break;

                case "bn":
                    expression7 = Expression.Not(Expression.Call(expression6, method, right, expression4));
                    break;

                case "ew":
                    //expression7 = Expression.Call(expression6, info4, right, expression4);//ORiginal Code
                    expression7 = Expression.Call(expression6, info4, right);
                    break;

                case "en":
                    expression7 = Expression.Not(Expression.Call(expression6, info4, right, expression4));
                    break;

                case "cn":
                    if (!(property.PropertyType == typeof(string)))
                    {
                        expression7 = Expression.Call(expression6, info5, new Expression[] { right });

                    }
                    else
                    {
                        //ignoring the string comparision enum
                        //expression7 = Expression.Call(info7, expression6, right, expression4);
                        //test expression downward
                        List<TEntity> list = new List<TEntity>();
                        foreach (TEntity item in source)
                        {
                            if (property.GetValue(item, null).ToString().Contains(searchString, StringComparison.InvariantCultureIgnoreCase))
                            {
                                list.Add(item);
                            }
                        }

                        var compareResult = list.AsQueryable<TEntity>();
                        return compareResult;
                    }
                    break;

                case "nc":
                    if (!(property.PropertyType == typeof(string)))
                    {
                        expression7 = Expression.Not(Expression.Call(expression6, info5, new Expression[] { right }));
                    }
                    else
                    {
                        //expression7 = Expression.Not(Expression.Call(info7, expression6, right, expression4));
                        //expression7 = Expression.Not(Expression.Call(info5, expression6, right));
                        List<TEntity> list = new List<TEntity>();
                        foreach (TEntity item in source)
                        {
                            if (!property.GetValue(item, null).ToString().Contains(searchString, StringComparison.InvariantCultureIgnoreCase))
                            {
                                list.Add(item);
                            }
                        }

                        var compareResult = list.AsQueryable<TEntity>();
                        return compareResult;
                    }
                    break;

                default:
                    if (property.PropertyType == typeof(string))
                    {
                        //Note: info6 method was not working so it was replaced with info8 
                        //expression7 = Expression.Call(info6, expression6, right, expression4);
                        expression7 = Expression.Call(info8, expression6, right);
                    }
                    else
                    {
                        expression7 = Expression.Equal(expression6, right);
                    }
                    break;
            }
            LambdaExpression expression9 = Expression.Lambda(expression7, new ParameterExpression[] { expression });
            MethodCallExpression expression10 = Expression.Call(typeof(Queryable), "Where", new Type[] { source.ElementType }, new Expression[] { source.Expression, expression9 });
            var result = source.Provider.CreateQuery<TEntity>(expression10);
            return result;
        }
    }
}

