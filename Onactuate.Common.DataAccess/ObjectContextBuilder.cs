﻿using System;
using System.Configuration;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Reflection;
using Onactuate.Common.Logging;
using Onactuate.Common.Logging.Contracts;
using Onactuate.Common.Logging.LoggingContextProviders;

namespace Onactuate.Common.DataAccess
{
    public class ObjectContextBuilder<TEntity> : DbModelBuilder, IObjectContextBuilder<TEntity> where TEntity: ObjectContext
    {
        public readonly ConnectionStringSettings CnStringSettings;
        private DbCompiledModel _dbModel;
        private readonly DbProviderFactory _factory;
        private readonly bool _lazyLoadingEnabled;
        private readonly ILoggingService _loggingService;
        private readonly bool _recreateDatabaseIfExists;
        public static string ServiceName { get; } = "ObjectContextBuilder";
        private readonly object _syncLock;

        public ObjectContextBuilder(string connectionStringName, string[] mappingAssemblies, bool recreateDatabaseIfExists, bool lazyLoadingEnabled)
        {
            this._syncLock = new object();
#pragma warning disable 618
            Conventions.Remove<IncludeMetadataConvention>(); 
#pragma warning restore 618
            this.CnStringSettings = ConfigurationManager.ConnectionStrings[connectionStringName];
            this._factory = DbProviderFactories.GetFactory(this.CnStringSettings.ProviderName);
            this._recreateDatabaseIfExists = recreateDatabaseIfExists;
            this._lazyLoadingEnabled = lazyLoadingEnabled;
            this._loggingService = new LoggingService(new StaticLoggingContextProvider("ObjectContextBuilder"));
            this.AddConfigurations(mappingAssemblies);
        }

        private void AddConfigurations(string[] mappingAssemblies)
        {
            this._loggingService.Debug("ObjectContextBuilder", "AddConfigurations: " + mappingAssemblies);
            if ((mappingAssemblies == null) || (mappingAssemblies.Length == 0))
            {
                throw new ArgumentNullException("mappingAssemblies", "You must specify at least one mapping assembly");
            }
            bool flag = false;
            foreach (string str in mappingAssemblies)
            {
                string assemblyName = MakeLoadReadyAssemblyName(str);
                Assembly assembly = AppDomain.CurrentDomain.GetAssemblies().ToList<Assembly>().Find(a => a.ManifestModule.Name.ToLower() == assemblyName.ToLower());
                foreach (Type type in assembly.GetTypes())
                {
                    if (!type.IsAbstract && (type.BaseType.IsGenericType && (type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>))))
                    {
                        flag = true;
                        object obj2 = Activator.CreateInstance(type);
                        this.Configurations.Add((dynamic) obj2);
                    }
                }
            }
            if (!flag)
            {
                throw new ArgumentException("No mapping class found!");
            }
        }

        public TEntity BuildObjectContext()
        {
            DbConnection connection = this._factory.CreateConnection();
            connection.ConnectionString = this.CnStringSettings.ConnectionString;

            lock (this._syncLock)
            {
                if (this._dbModel == null)
                {
                    this._loggingService.Debug("BuildObjectContext", "ConnectionString: " + this.CnStringSettings.ConnectionString);
                    this._dbModel = this.Build(connection).Compile();
                }
            }

            ObjectContext context = _dbModel.CreateObjectContext<ObjectContext>(connection);
            context.ContextOptions.LazyLoadingEnabled = this._lazyLoadingEnabled;
            return (TEntity) context;
        }

        private static string MakeLoadReadyAssemblyName(string assemblyName)
        {
            return ((assemblyName.IndexOf(".dll") == -1) ? (assemblyName.Trim() + ".dll") : assemblyName.Trim());
        }
    }
}

