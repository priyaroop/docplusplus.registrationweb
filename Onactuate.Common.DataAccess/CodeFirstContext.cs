﻿using System.Data.Entity;
using System.Text;

namespace Onactuate.Common.DataAccess
{
    public class CodeFirstContext: DbContext   
    {  
        public CodeFirstContext(string connectionString): base(connectionString)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<CodeFirstContext>());
        }

        public void CreateDatabaseUser(string newUser, string newPassword)
        {
            string databaseName = base.Database.Connection.Database;

            StringBuilder sql = new StringBuilder();
            sql.AppendLine($"CREATE LOGIN [{newUser}] WITH PASSWORD=N'{newPassword}', DEFAULT_DATABASE=[{databaseName}], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=ON");
            sql.AppendLine($"CREATE USER [{newUser}] FOR LOGIN [{newUser}]");
            sql.AppendLine($"ALTER AUTHORIZATION ON SCHEMA::[db_datawriter] TO [{newUser}]");
            sql.AppendLine($"ALTER AUTHORIZATION ON SCHEMA::[db_datareader] TO [{newUser}]");
            sql.AppendLine($"EXEC sp_addrolemember N'db_datawriter', N'{newUser}'");
            sql.AppendLine($"EXEC sp_addrolemember N'db_datareader', N'{newUser}'");

            base.Database.ExecuteSqlCommand(sql.ToString());

            sql = new StringBuilder();
            sql.AppendLine($"GRANT EXECUTE TO [{newUser}]");
            base.Database.ExecuteSqlCommand(sql.ToString());
        }

        public void RunSql(string sql)
        {
            base.Database.ExecuteSqlCommand(sql);
        }
    } 
}