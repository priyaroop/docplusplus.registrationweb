﻿using System;

namespace Onactuate.Common.DataAccess
{
    public static class StringExtensions
    {
        public static bool Contains(this string a, string b, StringComparison comparisonType)
        {
            return (a.IndexOf(b, comparisonType) >= 0);
        }
    }
}

