﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Onactuate.Common.DataAccess.LinqKit
{
    public static class Extensions
    {
        public static IQueryable<T> AsExpandable<T>(this IQueryable<T> query)
        {
            if (query is ExpandableQuery<T>)
            {
                return (ExpandableQuery<T>) query;
            }
            return new ExpandableQuery<T>(query);
        }

        public static Expression Expand(this Expression expr)
        {
            return new ExpressionExpander().Visit(expr);
        }

        public static Expression<TDelegate> Expand<TDelegate>(this Expression<TDelegate> expr)
        {
            return (Expression<TDelegate>) new ExpressionExpander().Visit(expr);
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (T local in source)
            {
                action(local);
            }
        }

        public static TResult Invoke<TResult>(this Expression<System.Func<TResult>> expr)
        {
            return expr.Compile()();
        }

        public static TResult Invoke<T1, TResult>(this Expression<System.Func<T1, TResult>> expr, T1 arg1)
        {
            return expr.Compile()(arg1);
        }

        public static TResult Invoke<T1, T2, TResult>(this Expression<System.Func<T1, T2, TResult>> expr, T1 arg1, T2 arg2)
        {
            return expr.Compile()(arg1, arg2);
        }

        public static TResult Invoke<T1, T2, T3, TResult>(this Expression<System.Func<T1, T2, T3, TResult>> expr, T1 arg1, T2 arg2, T3 arg3)
        {
            return expr.Compile()(arg1, arg2, arg3);
        }

        public static TResult Invoke<T1, T2, T3, T4, TResult>(this Expression<System.Func<T1, T2, T3, T4, TResult>> expr, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            return expr.Compile()(arg1, arg2, arg3, arg4);
        }
    }
}

