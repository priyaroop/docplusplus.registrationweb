﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Core.Objects;
//using System.Data.Objects;

namespace Onactuate.Common.DataAccess
{
    internal class UnitOfWork : IUnitOfWork, IDisposable
    {
        private bool _disposed;
        private ObjectContext _objectContext;
        private DbTransaction _transaction;

        public UnitOfWork(ObjectContext context)
        {
            this._objectContext = context;
        }

        public void BeginTransaction()
        {
            this.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            if (this._transaction != null)
            {
                throw new ApplicationException("Cannot begin a new transaction while an existing transaction is still running. Please commit or rollback the existing transaction before starting a new one.");
            }
            this.OpenConnection();
            this._transaction = this._objectContext.Connection.BeginTransaction(isolationLevel);
        }

        public void CommitTransaction()
        {
            if (this._transaction == null)
            {
                throw new ApplicationException("Cannot roll back a transaction while there is no transaction running.");
            }
            try
            {
                this._objectContext.SaveChanges();
                this._transaction.Commit();
            }
            catch
            {
                this._transaction.Rollback();
                throw;
            }
            finally
            {
                this.ReleaseCurrentTransaction();
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing && !this._disposed)
            {
                this.ReleaseCurrentTransaction();
                this._disposed = true;
            }
        }

        private void OpenConnection()
        {
            if (this._objectContext.Connection.State != ConnectionState.Open)
            {
                this._objectContext.Connection.Open();
            }
        }

        private void ReleaseCurrentTransaction()
        {
            if (this._transaction != null)
            {
                this._transaction.Dispose();
                this._transaction = null;
            }
        }

        public void RollBackTransaction()
        {
            if (this._transaction == null)
            {
                throw new ApplicationException("Cannot roll back a transaction while there is no transaction running.");
            }
            try
            {
                this._transaction.Rollback();
            }
            catch
            {
                throw;
            }
            finally
            {
                this.ReleaseCurrentTransaction();
            }
        }

        public void SaveChanges()
        {
            if (this.IsInTransaction)
            {
                throw new ApplicationException("A transaction is running. Call BeginTransaction instead.");
            }
            this._objectContext.SaveChanges();
        }

        public void SaveChanges(SaveOptions saveOptions)
        {
            if (this.IsInTransaction)
            {
                throw new ApplicationException("A transaction is running. Call BeginTransaction instead.");
            }
            this._objectContext.SaveChanges(saveOptions);
        }

        public bool IsInTransaction
        {
            get
            {
                return (this._transaction != null);
            }
        }
    }
}

