﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
//using System.Data.Entity.Core;
//using System.Data.Entity.Core.Objects;
using System.Data.Entity.Design.PluralizationServices;
//using System.Data.Objects;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Onactuate.Common.Logging;
using Onactuate.Common.Logging.Contracts;
using Onactuate.Common.Logging.LoggingContextProviders;

//using System.Data.Entity.Core;
//using System.Data.Entity.Core.Objects;

namespace Onactuate.Common.DataAccess
{
    public class RepositoryBase : IRepository, IDisposable
    {
        private readonly string _connectionStringName;
        private readonly ILoggingService _loggingService;
        private readonly PluralizationService _pluralizer;

        //private const string connStrHardCoded = @"Initial Catalog=OnactuateQuizDb;data source=Onactuate059\LUCIFER;USER ID=webportal;PASSWORD=webportal!;;MultipleActiveResultSets=true;";

        public RepositoryBase() : this(string.Empty)
        {
        }

        protected RepositoryBase(ObjectContext objectContext)
        {
            this._pluralizer = PluralizationService.CreateService(CultureInfo.GetCultureInfo("en"));
            this._loggingService = new LoggingService(new StaticLoggingContextProvider("RepositoryBase"));
            if (objectContext == null)
            {
                throw new ArgumentNullException("objectContext");
            }
        }

        private RepositoryBase(string connectionStringName)
        {
            this._pluralizer = PluralizationService.CreateService(CultureInfo.GetCultureInfo("en"));
            this._loggingService = new LoggingService(new StaticLoggingContextProvider("RepositoryBase"));
            this._connectionStringName = connectionStringName;
        }

        public string ConnectionString
        {
            get
            {
                string rawConnectionString = this.ObjectContext.Connection.ConnectionString;
                int startIndex = rawConnectionString.IndexOf("Initial Catalog", StringComparison.Ordinal);
                int endIndex = rawConnectionString.Length - startIndex - 2;
                string connStr = rawConnectionString.Substring(startIndex, endIndex);
                return connStr;
            }
        }

        public void Attach<TEntity>(TEntity entity) where TEntity: class
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                this.ObjectContext.AttachTo(this.GetEntityName<TEntity>(), entity);
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
        }

        public int Count<TEntity>() where TEntity: class
        {
            int num;
            try
            {
                num = this.GetQuery<TEntity>().Count<TEntity>();
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
            return num;
        }

        public int Count<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity: class
        {
            int num;
            try
            {
                num = Queryable.Count<TEntity>(this.GetQuery<TEntity>(), criteria);
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
            return num;
        }

        protected void Delete<TEntity>(IEnumerable<TEntity> entities) where TEntity: class
        {
            try
            {
                if (entities == null)
                {
                    throw new ArgumentNullException("entities");
                }
                foreach (TEntity local in entities)
                {
                    this.ObjectContext.DeleteObject(local);
                }
                this.ObjectContext.SaveChanges();
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
        }

        public void Delete<TEntity>(TEntity entity) where TEntity: class
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                this.ObjectContext.DeleteObject(entity);
                this.ObjectContext.SaveChanges();
            }
            catch (Exception exception)
            {
                for (Exception exception2 = exception; exception2 != null; exception2 = exception2.InnerException)
                {
                    this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception2, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                }
                throw;
            }
        }

        public void Delete<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity: class
        {
            try
            {
                IEnumerable<TEntity> enumerable = this.Find<TEntity>(criteria);
                foreach (TEntity local in enumerable)
                {
                    
                    this.ObjectContext.DeleteObject(local);
                }
                this.ObjectContext.SaveChanges();
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
        }

        public IList<TEntity> Find<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity: class
        {
            IList<TEntity> list;
            try
            {
                list = Queryable.Where<TEntity>(this.GetQuery<TEntity>(), criteria).ToList<TEntity>();
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
            return list;
        }

        public TEntity FindOne<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity: class
        {
            TEntity local;
            try
            {
                local = Queryable.Where<TEntity>(this.GetQuery<TEntity>(), criteria).FirstOrDefault<TEntity>();
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
            return local;
        }

        public TEntity First<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity: class
        {
            TEntity local;
            try
            {
                local = Queryable.FirstOrDefault<TEntity>(this.GetQuery<TEntity>(), predicate);
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
            return local;
        }

        public IList<TEntity> Get<TEntity>(Expression<Func<TEntity, string>> orderBy, int pageIndex, int pageSize, SortOrder sortOrder = 0) where TEntity: class
        {
            IList<TEntity> list;
            try
            {
                if (sortOrder == SortOrder.Ascending)
                {
                    return Queryable.OrderBy<TEntity, string>(this.GetQuery<TEntity>(), orderBy).Skip<TEntity>(pageIndex).Take<TEntity>(pageSize).ToList<TEntity>();
                }
                list = Queryable.OrderByDescending<TEntity, string>(this.GetQuery<TEntity>(), orderBy).Skip<TEntity>(pageIndex).Take<TEntity>(pageSize).ToList<TEntity>();
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
            return list;
        }

        public IList<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, string>> orderBy, int pageIndex, int pageSize, SortOrder sortOrder = 0) where TEntity: class
        {
            IList<TEntity> list;
            try
            {
                if (sortOrder == SortOrder.Ascending)
                {
                    return Queryable.OrderBy<TEntity, string>(Queryable.Where<TEntity>(this.GetQuery<TEntity>(), predicate), orderBy).Skip<TEntity>(pageIndex).Take<TEntity>(pageSize).ToList<TEntity>();
                }
                list = Queryable.OrderByDescending<TEntity, string>(Queryable.Where<TEntity>(this.GetQuery<TEntity>(), predicate), orderBy).Skip<TEntity>(pageIndex).Take<TEntity>(pageSize).ToList<TEntity>();
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
            return list;
        }

        public IList<TEntity> GetAll<TEntity>() where TEntity: class
        {
            IList<TEntity> list;
            try
            {
                list = this.GetQuery<TEntity>().ToList<TEntity>();
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
            return list;
        }

        public TEntity GetByKey<TEntity>(object keyValue) where TEntity: class
        {
            throw new NotImplementedException("Not implemented yet");
            //TEntity local;
            //try
            //{
            //    object obj2;
            //    var entityKey = this.GetEntityKey<TEntity>(keyValue);
            //    if (this.ObjectContext.TryGetObjectByKey(entityKey, out obj2))
            //    {
            //        return (TEntity) obj2;
            //    }
            //    local = default(TEntity);
            //}
            //catch (Exception exception)
            //{
            //    this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
            //    throw;
            //}
            //return local;
        }

        private EntityKey GetEntityKey<TEntity>(object keyValue) where TEntity: class
        {
            string entityName = this.GetEntityName<TEntity>();
            string keyName = this.ObjectContext.CreateObjectSet<TEntity>().EntitySet.ElementType.KeyMembers[0].ToString();
            return new EntityKey(entityName, new EntityKeyMember[] { new EntityKeyMember(keyName, keyValue) });
        }

        private string GetEntityName<TEntity>() where TEntity: class
        {
            return string.Format("{0}.{1}", this.ObjectContext.DefaultContainerName, this._pluralizer.Pluralize(typeof(TEntity).Name));
        }

        protected ObjectResult<TEntity> GetList<TEntity>(string sqlQuery) where TEntity: class
        {
            ObjectResult<TEntity> result;
            try
            {
                result = this.ObjectContext.ExecuteStoreQuery<TEntity>(sqlQuery, new object[0]);
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
            return result;
        }

        public PagingResultset<TEntity> GetPagedResultset<TEntity>(PagingParameters<TEntity> pagingParameters) where TEntity: class
        {
            IQueryable<TEntity> source = (pagingParameters.DefaultFilterExpression != null) ? Queryable.Where<TEntity>(this.GetQuery<TEntity>(), pagingParameters.DefaultFilterExpression) : this.GetQuery<TEntity>();
            return source.AsQueryable<TEntity>().GetPagedResultset<TEntity>(pagingParameters);
        }

        public IQueryable<TEntity> GetQuery<TEntity>() where TEntity: class
        {
            IQueryable<TEntity> queryable;
            try
            {
                string entityName = this.GetEntityName<TEntity>();
                queryable = this.ObjectContext.CreateQuery<TEntity>(entityName, new ObjectParameter[0]);
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
            return queryable;
        }

        public IQueryable<TEntity> GetQuery<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity: class
        {
            IQueryable<TEntity> queryable;
            try
            {
                queryable = Queryable.Where<TEntity>(this.GetQuery<TEntity>(), predicate);
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
            return queryable;
        }

        public void SaveChanges<TEntity>(TEntity entity) where TEntity: class
        {
            try
            {
                this.ObjectContext.AddObject(this.GetEntityName<TEntity>(), entity);
                this.ObjectContext.SaveChanges();
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
        }

        protected void SaveChanges<TEntity>(IEnumerable<TEntity> tEntities) where TEntity: class
        {
            try
            {
                foreach (TEntity local in tEntities)
                {
                    this.ObjectContext.AddObject(this.GetEntityName<TEntity>(), local);
                }
                this.ObjectContext.SaveChanges();
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
        }

        public TEntity Single<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity: class
        {
            TEntity local;
            try
            {
                local = Queryable.SingleOrDefault<TEntity>(this.GetQuery<TEntity>(), criteria);
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
            return local;
        }

        public void Update<TEntity>(TEntity entity) where TEntity: class
        {
            try
            {
                object obj2;
                string entityName = this.GetEntityName<TEntity>();
                var key = this.ObjectContext.CreateEntityKey(entityName, entity);
                if (this.ObjectContext.TryGetObjectByKey(key, out obj2))
                {
                    this.ObjectContext.ApplyCurrentValues<TEntity>(key.EntitySetName, entity);
                }
                this.ObjectContext.SaveChanges();
            }
            catch (Exception exception)
            {
                this._loggingService.Error(typeof(TEntity).Name, LoggingMessageHelper.SerializeException(exception, string.Format("SQL Error: {0} failed for {1}", MethodBase.GetCurrentMethod(), typeof(TEntity).Name)));
                throw;
            }
        }

        public ObjectContext ObjectContext
        {
            get
            {
                return (string.IsNullOrEmpty(this._connectionStringName) ? ObjectContextManager.Current : ObjectContextManager.CurrentFor(this._connectionStringName));
            }
        }
    }
}

