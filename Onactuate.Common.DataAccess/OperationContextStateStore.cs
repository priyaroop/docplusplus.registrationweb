﻿using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;

namespace Onactuate.Common.DataAccess
{
    public class OperationContextStateStore : IExtension<OperationContext>
    {
        public OperationContextStateStore()
        {
            this.State = new Dictionary<string, object>();
        }

        public void Attach(OperationContext owner)
        {
        }

        public void Detach(OperationContext owner)
        {
        }

        public IDictionary State { get; private set; }
    }
}

