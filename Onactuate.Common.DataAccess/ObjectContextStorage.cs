﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects;

//using System.Data.Objects;

namespace Onactuate.Common.DataAccess
{
    public class ObjectContextStorage : IObjectContextStorage
    {
        private Dictionary<string, ObjectContext> storage = new Dictionary<string, ObjectContext>();

        public IEnumerable<ObjectContext> GetAllObjectContexts()
        {
            return this.storage.Values;
        }

        public ObjectContext GetObjectContextForKey(string key)
        {
            ObjectContext context;
            if (!this.storage.TryGetValue(key, out context))
            {
                return null;
            }
            return context;
        }

        public void SetObjectContextForKey(string key, ObjectContext objectContext)
        {
            this.storage.Add(key, objectContext);
        }
    }
}

