﻿using System.Data.Entity.Core.Objects;
//using System.Data.Objects;

namespace Onactuate.Common.DataAccess
{
    public interface IObjectContextBuilder<TEntity> where TEntity: ObjectContext
    {
        TEntity BuildObjectContext();
    }
}

