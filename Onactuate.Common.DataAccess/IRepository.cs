﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Onactuate.Common.DataAccess
{
    public interface IRepository : IDisposable
    {
        string ConnectionString { get; }
        void Attach<TEntity>(TEntity entity) where TEntity: class;
        int Count<TEntity>() where TEntity: class;
        int Count<TEntity>(Expression<System.Func<TEntity, bool>> criteria) where TEntity: class;
        void Delete<TEntity>(TEntity entity) where TEntity: class;
        void Delete<TEntity>(Expression<System.Func<TEntity, bool>> criteria) where TEntity: class;
        IList<TEntity> Find<TEntity>(Expression<System.Func<TEntity, bool>> criteria) where TEntity: class;
        TEntity FindOne<TEntity>(Expression<System.Func<TEntity, bool>> criteria) where TEntity: class;
        TEntity First<TEntity>(Expression<System.Func<TEntity, bool>> predicate) where TEntity: class;
        IList<TEntity> Get<TEntity>(Expression<System.Func<TEntity, string>> orderBy, int pageIndex, int pageSize, SortOrder sortOrder = 0) where TEntity: class;
        IList<TEntity> Get<TEntity>(Expression<System.Func<TEntity, bool>> criteria, Expression<System.Func<TEntity, string>> orderBy, int pageIndex, int pageSize, SortOrder sortOrder = 0) where TEntity: class;
        IList<TEntity> GetAll<TEntity>() where TEntity: class;
        TEntity GetByKey<TEntity>(object keyValue) where TEntity: class;
        PagingResultset<TEntity> GetPagedResultset<TEntity>(PagingParameters<TEntity> pagingParameters) where TEntity: class;
        IQueryable<TEntity> GetQuery<TEntity>() where TEntity: class;
        IQueryable<TEntity> GetQuery<TEntity>(Expression<System.Func<TEntity, bool>> predicate) where TEntity: class;
        void SaveChanges<TEntity>(TEntity entity) where TEntity: class;
        TEntity Single<TEntity>(Expression<System.Func<TEntity, bool>> criteria) where TEntity: class;
        void Update<TEntity>(TEntity entity) where TEntity: class;
    }
}

