﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Onactuate.Common.Logging;
using Onactuate.Common.Logging.Contracts;
using Onactuate.Common.Logging.LoggingContextProviders;
using Onactuate.DocPlusPlus.RegistrationWeb.UserStateManagement;
using Onactuate.DocPlusPlus.RegistrationWeb.Utility;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Controllers
{
    [ValidateInput(false)]
    [NoCache]
    public abstract class BaseController : Controller
    {
        public BaseController()
            : this(new LoggingService(new StaticLoggingContextProvider(ApplicationLoggingConstants.ApplicationName)))
        {}

        public BaseController(ILoggingService loggingService)
        {
            LoggingService = loggingService;
        }

        public ILoggingService LoggingService { get; internal set; }

        protected virtual string LoggingSource
        {
            get { return this.GetType().Name; }
        }

        /// <summary>
        /// GET: The default view for the inherited controller
        /// So, every controller must be having this implimented
        /// </summary>
        public virtual ActionResult Index()
        {
            return RedirectToAction("Index", "Account", new {Area=""});
        }

        protected override RedirectResult Redirect(string url)
        {
            return new AjaxAwareRedirectResult(url);
        }

        protected AdminUserState GetUserState()
        {
            //Log.Debug(this, "CJB: GetUserState: " + System.Web.HttpContext.Current.Session.SessionID + " " + (System.Web.HttpContext.Current.Session["AdminUserState"] != null));
            return (System.Web.HttpContext.Current.Session["AdminUserState"] != null) ? (AdminUserState)System.Web.HttpContext.Current.Session["AdminUserState"] : new AdminUserState();
            //return (System.Web.HttpContext.Current.Session["AdminUserState"] != null) ? (AdminUserState)System.Web.HttpContext.Current.Session["AdminUserState"] : null;
        }

        protected void ClearUserState()
        {
            System.Web.HttpContext.Current.Session.Remove("AdminUserState");
        }

        /// <summary>
        /// Adds a confirmation message that can be used to give the user feedback without concern about what page is being displayed.
        /// </summary>
        protected void AddConfirmationMessage(String confirmationMessage)
        {
            List<String> confirmationMessages = TempData["ApplicationController_ConfirmationMessage"] as List<String> ??
                                                new List<String>();

            confirmationMessages.Add(confirmationMessage);

            TempData["ApplicationController_ConfirmationMessage"] = confirmationMessages;
        }

        /// <summary>
        /// Adds a error message that can be used to give the user feedback without concern about what page is being displayed.
        protected void AddErrorMessage(String errorMessage)
        {
            List<String> errorMessages = TempData["ApplicationController_ErrorMessage"] as List<String> ??
                                         new List<String>();
            errorMessages.Add(errorMessage);
            TempData["ApplicationController_ErrorMessage"] = errorMessages;
        }

        /// <summary>
        /// Adds a error message that can be used to give the user feedback without concern about what page is being displayed.
        protected void AddValidationErrorMessage(String errorMessage)
        {
            List<String> errorMessages = TempData["ApplicationController_ErrorMessage"] as List<String> ??
                                         new List<String>();
            errorMessages.Add(errorMessage);
            TempData["ApplicationController_ErrorMessage"] = errorMessages;
        }

        protected void ClearValidationErrorMessage()
        {
            List<String> errorMessages = TempData["ApplicationController_ErrorMessage"] as List<String> ??
                                         new List<String>();
            errorMessages.Clear();
            TempData["ApplicationController_ErrorMessage"] = errorMessages;
        }

        protected void ClearConfirmationMessage()
        {
            List<String> confirmationMessages = TempData["ApplicationController_ConfirmationMessage"] as List<String> ?? new List<String>();

            confirmationMessages.Clear();

            TempData["ApplicationController_ConfirmationMessage"] = confirmationMessages;
        }

        /// <summary>
        /// Called when an unhandled exception occurs in the action
        /// This override will always log the exception that has occured
        /// it will then attempt to still return the base view in case of an unhandled
        /// exception. The base view will render with an inline error message that
        /// preserves the user page context.
        /// </summary>
        /// <param name="filterContext">Information about the current request and action.</param>
        protected override void OnException(ExceptionContext filterContext)
        {
            string originalRouteInfo =
                string.Format(
                    "Uncaught exception occured at Controller:{0} for Action:{1}",
                    filterContext.RouteData.Values["controller"],
                    filterContext.RouteData.Values["action"]);

            LoggingService.Error(
                filterContext.RouteData.Values["controller"].ToString(),
                LoggingMessageHelper.SerializeException(filterContext.Exception, originalRouteInfo));
        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }
    }
    

    public class NoCacheAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
            if (filterContext == null) throw new ArgumentNullException("filterContext");

            var cache = GetCache(filterContext);

            cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            cache.SetValidUntilExpires(false);
            cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            cache.SetCacheability(HttpCacheability.NoCache);
            cache.SetNoStore();
        }

        /// <summary>
        /// Get the reponse cache
        /// </summary>
        /// <param name="filterContext"></param>
        /// <returns></returns>
        protected virtual HttpCachePolicyBase GetCache(ResultExecutingContext filterContext)
        {
            return filterContext.HttpContext.Response.Cache;
        }
    }

    /// <summary>
    /// An abstract class used to restrict access to certain objects.
    /// </summary>
    public abstract class EnsureAccessBaseAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Indicates the name of the Route Value representing the Id of the object being requested. Defaults to "id".
        /// </summary>
        public String IdPropertyName { get; set; }

        protected abstract bool EnsureAccess(object id);
    }

    public class EnsureSessionAlive : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            // check if session is supported
            if (ctx.Session != null)
            {

                // check if a new session id was generated
                if (ctx.Session.IsNewSession)
                {

                    // If it says it is a new session, but an existing cookie exists, then it must
                    // have timed out
                    string sessionCookie = ctx.Request.Headers["Cookie"];
                    if ((null != sessionCookie) && (sessionCookie.IndexOf("ASP.NET_SessionId") >= 0))
                    {
                        var x = new UrlHelper(filterContext.RequestContext);
                        var url = x.Action("Logout", "Account", new { area = "" });
                        ctx.Response.Redirect(url);
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }

    public class LocalizedDisplayNameAttribute : DisplayNameAttribute
    {
        private PropertyInfo _nameProperty;
        private Type _resourceType;

        public LocalizedDisplayNameAttribute(String displayNameKey)
            : base(displayNameKey)
        {

        }

        public Type NameResourceType
        {
            get
            {
                return _resourceType;
            }
            set
            {
                _resourceType = value;
                //initialize nameProperty when type property is provided by setter
                _nameProperty = _resourceType.GetProperty(base.DisplayName, BindingFlags.Static | BindingFlags.Public);
            }
        }

        public override String DisplayName
        {
            get
            {
                //check if nameProperty is null and return original display name value
                if (_nameProperty == null)
                {
                    return base.DisplayName;
                }

                return (String)_nameProperty.GetValue(_nameProperty.DeclaringType, null);
            }
        }
    }

    /// <summary>
    /// Class I found to allow Redirect from inside an Ajax action. 
    /// For example, you might use this after a POST to redirect to the index action,
    /// even though the POST was inside a modal dialog and you just want to redirect
    /// the entire outer page.
    /// </summary>
    /// <see cref="http://craftycodeblog.com/2010/05/15/asp-net-mvc-ajax-redirect/"/>
    public class AjaxAwareRedirectResult : RedirectResult
    {
        public AjaxAwareRedirectResult(string url)
            : base(url)
        {
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                string destinationUrl = UrlHelper.GenerateContentUrl(Url, context.HttpContext);

                JavaScriptResult result = new JavaScriptResult()
                {
                    Script = "window.location='" + destinationUrl + "';"
                };
                result.ExecuteResult(context);
            }
            else
                base.ExecuteResult(context);
        }

    }

    /// <summary>
    /// We are going to create a RequiredIfAttribute that allows a field to be mandatory only if another 
    /// field contains a certain value. This is a common UI requirement where for example, if you tick a 
    /// checkbox, then a text field must be filled in. A typical real world example is a dropdown list 
    /// that contains multiple values including 'Other'. If the user selects this option, then some 
    /// javascript displays a 'Please specify...' text input which is a required field.
    /// </summary>
    /// <see cref="http://www.devtrends.co.uk/blog/the-complete-guide-to-validation-in-asp.net-mvc-3-part-2"/>
    public enum Comparison
    {
        IsEqualTo,
        IsNotEqualTo
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class RequiredIfAttribute : ValidationAttribute, IClientValidatable
    {
        private const string DefaultErrorMessageFormatString = "{0} is required";

        public string OtherProperty { get; private set; }
        public Comparison Comparison { get; private set; }
        public object Value { get; private set; }

        public RequiredIfAttribute(string otherProperty, Comparison comparison, object value)
        {
            if (string.IsNullOrEmpty(otherProperty))
            {
                throw new ArgumentNullException("otherProperty");
            }

            OtherProperty = otherProperty;
            Comparison = comparison;
            Value = value;

            ErrorMessage = DefaultErrorMessageFormatString;
        }

        private bool Validate(object actualPropertyValue)
        {
            switch (Comparison)
            {
                case Comparison.IsEqualTo: // This had to be changed from "IsNotEqualTo" to reverse the behavior
                    return actualPropertyValue == null || !actualPropertyValue.Equals(Value);
                default:
                    return actualPropertyValue != null && actualPropertyValue.Equals(Value);
            }
        }

        protected override ValidationResult IsValid(object value,
                                                    ValidationContext validationContext)
        {
            if (value == null)
            {
                var property = validationContext.ObjectInstance.GetType()
                                                .GetProperty(OtherProperty);

                var propertyValue = property.GetValue(validationContext.ObjectInstance, null);

                if (!Validate(propertyValue))
                {
                    return new ValidationResult(
                        string.Format(ErrorMessageString, validationContext.DisplayName));
                }
            }
            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(
                                                            ModelMetadata metadata,
                                                            ControllerContext context)
        {
            return new[]
        {
            new ModelClientValidationRequiredIfRule(string.Format(ErrorMessageString, 
                metadata.GetDisplayName()), OtherProperty, Comparison, Value)
        };
        }
    }

    public class ModelClientValidationRequiredIfRule : ModelClientValidationRule
    {
        public ModelClientValidationRequiredIfRule(string errorMessage,
                                                   string otherProperty,
                                                   Comparison comparison,
                                                   object value)
        {
            ErrorMessage = errorMessage;
            ValidationType = "requiredif";
            ValidationParameters.Add("other", otherProperty);
            ValidationParameters.Add("comp", comparison.ToString().ToLower());
            ValidationParameters.Add("value", value.ToString().ToLower());
        }
    }
}