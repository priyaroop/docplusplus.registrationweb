﻿using System;
using System.Web.Mvc;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Controllers
{
    [Serializable]
    public class ErrorContext
    {
        [NonSerialized]
        private ExceptionContext _context;
        [NonSerialized]
        internal const string ERRORCONTEXTKEY = "Onactuate-Cloud_ErrorContext";

        public ErrorContext()
        {

        }
        public ErrorContext(ExceptionContext context)
        {
            this._context = context;
            this.Init();
        }
        //init these calues from context;
        private void Init()
        {
            this.ErrorMessage = this._context.Exception.Message;
            this.StackTrace = this._context.Exception.StackTrace;
            this.IsInline = false;
        }
        //Message corresponding to the error that occured (Exception)
        public string ErrorMessage { get; set; }
        //Need to check if we need something as specific as stacktrace
        public string StackTrace { get; set; }

        public string PartialView { get; set; }

        public bool IsInline { get; set; }

    }
}