using System;
using System.IO;
using System.Net.Mail;
using System.Web.Hosting;
using System.Web.Mvc;
using Onactuate.Common.Logging.Contracts;
using Onactuate.DocPlusPlus.RegistrationWeb.Models;
using Onactuate.DocPlusPlus.RegistrationWeb.Providers;
using Onactuate.DocPlusPlus.RegistrationWeb.Utility;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Platform.Services.SqlTableServices;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Controllers
{
    // [Authorize]
    public class AccountController : BaseController
    {
        #region Private Member Variables
        
        private ITenantMasterService TenantMasterServiceObj { get; set; }
        private ILoggingService LoggingServiceObj { get; set; }
        private IDatabaseConfiguration DatabaseConfigurationObj { get;set; }

        #endregion

        #region Class Constructor

        // TODO: Remove this and initialize from Unity
        public AccountController()
        {
            TenantMasterServiceObj = new TenantMasterService();
            DatabaseConfigurationObj = new DatabaseConfiguration();
            //LoggingServiceObj = new LoggingService();
        }

        #endregion

        #region Action Methods

        [HttpGet]
        [AllowAnonymous]
        public override ActionResult Index()
        {
            TenantRegistrationModel viewModel = new TenantRegistrationModel();
            return View(viewModel);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Registration()
        {
            return View("Registration");
        }
        
        [HttpGet]
        [AllowAnonymous]
        public ActionResult RegistrationStatusPartial()
        {
            return PartialView("RegistrationStatusPartial");
        }

        public JsonResult GetRegistrationStatus()
        {

            int hrs = DateTime.Now.Hour;
            int min = DateTime.Now.Minute;
            int sec = DateTime.Now.Second;

            int totalSeconds = sec + (min * 60) + (hrs * 3600);
            int totalVictims = totalSeconds * 14;

            //logic here for getting your count
            int victimCount = totalVictims;

            return Json(victimCount, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method creates a new Web Tenant 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Registration(TenantRegistrationModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View("Index", viewModel);
                }

                string firstName = viewModel.FirstName;
                string lastName = viewModel.LastName;
                string emailAddress = viewModel.EmailAddress;
                string company = viewModel.CompanyName;
                string password = viewModel.Password;
                string phoneNumber = viewModel.PhoneNumber;
                string subscriptionPlan = viewModel.SubscriptionPlan;

                string tenantName = $"Cafr365{DateTime.Now.ToString("yyyyMMddHHmmss")}";
                string username = (new MailAddress(emailAddress)).User;

                string domainForLogin = DatabaseConfigurationObj.DomainForLogin;
                string webSiteRootPath = DatabaseConfigurationObj.WebsiteRootPath;
                string ipAddress = DatabaseConfigurationObj.MachineIpAddress;
                string webDomainName = DatabaseConfigurationObj.WebDomainName;
                string databaseAdminUser = DatabaseConfigurationObj.DatabaseAdminUser;
                string databaseAdminPassword = DatabaseConfigurationObj.DatabaseAdminPassword;
                string databaseServer = DatabaseConfigurationObj.DatabaseServer;

                string physicalPath  = Path.Combine(webSiteRootPath, Guid.NewGuid().ToString());

                // Database fields
                string tenantDatabaseName = $"sqldb-cafr365-{tenantName}";
                var connectionString = DatabaseHelper.GetConnectionString(databaseServer, tenantDatabaseName, databaseAdminUser, databaseAdminPassword);
                string tenantDbOwnerUsername = $"svc_{tenantName}_prod";
                Random generator = new Random();
                String randomNumber = generator.Next(0, 999999).ToString("D6");
                string tenantDbOwnerPassword = $"$Tenant@{Guid.NewGuid().ToString().Replace("-","")}{randomNumber}==";

                // Create Registration request in the database (User will track status from here)
                TenantRegistration tenantRegistration = new TenantRegistration()
                {
                    EmailAddress = emailAddress,
                    TenantName = tenantName,
                    RegistrationStatus = Convert.ToInt32(RegistrationStatus.Created),
                    RegistrationStatusMessage = "Registration request submitted successfully",
                    CreationTime = DateTime.Now,
                    LastUpdated = DateTime.Now
                };
                TenantMasterServiceObj.CreateTenantRegistration(tenantRegistration);

                TenantRegistrationModel registrationData = new TenantRegistrationModel()
                {
                    TenantName = tenantName,
                    Username = username,
                    Password = password,
                    CompanyName = company,
                    DomainForLogin = domainForLogin,
                    FirstName = firstName,
                    LastName = lastName,
                    EmailAddress = emailAddress,
                    PhoneNumber = phoneNumber,
                    SubscriptionPlan = subscriptionPlan,
                    PhysicalPath  = physicalPath,
                    IpAddress = ipAddress,
                    DomainName = webDomainName,

                    // Database fields
                    TenantDatabaseName = tenantDatabaseName,
                    DatabaseAdminUser = databaseAdminUser,
                    DatabaseAdminPassword = databaseAdminPassword,
                    DatabaseServer = databaseServer,
                    ConnectionString = connectionString,
                    TenantDbOwnerUsername = tenantDbOwnerUsername,
                    TenantDbOwnerPassword = tenantDbOwnerPassword,

                    TenantRegistrationObj = tenantRegistration,
                    DatabaseConfigurationObj = DatabaseConfigurationObj
                };

                // TODO:: See if WWF can be used here for better workflow
                // Process the registration request in a background worker thread and move on
                HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => new BackgroundWorker().RegisterNewTenant(cancellationToken, registrationData));  

            }
            catch (Exception)
            {
                throw;
            }

            return View("Registration");
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult IsEmailAddressUnique(string emailAddress)
        {
            bool isUnique = TenantMasterServiceObj.IsEmailAddressUnique(emailAddress);
            return Json(isUnique, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Private Helper Methods - TO DELETE

        //private string CreateWebsite(string tenantName, string physicalPath)
        //{
        //    string websiteAddress = string.Empty;
        //    try
        //    {
        //        string dotNetVersion = "";
        //        string siteName = string.Format("CAFR 365 - {0}", tenantName);
        //        string hostHeader = string.Format("{0}.docplusplus.com", tenantName);

        //        // From AppSettings in Web.Config
        //        string metabasePath = ConfigurationManager.AppSettings["WebsiteMetabasePath"];
        //        //string physicalPath  = Path.Combine(ConfigurationManager.AppSettings["WebsiteRootPath"], Guid.NewGuid().ToString());
        //        string publishWebPath = ConfigurationManager.AppSettings["PublishWebPath"];
        //        string ipAddress = ConfigurationManager.AppSettings["MachineIpAddress"];
        //        string windowsUserName = ConfigurationManager.AppSettings["WindowsUserName"];
        //        string windowsUserPassword = ConfigurationManager.AppSettings["WindowsUserPassword"];

        //        // Copy the website to the physical path
        //        Directory.CreateDirectory(physicalPath);
        //        DirectoryHelper.Copy(publishWebPath, physicalPath);

        //        using (WindowsSecurityAuthentication security = new WindowsSecurityAuthentication())
        //        {
        //            if(security.ValidateUser(windowsUserName, windowsUserPassword, "", out WindowsIdentity loggedInUser, out _))
        //            {
        //                loggedInUser.Impersonate();
        //                if (!WebsiteHelper.CreateSite(metabasePath, siteName, physicalPath, ipAddress, hostHeader,
        //                    dotNetVersion, windowsUserName, windowsUserPassword))
        //                {
        //                    // Couldn't create website
        //                    throw new Exception("Could not create website");
        //                }

        //                websiteAddress = hostHeader;
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }

        //    return websiteAddress;
        //}
        
        #endregion
    }
}