﻿#region

using System;
using System.Security.Principal;
using System.Web;

#endregion

namespace Onactuate.DocPlusPlus.RegistrationWeb.UserStateManagement
{
    [Serializable]
    public class AdminUserState : IAdminUserState
    {
        private string _user;
        private bool _isAdmin = false;
        private bool _isAuthorized = false;
        private int _userPermissionId = int.MinValue;
        public AdminUserState()
        {
            if (HttpContext.Current != null)
            {
                object cached = HttpContext.Current.Session["CachedUser"];
                //object loggedUser = HttpContext.Current.Session["UserIdentity"];
                object isAdmin = HttpContext.Current.Session["IsAdmin"];
                object isAuthorized = HttpContext.Current.Session["IsAuthorized"];
                object userPermissionId = HttpContext.Current.Session["UserPermissionId"];
                if (cached != null)
                {
                    _user = (string)cached;
                }
                //if (loggedUser != null)
                //{
                //    _userIdentity = (IPrincipal)loggedUser;
                //}
                if (isAdmin != null)
                {
                    _isAdmin = Convert.ToBoolean(isAdmin);
                }
                if (isAuthorized != null)
                {
                    _isAuthorized = Convert.ToBoolean(isAuthorized);
                }
                if (userPermissionId != null)
                {
                    _userPermissionId = Convert.ToInt32(userPermissionId);
                }
            }
        }

        #region IAdminUserState Members

        private IPrincipal _userIdentity;
        public IPrincipal UserIdentity
        {
            get
            {
                if (_userIdentity == null)
                {
                    if (HttpContext.Current == null || !HttpContext.Current.Request.IsAuthenticated)
                    {
                        _userIdentity = null;
                    }

                    HttpContext.Current.Session.Add("UserIdentity", _userIdentity);
                }
                return _userIdentity;
            }
        }

        /// <summary>
        ///   Returns a User object representing the current user
        /// </summary>
        public string CurrentUser
        {
            get
            {
                if (_user == null)
                {
                    if (HttpContext.Current == null || !HttpContext.Current.Request.IsAuthenticated)
                    {
                        _user = null;
                    }

                    HttpContext.Current.Session.Add("CachedUser", _user);
                }
                return _user;
            }
        }

        public bool IsAuthorizedUser
        {
            get { return _isAuthorized; }
        }

        public int UserPermissionId
        {
            get { return _userPermissionId; }
        }

        public bool IsAdmin
        {
            get { return _isAdmin; }
        }

        /// <summary>
        /// GetClientIPAddress
        /// </summary>
        /// <returns></returns>
        public string GetClientIPAddress()
        {
            return HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ??
                   HttpContext.Current.Request.UserHostAddress;
        }

        #endregion
    }
}