using System.Security.Principal;

namespace Onactuate.DocPlusPlus.RegistrationWeb.UserStateManagement
{
    public interface IAdminUserState
    {
        /// <summary>
        /// Identity of the logged in user
        /// </summary>
        IPrincipal UserIdentity { get; }

        /// <summary>
        /// Returns a current user
        /// </summary>
        string CurrentUser { get; }

        /// <summary>
        /// Returns if the current user is authorized to view the application
        /// </summary>
        bool IsAuthorizedUser { get; }

        /// <summary>
        /// Permission Id of the current in user
        /// </summary>
        int UserPermissionId { get; }

        /// <summary>
        /// Returns if the user is the Administrator of the application
        /// </summary>
        bool IsAdmin { get; }
    }
}