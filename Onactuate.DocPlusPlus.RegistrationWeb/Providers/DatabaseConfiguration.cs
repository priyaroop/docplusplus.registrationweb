﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CommonServiceLocator;
using Onactuate.Common.Configuration.ConfigSettings.ConfigEnvironment;
using Onactuate.Common.Configuration.Models;
using Onactuate.Common.Utilities.RESTClient;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Providers
{
    public class DatabaseConfiguration : IDatabaseConfiguration
    {
        private Uri EnvConfigRestServiceBaseUrl { get; set; }
        private string AuthenticationUsername { get; set; }
        private string AuthenticationPassword { get; set; }
        private string GrantType { get; set; }
        private readonly string _configToken;
        private readonly string _environment;
        protected readonly Dictionary<string, string> _configuration;

        public DatabaseConfiguration()
        {
            EnvConfigRestServiceBaseUrl = new Uri(ConfigurationManager.AppSettings[Utility.Constants.AppSettingsKeys.EnvConfigRestServiceBaseUrlKey]);
            AuthenticationUsername = ConfigurationManager.AppSettings[Utility.Constants.AppSettingsKeys.AuthenticationUsernameKey];
            AuthenticationPassword = ConfigurationManager.AppSettings[Utility.Constants.AppSettingsKeys.AuthenticationPasswordKey];
            GrantType = "password";

            _configToken = Utility.Constants.EnvironmentTokens.CreateWebSettingsToken;
            _environment = EnvironmentSection.GetEnvironmentName;

            // Call Web API to get the keys for the environment
            SimpleRESTClient restClient = new SimpleRESTClient(EnvConfigRestServiceBaseUrl, AuthenticationUsername, AuthenticationPassword);
            string methodName = "EnvConfig/GetEnvironmentKeys";
            IDictionary<string, string> parameters = new Dictionary<string, string>
            {
                {"environmentName", _environment}, {"tokenName", _configToken}
            };

            EnvironmentKeys keys = restClient.CallHttpGetMethod<EnvironmentKeys>(methodName, new RequestParameters(parameters));
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            _configuration = new Dictionary<string, string>(keys.Keys, comparer);
        }

        public virtual string DomainForLogin
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.DomainForLoginKey]; }
        }

        public virtual string WindowsUserName
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.WindowsUserNameKey]; }
        }

        public virtual string WindowsUserPassword
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.WindowsUserPasswordKey]; }
        }

        public virtual string WebsiteMetabasePath
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.WebsiteMetabasePathKey]; }
        }

        public virtual string WebsiteRootPath
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.WebsiteRootPathKey]; }
        }

        public virtual string PublishWebPath
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.PublishWebPathKey]; }
        }

        public virtual string MachineIpAddress
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.MachineIpAddressKey]; }
        }

        public virtual string WebDomainName
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.WebDomainNameKey]; }
        }

        public virtual string NameApiRestUrl
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NameApiRestUrlKey]; }
        }

        public virtual string NameApiTokenName
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NameApiTokenNameKey]; }
        }

        public virtual string NameApiToken
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NameApiTokenKey]; }
        }

        public virtual string NameApiLoginUsername
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NameApiLoginUsernameKey]; }
        }

        public virtual string NameApiLoginPassword
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NameApiLoginPasswordKey]; }
        }

        public virtual string DatabaseServer
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.DatabaseServerKey]; }
        }

        public virtual string DatabaseAdminUser
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.DatabaseAdminUserKey]; }
        }

        public virtual string DatabaseAdminPassword
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.DatabaseAdminPasswordKey]; }
        }
    }
}