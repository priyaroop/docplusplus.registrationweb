﻿using System.Collections.Generic;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Providers
{
    public interface IEmailConfiguration
    {
        Dictionary<string, string> ConfigKeys { get; }

        // SMTP Server settings
        string NotificationMailFormat { get; }
        string NotificationMailPriority { get; }
        string NotificationMailEncoding { get; }
        string NotificationSmtpServer { get; }
        string NotificationAuthenticationType { get; }
        int NotificationSmtpPort { get; }
        string NotificationSmtpUserName { get; }
        string NotificationSmtpPassword { get; }
        string NotificationSmtpMachineDomain { get; }

        // Email Error Messages
        string ATTACHEMNT_FILE_NOT_FOUND { get; }
        string AUTHENTICATION_TYPE_NOT_PROVIDED { get; }
        string INVALID_EMAIL_ADDRESS { get; }
        string NO_RECIPIENTS { get; }
        string NO_SENDER_PROVIDED { get; }
        string PASSWORD_NAME_NOT_PROVIDED { get; }
        string SMTP_SERVER_NOT_PROVIDED { get; }
        string USER_NAME_NOT_PROVIDED { get; }

        // Email Subject and Email body
        string RegistrationSuccessNotificationSubject { get; }
        string RegistrationErrorNotificationSubject { get; }
        string RegistrationErrorNotificationBody { get; }
        string RegistrationSuccessNotificationBody { get; }

        // Email Addresses
        string FromEmailAddress { get; }

        // Email Send Boolean flags
        bool RegistrationSuccessNotificationEmail { get; }
        bool RegistrationErrorNotificationEmail { get; }
    }
}