﻿namespace Onactuate.DocPlusPlus.RegistrationWeb.Providers
{
    public interface IDatabaseConfiguration
    {
        // Web Server Settings
        string DomainForLogin { get; }
        string WindowsUserName { get; }
        string WindowsUserPassword { get; }
        string WebsiteMetabasePath { get; }
        string WebsiteRootPath { get; }
        string PublishWebPath { get; }
        string MachineIpAddress { get; }
        string WebDomainName { get; }
        
        // Name.com
        string NameApiRestUrl { get; }
        string NameApiTokenName { get; }
        string NameApiToken { get; }
        string NameApiLoginUsername { get; }
        string NameApiLoginPassword { get; }

        // Database Server settings
        string DatabaseServer { get; }
        string DatabaseAdminUser { get; }
        string DatabaseAdminPassword { get; }
    }
}