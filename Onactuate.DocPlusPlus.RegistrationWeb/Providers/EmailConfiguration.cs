﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Onactuate.Common.Configuration.ConfigSettings.ConfigEnvironment;
using Onactuate.Common.Configuration.Models;
using Onactuate.Common.Utilities.RESTClient;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Providers
{
    public class EmailConfiguration : IEmailConfiguration
    {
        private Uri EnvConfigRestServiceBaseUrl { get; set; }
        private string AuthenticationUsername { get; set; }
        private string AuthenticationPassword { get; set; }
        private string GrantType { get; set; }
        private readonly string _configToken;
        private readonly string _environment;
        private readonly Dictionary<string, string> _configuration;

        public EmailConfiguration()
        {
            EnvConfigRestServiceBaseUrl = new Uri(ConfigurationManager.AppSettings[Utility.Constants.AppSettingsKeys.EnvConfigRestServiceBaseUrlKey]);
            AuthenticationUsername = ConfigurationManager.AppSettings[Utility.Constants.AppSettingsKeys.AuthenticationUsernameKey];
            AuthenticationPassword = ConfigurationManager.AppSettings[Utility.Constants.AppSettingsKeys.AuthenticationPasswordKey];
            GrantType = "password";

            _configToken = Utility.Constants.EnvironmentTokens.EmailSettingsToken;
            _environment = EnvironmentSection.GetEnvironmentName;

            // Call Web API to get the keys for the environment
            SimpleRESTClient restClient = new SimpleRESTClient(EnvConfigRestServiceBaseUrl, AuthenticationUsername, AuthenticationPassword);
            string methodName = "EnvConfig/GetEnvironmentKeys";
            IDictionary<string, string> parameters = new Dictionary<string, string>
            {
                {"environmentName", _environment}, {"tokenName", _configToken}
            };

            EnvironmentKeys keys = restClient.CallHttpGetMethod<EnvironmentKeys>(methodName, new RequestParameters(parameters));
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            _configuration = new Dictionary<string, string>(keys.Keys, comparer);
        }
        
        public virtual Dictionary<string, string> ConfigKeys
        {
            get { return _configuration; }
        }
        
        #region SMTP Server settings

        public virtual string NotificationMailFormat
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NotificationMailFormatConfig]; }
        }

        public virtual string NotificationMailPriority
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NotificationMailPriorityConfig]; }
        }

        public virtual string NotificationMailEncoding
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NotificationMailEncodingConfig]; }

        }

        public virtual string NotificationSmtpServer
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NotificationSmtpServerConfig]; }
        }

        public virtual string NotificationAuthenticationType
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NotificationAuthenticationTypeConfig]; }
        }

        public virtual int NotificationSmtpPort
        {
            get { return Convert.ToInt32(_configuration[Utility.Constants.EnvironmentKeys.NotificationSmtpPortConfig]); }
        }

        public virtual string NotificationSmtpUserName
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NotificationSmtpUserNameConfig]; }
        }

        public virtual string NotificationSmtpPassword
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NotificationSmtpPasswordConfig]; }
        }

        public virtual string NotificationSmtpMachineDomain
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NotificationSmtpMachineDomainConfig]; }
        }

        #endregion

        #region Email Error Messages

        public virtual string ATTACHEMNT_FILE_NOT_FOUND
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.ATTACHEMNT_FILE_NOT_FOUND_Config]; }
        }

        public virtual string AUTHENTICATION_TYPE_NOT_PROVIDED
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.AUTHENTICATION_TYPE_NOT_PROVIDED_Config]; }
        }

        public virtual string INVALID_EMAIL_ADDRESS
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.INVALID_EMAIL_ADDRESS_Config]; }
        }

        public virtual string NO_RECIPIENTS
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NO_RECIPIENTS_Config]; }
        }

        public virtual string NO_SENDER_PROVIDED
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.NO_SENDER_PROVIDED_Config]; }
        }

        public virtual string PASSWORD_NAME_NOT_PROVIDED
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.PASSWORD_NAME_NOT_PROVIDED_Config]; }
        }

        public virtual string SMTP_SERVER_NOT_PROVIDED
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.SMTP_SERVER_NOT_PROVIDED_Config]; }
        }

        public virtual string USER_NAME_NOT_PROVIDED
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.USER_NAME_NOT_PROVIDED_Config]; }
        }

        #endregion

        #region Email Subject and Email body

        public virtual string RegistrationSuccessNotificationSubject
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.RegistrationSuccessNotificationSubjectConfig]; }

        }

        public virtual string RegistrationErrorNotificationSubject
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.RegistrationErrorNotificationSubjectConfig]; }

        }

        public virtual string RegistrationErrorNotificationBody
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.RegistrationErrorNotificationBodyConfig]; }

        }

        public virtual string RegistrationSuccessNotificationBody
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.RegistrationSuccessNotificationBodyConfig]; }

        }

        #endregion

        #region Email Addresses

        public virtual string FromEmailAddress
        {
            get { return _configuration[Utility.Constants.EnvironmentKeys.FromEmailAddressConfig]; }
        }
       
        #endregion

        #region Email Send Boolean flags

        public bool RegistrationSuccessNotificationEmail
        {
            get { return Convert.ToBoolean(_configuration[Utility.Constants.EnvironmentKeys.SendRegistrationSuccessEmailConfig]); }
        }

        public bool RegistrationErrorNotificationEmail
        {
            get { return Convert.ToBoolean(_configuration[Utility.Constants.EnvironmentKeys.SendRegistrationErrorEmailConfig]); }
        }
    
        #endregion
    }
}