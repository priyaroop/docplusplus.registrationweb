﻿namespace Onactuate.DocPlusPlus.RegistrationWeb.Models
{
    public enum SubscriptionPlanEnum
    {
        FreeTrial = 0,
        BasicPlan = 1,
        StandardPlan = 2,
        EnterprisePlan = 3
    }
}