﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Onactuate.DocPlusPlus.RegistrationWeb.Providers;
using Onactuate.DocPlusPlus.RegistrationWeb.Utility;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Models
{
    public class TenantRegistrationModel
    {
        // Form Fields ---------------------------------------------
        [Required(ErrorMessage="First Name field is required")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage="Last Name field is required")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage="Please enter Company Email Address")]
        [Display(Name = "Company Email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression("^([\\w-.]+@(?!gmail\\.com)(?!yahoo\\.com)(?!hotmail\\.com)([\\w-]+.)+[\\w-]{2,4})?$", ErrorMessage = "Please enter Company Email only")]
        [Remote("IsEmailAddressUnique","Account", HttpMethod = "POST", ErrorMessage="Email Address is already in use")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage="Please enter Company Name.")]
        [Display(Name = "Company")]
        public string CompanyName { get; set; }
        
        [Required(ErrorMessage="Please enter a valid Phone Number.")]
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage="Please create a Password")]
        [Display(Name = "Create Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage="Please select a Subscription Plan")]
        [Display(Name = "Select a Subscription Plan")]
        public string SubscriptionPlan { get; set; }

        [CheckBoxRequired(ErrorMessage = "Please agree to the Evaluation Terms and Privacy Policy")]
        public bool PrivacyTerm { get; set; }

        // ---------------------------------------------------------

        public int TenantRegistrationId { get; set; }
        public string TenantName { get; set; }
        public string Username { get; set; }
        public string DomainForLogin { get; set; }
        public string PhysicalPath  { get; set; }
        public string IpAddress { get; set; }
        public string DomainName { get; set; }

        // Database fields
        public string TenantDatabaseName { get; set; }
        public string DatabaseAdminUser { get; set; }
        public string DatabaseAdminPassword { get; set; }
        public string DatabaseServer { get; set; }
        public string ConnectionString { get; set; }
        public string TenantDbOwnerUsername { get; set; }
        public string TenantDbOwnerPassword { get; set; }

        public TenantRegistration TenantRegistrationObj { get; set; }

        public IDatabaseConfiguration DatabaseConfigurationObj { get; set; }
    }
}