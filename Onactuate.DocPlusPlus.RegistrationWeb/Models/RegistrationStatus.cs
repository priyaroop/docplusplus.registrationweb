﻿namespace Onactuate.DocPlusPlus.RegistrationWeb.Models
{
    public enum RegistrationStatus
    {
        Created = 0,
        Inprogress = 1,
        Completed = 2,
        Error = 3
    }
}