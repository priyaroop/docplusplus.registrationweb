/****** Object:  Table [dbo].[ApplicationPermission]    Script Date: 9/10/2018 2:44:51 PM ******/
CREATE TABLE [dbo].[ApplicationPermission](
	[PermissionId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](500) NOT NULL,
	[Password] [nvarchar](2000) NOT NULL,
	[IsDeletable] [bit] NULL,
	[PermissionTypeId] [int] NULL,
 CONSTRAINT [PK_ApplicationPermission] PRIMARY KEY CLUSTERED 
(
	[PermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[ApproverGroupMaster]    Script Date: 9/10/2018 2:44:53 PM ******/
CREATE TABLE [dbo].[ApproverGroupMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_ApproverGroupMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[ApproverGroupMember]    Script Date: 9/10/2018 2:44:54 PM ******/
CREATE TABLE [dbo].[ApproverGroupMember](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApproverGroupId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
 CONSTRAINT [PK_ApproverGroupMember] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[CafrDataReports]    Script Date: 9/10/2018 2:44:55 PM ******/
CREATE TABLE [dbo].[CafrDataReports](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DataReportName] [nvarchar](500) NOT NULL,
	[DataReportDescription] [nvarchar](2000) NULL,
	[UploadFileName] [nvarchar](2000) NOT NULL,
	[UploadFilePath] [nvarchar](4000) NOT NULL,
	[UploadedOn] [datetime] NOT NULL,
	[UploadedByPermissionId] [int] NOT NULL,
 CONSTRAINT [PK_CafrDataReports] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[CafrTemplateMaster]    Script Date: 9/10/2018 2:44:56 PM ******/
CREATE TABLE [dbo].[CafrTemplateMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](500) NOT NULL,
	[TemplateDescription] [nvarchar](500) NULL,
	[UploadFileName] [nvarchar](2000) NOT NULL,
	[UploadFilePath] [nvarchar](4000) NOT NULL,
	[UploadedOn] [datetime] NOT NULL,
	[UploadedByPermissionId] [int] NOT NULL,
 CONSTRAINT [PK_CafrTemplateMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[DocumentApproverMaster]    Script Date: 9/10/2018 2:44:56 PM ******/
CREATE TABLE [dbo].[DocumentApproverMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DocumentId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
 CONSTRAINT [PK_DocumentApproverMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[DocumentId] ASC,
	[PermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[DocumentMaster]    Script Date: 9/10/2018 2:44:57 PM ******/
CREATE TABLE [dbo].[DocumentMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DocumentName] [nvarchar](500) NOT NULL,
	[DocumentDescription] [nvarchar](500) NULL,
	[UploadFileName] [nvarchar](2000) NOT NULL,
	[UploadedOn] [datetime] NOT NULL,
	[UploadedByPermissionId] [int] NOT NULL,
	[HasBeenOpened] [bit] NOT NULL,
	[DocumentType] [int] NOT NULL,
 CONSTRAINT [PK_DocumentMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[DocumentType]    Script Date: 9/10/2018 2:44:58 PM ******/
CREATE TABLE [dbo].[DocumentType](
	[Id] [int] NOT NULL,
	[DocumentType] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_DocumentType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[PermissionTypeMaster]    Script Date: 9/10/2018 2:44:59 PM ******/
CREATE TABLE [dbo].[PermissionTypeMaster](
	[Id] [int] NOT NULL,
	[PermissionType] [nvarchar](500) NOT NULL
) ON [PRIMARY]

/****** Object:  Table [dbo].[ReportCafr]    Script Date: 9/10/2018 2:45:00 PM ******/
CREATE TABLE [dbo].[ReportCafr](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Text] [nvarchar](500) NULL,
 CONSTRAINT [PK_ReportCafr] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[ReportCafrMap]    Script Date: 9/10/2018 2:45:00 PM ******/
CREATE TABLE [dbo].[ReportCafrMap](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReportId] [int] NULL,
	[CategoryId] [int] NULL,
	[SubCategoryId] [int] NULL,
	[ColumnId] [int] NULL,
	[TrialBalanceId] [int] NULL,
	[CurrentDate] [datetime] NULL,
 CONSTRAINT [PK_ReportCafrMap] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[ReportCategory]    Script Date: 9/10/2018 2:45:01 PM ******/
CREATE TABLE [dbo].[ReportCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
 CONSTRAINT [PK_ReportCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[ReportCategoryMap]    Script Date: 9/10/2018 2:45:02 PM ******/
CREATE TABLE [dbo].[ReportCategoryMap](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReportId] [int] NULL,
	[CategoryId] [int] NULL,
	[SubCategoryId] [int] NULL,
	[CurrentDate] [datetime] NULL,
 CONSTRAINT [PK_ReportCategoryMap] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[ReportColumns]    Script Date: 9/10/2018 2:45:03 PM ******/
CREATE TABLE [dbo].[ReportColumns](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_ReportColumns] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[ReportColumnTB]    Script Date: 9/10/2018 2:45:04 PM ******/
CREATE TABLE [dbo].[ReportColumnTB](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReportId] [int] NULL,
	[ColumnId] [int] NULL,
	[TrialBalanceId] [int] NULL,
	[CurrentDate] [datetime] NULL,
 CONSTRAINT [PK_ReportColumnTB] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[ReportSubCategory]    Script Date: 9/10/2018 2:45:05 PM ******/
CREATE TABLE [dbo].[ReportSubCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Name] [nvarchar](500) NULL,
 CONSTRAINT [PK_ReportSubCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[ReportTrialBalance]    Script Date: 9/10/2018 2:45:05 PM ******/
CREATE TABLE [dbo].[ReportTrialBalance](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SubCategoryId] [int] NULL,
	[Fund] [int] NULL,
	[MainAccount] [int] NULL,
	[Division] [int] NULL,
	[Name] [nvarchar](max) NULL,
	[OpeningBalance] [decimal](18, 2) NULL,
	[Debit] [decimal](18, 2) NULL,
	[Credit] [decimal](18, 2) NULL,
	[ClosingBalance] [decimal](18, 2) NULL,
 CONSTRAINT [PK_TrialBalance] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Table [dbo].[SharepointMaster]    Script Date: 9/10/2018 2:45:06 PM ******/
CREATE TABLE [dbo].[SharepointMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DocumentMasterId] [int] NOT NULL,
	[SharepointUrl] [nvarchar](max) NOT NULL,
	[SharepointUsername] [nvarchar](max) NOT NULL,
	[SharepointPassword] [varbinary](max) NOT NULL,
	[SharepointRelativeFolderPath] [nvarchar](max) NULL,
 CONSTRAINT [PK_SharepointMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Table [dbo].[sysdiagrams]    Script Date: 9/10/2018 2:45:07 PM ******/
CREATE TABLE [dbo].[sysdiagrams](
	[name] [sysname] NOT NULL,
	[principal_id] [int] NOT NULL,
	[diagram_id] [int] IDENTITY(1,1) NOT NULL,
	[version] [int] NULL,
	[definition] [varbinary](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[diagram_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_principal_name] UNIQUE NONCLUSTERED 
(
	[principal_id] ASC,
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Table [dbo].[TaskComment]    Script Date: 9/10/2018 2:45:08 PM ******/
CREATE TABLE [dbo].[TaskComment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaskId] [int] NOT NULL,
	[Comments] [nvarchar](max) NOT NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_TaskComment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Table [dbo].[TaskMaster]    Script Date: 9/10/2018 2:45:09 PM ******/
CREATE TABLE [dbo].[TaskMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaskName] [nvarchar](1000) NOT NULL,
	[OwnerPermissionId] [int] NOT NULL,
	[AssignedToPermissionId] [int] NOT NULL,
	[CompletionDate] [datetime] NOT NULL,
	[TaskStatusId] [int] NOT NULL,
	[IsNewTask] [bit] NOT NULL,
	[TaskDocumentName] [nvarchar](1000) NOT NULL,
	[TaskDocumentCopyName] [nvarchar](1000) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[MasterDocumentId] [int] NOT NULL,
	[ApproverGroupId] [int] NOT NULL,
 CONSTRAINT [PK_TaskMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[TaskStatus]    Script Date: 9/10/2018 2:45:09 PM ******/
CREATE TABLE [dbo].[TaskStatus](
	[Id] [int] NOT NULL,
	[TaskStatusDescription] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_TaskStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[UserDocuments]    Script Date: 9/10/2018 2:45:11 PM ******/
CREATE TABLE [dbo].[UserDocuments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DocumentId] [int] NULL,
	[UserId] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_UserDocuments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[WorkflowInstance]    Script Date: 9/10/2018 2:45:12 PM ******/
CREATE TABLE [dbo].[WorkflowInstance](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkflowMasterId] [int] NOT NULL,
	[CurrentWorkflowStepId] [int] NOT NULL,
	[CurrentStatusId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
 CONSTRAINT [PK_WorkflowInstance] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[WorkflowMaster]    Script Date: 9/10/2018 2:45:13 PM ******/
CREATE TABLE [dbo].[WorkflowMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkflowName] [nvarchar](250) NOT NULL,
	[WorkflowDescription] [nvarchar](2000) NULL,
 CONSTRAINT [PK_WorkflowMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[WorkflowStatus]    Script Date: 9/10/2018 2:45:14 PM ******/
CREATE TABLE [dbo].[WorkflowStatus](
	[ID] [int] NOT NULL,
	[StatusName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_WorkflowStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[WorkflowStepApprovers]    Script Date: 9/10/2018 2:45:14 PM ******/
CREATE TABLE [dbo].[WorkflowStepApprovers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkflowStepId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
 CONSTRAINT [PK_WorkflowStepApprovers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[WorkflowSteps]    Script Date: 9/10/2018 2:45:15 PM ******/
CREATE TABLE [dbo].[WorkflowSteps](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkflowMasterId] [int] NOT NULL,
	[StepName] [nvarchar](250) NOT NULL,
	[StepDescription] [nvarchar](2000) NULL,
 CONSTRAINT [PK_WorkflowSteps] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[DocumentMaster] ADD  CONSTRAINT [DF_DocumentMaster_HasBeenOpened]  DEFAULT ((0)) FOR [HasBeenOpened]

ALTER TABLE [dbo].[TaskMaster] ADD  CONSTRAINT [DF_TaskMaster_IsNewTask]  DEFAULT ((1)) FOR [IsNewTask]

ALTER TABLE [dbo].[ApproverGroupMember]  WITH CHECK ADD  CONSTRAINT [FK_ApproverGroupMember_ApplicationPermission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[ApplicationPermission] ([PermissionId])

ALTER TABLE [dbo].[ApproverGroupMember] CHECK CONSTRAINT [FK_ApproverGroupMember_ApplicationPermission]

ALTER TABLE [dbo].[ApproverGroupMember]  WITH CHECK ADD  CONSTRAINT [FK_ApproverGroupMember_ApproverGroupMaster] FOREIGN KEY([ApproverGroupId])
REFERENCES [dbo].[ApproverGroupMaster] ([Id])

ALTER TABLE [dbo].[ApproverGroupMember] CHECK CONSTRAINT [FK_ApproverGroupMember_ApproverGroupMaster]

ALTER TABLE [dbo].[DocumentApproverMaster]  WITH CHECK ADD  CONSTRAINT [FK_DocumentApproverMaster_ApplicationPermission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[ApplicationPermission] ([PermissionId])

ALTER TABLE [dbo].[DocumentApproverMaster] CHECK CONSTRAINT [FK_DocumentApproverMaster_ApplicationPermission]

ALTER TABLE [dbo].[DocumentApproverMaster]  WITH CHECK ADD  CONSTRAINT [FK_DocumentApproverMaster_DocumentMaster] FOREIGN KEY([DocumentId])
REFERENCES [dbo].[DocumentMaster] ([Id])

ALTER TABLE [dbo].[DocumentApproverMaster] CHECK CONSTRAINT [FK_DocumentApproverMaster_DocumentMaster]

ALTER TABLE [dbo].[DocumentMaster]  WITH CHECK ADD  CONSTRAINT [FK_DocumentMaster_ApplicationPermission] FOREIGN KEY([UploadedByPermissionId])
REFERENCES [dbo].[ApplicationPermission] ([PermissionId])

ALTER TABLE [dbo].[DocumentMaster] CHECK CONSTRAINT [FK_DocumentMaster_ApplicationPermission]

ALTER TABLE [dbo].[DocumentMaster]  WITH CHECK ADD  CONSTRAINT [FK_DocumentMaster_DocumentType] FOREIGN KEY([DocumentType])
REFERENCES [dbo].[DocumentType] ([Id])

ALTER TABLE [dbo].[DocumentMaster] CHECK CONSTRAINT [FK_DocumentMaster_DocumentType]

ALTER TABLE [dbo].[SharepointMaster]  WITH CHECK ADD  CONSTRAINT [FK_SharepointMaster_DocumentMaster] FOREIGN KEY([DocumentMasterId])
REFERENCES [dbo].[DocumentMaster] ([Id])

ALTER TABLE [dbo].[SharepointMaster] CHECK CONSTRAINT [FK_SharepointMaster_DocumentMaster]

ALTER TABLE [dbo].[TaskComment]  WITH CHECK ADD  CONSTRAINT [FK_TaskComment_TaskMaster] FOREIGN KEY([TaskId])
REFERENCES [dbo].[TaskMaster] ([Id])

ALTER TABLE [dbo].[TaskComment] CHECK CONSTRAINT [FK_TaskComment_TaskMaster]

ALTER TABLE [dbo].[TaskMaster]  WITH CHECK ADD  CONSTRAINT [FK_TaskMaster_ApplicationPermission] FOREIGN KEY([OwnerPermissionId])
REFERENCES [dbo].[ApplicationPermission] ([PermissionId])

ALTER TABLE [dbo].[TaskMaster] CHECK CONSTRAINT [FK_TaskMaster_ApplicationPermission]

ALTER TABLE [dbo].[TaskMaster]  WITH CHECK ADD  CONSTRAINT [FK_TaskMaster_ApplicationPermission1] FOREIGN KEY([AssignedToPermissionId])
REFERENCES [dbo].[ApplicationPermission] ([PermissionId])

ALTER TABLE [dbo].[TaskMaster] CHECK CONSTRAINT [FK_TaskMaster_ApplicationPermission1]

ALTER TABLE [dbo].[TaskMaster]  WITH CHECK ADD  CONSTRAINT [FK_TaskMaster_DocumentMaster] FOREIGN KEY([MasterDocumentId])
REFERENCES [dbo].[DocumentMaster] ([Id])

ALTER TABLE [dbo].[TaskMaster] CHECK CONSTRAINT [FK_TaskMaster_DocumentMaster]

ALTER TABLE [dbo].[TaskMaster]  WITH CHECK ADD  CONSTRAINT [FK_TaskMaster_TaskStatus] FOREIGN KEY([TaskStatusId])
REFERENCES [dbo].[TaskStatus] ([Id])

ALTER TABLE [dbo].[TaskMaster] CHECK CONSTRAINT [FK_TaskMaster_TaskStatus]

ALTER TABLE [dbo].[WorkflowInstance]  WITH CHECK ADD  CONSTRAINT [FK_WorkflowInstance_ApplicationPermission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[ApplicationPermission] ([PermissionId])

ALTER TABLE [dbo].[WorkflowInstance] CHECK CONSTRAINT [FK_WorkflowInstance_ApplicationPermission]

ALTER TABLE [dbo].[WorkflowInstance]  WITH CHECK ADD  CONSTRAINT [FK_WorkflowInstance_WorkflowMaster] FOREIGN KEY([WorkflowMasterId])
REFERENCES [dbo].[WorkflowMaster] ([Id])

ALTER TABLE [dbo].[WorkflowInstance] CHECK CONSTRAINT [FK_WorkflowInstance_WorkflowMaster]

ALTER TABLE [dbo].[WorkflowInstance]  WITH CHECK ADD  CONSTRAINT [FK_WorkflowInstance_WorkflowStatus] FOREIGN KEY([CurrentStatusId])
REFERENCES [dbo].[WorkflowStatus] ([ID])

ALTER TABLE [dbo].[WorkflowInstance] CHECK CONSTRAINT [FK_WorkflowInstance_WorkflowStatus]

ALTER TABLE [dbo].[WorkflowInstance]  WITH CHECK ADD  CONSTRAINT [FK_WorkflowInstance_WorkflowSteps] FOREIGN KEY([CurrentWorkflowStepId])
REFERENCES [dbo].[WorkflowSteps] ([Id])

ALTER TABLE [dbo].[WorkflowInstance] CHECK CONSTRAINT [FK_WorkflowInstance_WorkflowSteps]

ALTER TABLE [dbo].[WorkflowStepApprovers]  WITH CHECK ADD  CONSTRAINT [FK_WorkflowStepApprovers_ApproverGroupMaster] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[ApplicationPermission] ([PermissionId])

ALTER TABLE [dbo].[WorkflowStepApprovers] CHECK CONSTRAINT [FK_WorkflowStepApprovers_ApproverGroupMaster]

ALTER TABLE [dbo].[WorkflowStepApprovers]  WITH CHECK ADD  CONSTRAINT [FK_WorkflowStepApprovers_WorkflowSteps] FOREIGN KEY([WorkflowStepId])
REFERENCES [dbo].[WorkflowSteps] ([Id])

ALTER TABLE [dbo].[WorkflowStepApprovers] CHECK CONSTRAINT [FK_WorkflowStepApprovers_WorkflowSteps]

ALTER TABLE [dbo].[WorkflowSteps]  WITH CHECK ADD  CONSTRAINT [FK_WorkflowSteps_WorkflowMaster] FOREIGN KEY([WorkflowMasterId])
REFERENCES [dbo].[WorkflowMaster] ([Id])

ALTER TABLE [dbo].[WorkflowSteps] CHECK CONSTRAINT [FK_WorkflowSteps_WorkflowMaster]
