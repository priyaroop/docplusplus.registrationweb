﻿
CREATE PROCEDURE [dbo].[sp_getSelectedtrialBalanceDetails] 
	
	@SubCategoryId int

AS
BEGIN
	
	SET NOCOUNT ON;

   select * from [dbo].[ReportTrialBalance] tb
   INNER JOIN [dbo].[ReportSubCategory] sc ON tb.SubCategoryId = sc.Id
   where sc.Id = @SubCategoryId

END

