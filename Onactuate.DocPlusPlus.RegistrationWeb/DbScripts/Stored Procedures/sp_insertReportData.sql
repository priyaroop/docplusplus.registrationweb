﻿
CREATE PROCEDURE [dbo].[sp_insertReportData] 
	@reportId int,
	@categoryId int,
	@subCategoryId int,
	@columnId int,
	@trialBalanceId int,
	@CurrentDate DateTime
	
AS
BEGIN
	
	SET NOCOUNT ON;

	INSERT INTO ReportCafrMap (ReportId, CategoryId , SubCategoryId,ColumnId,TrialBalanceId,CurrentDate) 
    VALUES (@reportId, @categoryId, @subCategoryId,@columnId,@trialBalanceId,@CurrentDate)
    
END
