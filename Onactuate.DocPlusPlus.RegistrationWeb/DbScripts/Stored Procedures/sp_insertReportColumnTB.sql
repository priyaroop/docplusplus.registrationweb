﻿
CREATE PROCEDURE [dbo].[sp_insertReportColumnTB] 
	@reportId int,
	@columnId int,
	@trialBalanceId int,
	@CurrentDate DateTime
	
AS
BEGIN
	
	SET NOCOUNT ON;

	INSERT INTO [dbo].[ReportColumnTB](ReportId,ColumnId,TrialBalanceId,CurrentDate)
	VALUES (@reportId,@columnId,@trialBalanceId,@CurrentDate);

    
END
