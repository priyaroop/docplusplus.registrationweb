﻿
CREATE PROCEDURE [dbo].[sp_getSelectedSubCategory] 
	
	@CategoryId int

AS
BEGIN
	
	SET NOCOUNT ON;

   select * from [dbo].[ReportSubCategory] rs
   INNER JOIN [dbo].[ReportCategory] rc ON rs.CategoryId = rc.Id
   where rc.Id = @CategoryId

END

