﻿using System.Web.Mvc;

namespace Onactuate.DocPlusPlus.RegistrationWeb
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
