﻿using System.Web.Mvc;
using Onactuate.DocPlusPlus.RegistrationWeb.Controllers;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public static class WebViewPageExtensions
    {
        private const string HasUnhandledExceptionKey = "HasUnhandledException";

        /// <summary>
        /// Determines whether the hasunhandledexception is present in tempdata
        /// and the value is set to true
        /// </summary>
        /// <param name="viewPage">The view page.</param>
        public static bool HasUnhandledException(this WebViewPage viewPage)
        {
            return viewPage.TempData[HasUnhandledExceptionKey] != null &&
                   (bool)viewPage.TempData[HasUnhandledExceptionKey] == true;


        }

        /// <summary>
        /// Determines whether the hasunhandledexception is present in tempdata
        /// and the value is set to true
        /// </summary>
        /// <param name="viewPage">The view page.</param>
        public static void SetHasUnhandledException(this WebViewPage viewPage, bool isUnhandled)
        {
            viewPage.TempData[HasUnhandledExceptionKey] = isUnhandled;

        }


        /// <summary>
        /// Gets the partial view associated with the errorContext (specified in the inlineErrorAttribute)
        /// </summary>
        /// <param name="viewPage"></param>
        /// <returns></returns>
        public static string ErrorPartialView(this WebViewPage viewPage)
        {
            ErrorContext context = viewPage.TempData[ErrorContext.ERRORCONTEXTKEY] as ErrorContext;
            return context == null ? null : context.PartialView;
        }

        /// <summary>
        /// Propogates the errorContext to the next page, since the scope of TEMPDATA is only for a current request at the next, 
        /// we need to set again if we need to propagate
        /// </summary>
        /// <param name="viewPage"></param>
        public static void PropogateErrorContext(this WebViewPage viewPage)
        {
            //to be used only on the partial error views
            ErrorContext context = viewPage.TempData[ErrorContext.ERRORCONTEXTKEY] as ErrorContext;
            if (context != null)
            {
                //context.HasUnhandledException = false;
                viewPage.TempData.Keep(ErrorContext.ERRORCONTEXTKEY);
            }
        }
    }
}