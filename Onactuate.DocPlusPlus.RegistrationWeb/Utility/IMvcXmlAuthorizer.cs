﻿using System;
using System.Security.Principal;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public interface IMvcXmlAuthorizer
    {
        bool IsAuthorized(String controllerName, String actionName, IPrincipal user);
        bool IsAuthorized(String controllerName, IPrincipal user);

        ActionAuthorizationInfo GetAction(String controllerName, String actionName);
        string GetParamsForControllerAction(String controllerName, String actionName, IPrincipal user);
        string[] GetParamsArrayForControllerAction(String controllerName, String actionName, IPrincipal user);
    }
}