﻿using System;
using System.Linq;
using System.Xml.Serialization;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public abstract class AuthorizationInfo
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [XmlAttribute("name")]
        public string Name { get; set; }
        
        /// <summary>
        /// Gets or sets the roles.
        /// </summary>
        /// <value>The roles.</value>
        [XmlArray(ElementName = "roles"), XmlArrayItem(ElementName = "role")]
        public string[] Roles { get; set; }

        [XmlArray(ElementName = "users"), XmlArrayItem(ElementName = "user")]
        public string[] Users { get; set; }

        [XmlArray(ElementName = "additionalparams"), XmlArrayItem(ElementName = "param")]
        public AdditionalParameter[] AdditionalParameters { get; set; }

        /// <summary>
        /// Determines whether [is in any roles] [the specified user].
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>
        /// 	<c>true</c> if [is in any roles] [the specified user]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsAllowUser(System.Security.Principal.IPrincipal user)
        {
            if (user == null)
                return false;

            // if no roles or users are specified, deny access
            if ((Roles == null || Roles.Length == 0) && (Users == null || Users.Length == 0))
            {
                return false;
            }

            if (Users != null)
            {
                if (Users.Contains(user.Identity.Name, StringComparer.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }
         
            if (Roles != null)
            {
                return Roles.Any(user.IsInRole);
            }

            return false;
        }
    }
}