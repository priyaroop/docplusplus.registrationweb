﻿using System.Collections.Generic;
using Onactuate.DocPlusPlus.RegistrationWeb.Providers;
using Onactuate.Notification.Core.Constants;
using Onactuate.Notification.Core.NotificationAPI;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility.EmailNotifier
{
    public class BusinessNotificationAdapter
    {
        private AbstractNotificationController notificationController { get; set; }

        private EmailNotificationProvider notificationProvider { get; set; }
        private IEmailConfiguration emailConfig { get; set; }

        public BusinessNotificationAdapter()
        {
            emailConfig = new EmailConfiguration();

            notificationController = new BusinessNotificationController(emailConfig.ConfigKeys);
            notificationProvider = new EmailNotificationProvider(emailConfig.ConfigKeys);
        }

        public BusinessNotificationAdapter(AbstractNotificationController notificationController, EmailNotificationProvider notificationProvider)
        {
            this.notificationController = notificationController;
            this.notificationProvider = notificationProvider;
        }

        public void SendRegistrationSuccessNotification(
            IList<string> toEmailAddressList,
            List<KeyValuePair<string, string>> placeholdersDataForSubject,
            List<KeyValuePair<string, string>> placeholdersDataForBody)
        {
            notificationController.PrepareAndSendEmail(notificationProvider, NotificaionType.RegistrationSuccessNotification, toEmailAddressList, placeholdersDataForBody, placeholdersDataForSubject, null);
        }
        
        public void SendRegistrationErrorNotification(
            IList<string> toEmailAddressList,
            List<KeyValuePair<string, string>> placeholdersDataForSubject,
            List<KeyValuePair<string, string>> placeholdersDataForBody)
        {
            notificationController.PrepareAndSendEmail(notificationProvider, NotificaionType.RegistrationErrorNotification, toEmailAddressList, placeholdersDataForBody, placeholdersDataForSubject, null);
        }
    }
}