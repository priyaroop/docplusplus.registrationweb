﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Onactuate.Notification.Core.Constants;
using Onactuate.Notification.Core.NotificationAPI;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility.EmailNotifier
{
    public class BusinessNotificationController : AbstractNotificationController
    {
        private INotificationConfiguration Config { get; set; }
        private EmailNotificationProvider NotificationProviderObj { get; set; }

        public BusinessNotificationController(Dictionary<string, string> configuration)
        {
            Config = new NotificationConfigurationBase(configuration);
            NotificationProviderObj = new EmailNotificationProvider(configuration);
        }
        
        /// <summary>
        /// This method returns a subject template.
        /// </summary>
        /// <param name="typeOfNotification">This is the type of notification.</param>
        /// <returns>This is the subject template</returns>
        protected override string GetSubjectTemplate(NotificaionType typeOfNotification)
        {
            switch (typeOfNotification)
            {
                case NotificaionType.RegistrationErrorNotification:
                    return Config.RegistrationErrorNotificationSubject;
                    break;
                case NotificaionType.RegistrationSuccessNotification:
                    return Config.RegistrationSuccessNotificationSubject;
                    break;
            }
            throw new ArgumentException();
        }

        /// <summary>
        /// This method returns a body template.
        /// </summary>
        /// <param name="typeOfNotification">This is the type of notification.</param>
        /// <returns>This is the body template</returns>
        protected override string GetBodyTemplate(NotificaionType typeOfNotification)
        {
            switch (typeOfNotification)
            {
                case NotificaionType.RegistrationErrorNotification:
                    return Config.RegistrationErrorNotificationBody;
                    break;
                case NotificaionType.RegistrationSuccessNotification:
                    return Config.RegistrationSuccessNotificationBody;
                    break;
            }
            throw new ArgumentException();
        }

        /// <summary>
        /// This method returns the subject data after replacing the placeholder
        /// </summary>
        /// <param name="subjectTemplate">The subject template</param>
        /// <param name="placeholdersDataForSubject">The placeholder data</param>
        /// <returns>The subject</returns>
        protected override string ReplaceSubjectPlaceholders(string subjectTemplate, IList<KeyValuePair<string, string>> placeholdersDataForSubject)
        {
            return this.ReplacePlaceHolders(subjectTemplate, placeholdersDataForSubject);
        }
        /// <summary>
        /// This method returns the subject data after replacing the placeholder
        /// </summary>
        /// <param name="bodyTemplate">The body template</param>
        /// <param name="placeholdersDataForBody">The placeholder data</param>
        /// <returns>The subject</returns>
        protected override string ReplaceBodyPlaceholders(string bodyTemplate, IList<KeyValuePair<string, string>> placeholdersDataForBody)
        {
            return this.ReplacePlaceHolders(bodyTemplate, placeholdersDataForBody);
        }

        /// <summary>
        /// This method replaces the placeholder data in the template
        /// </summary>
        /// <param name="template">The template string</param>
        /// <param name="placeholdersData">placeholder data</param>
        /// <returns>The modified template</returns>
        private string ReplacePlaceHolders(string template, IList<KeyValuePair<string, string>> placeholdersData)
        {
            StringBuilder sb = new StringBuilder(template);
            foreach (KeyValuePair<string, string> token in placeholdersData)
            {
                sb = sb.Replace(token.Key, HttpUtility.HtmlEncode(token.Value));
            }
            return sb.ToString();
        }
    }
}