﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    [XmlRoot("controllers")]
    public class ControllerAuthorizationInfoCollection : List<ControllerAuthorizationInfo>
    {
        /// <summary>
        /// Determines whether user can access specific controller.
        /// </summary>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="user">The user.</param>
        /// <returns>
        /// 	<c>true</c> if this instance [can access controller] the specified controller name; otherwise, <c>false</c>.
        /// </returns>
        public bool CanAccessController(String controllerName, System.Security.Principal.IPrincipal user)
        {
            ControllerAuthorizationInfo controllerInfo = FindController(controllerName);
            //if the controller wasn't specified in the config, deny access.
            if (null == controllerInfo)
            {
                return false;
            }
            return controllerInfo.IsAllowUser(user);
        }

        /// <summary>
        /// Determines whether user can access specific controller and action.
        /// </summary>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <param name="user">The user.</param>
        /// <returns>
        /// 	<c>true</c> if this instance [can access action] the specified controller name; otherwise, <c>false</c>.
        /// </returns>
        public bool CanAccessAction(String controllerName, String actionName, System.Security.Principal.IPrincipal user)
        {
            ControllerAuthorizationInfo controllerInfo = FindController(controllerName);
            //if the controller wasn't specified in the config, deny access.
            if (null == controllerInfo)
            {
                return false;
            }
            
            // if user can not access controller then he should not have access to action.
            if (controllerInfo.IsAllowUser(user) == false)
            {
                return false;
            }

            ActionAuthorizationInfo actionInfo = controllerInfo.FindAction(actionName);
            if (actionInfo == null)
            {
                // If the action wasn't specified in the config, deny access.
                // Could not find the action
                return false;
            }

            if (actionInfo.IsAllowUser(user) == false)
            {
                // If found the action then check if the user is allowed or not
                return false;
            }

            return true;
        }

        public ActionAuthorizationInfo GetAction(String controllerName, String actionName)
        {
            ControllerAuthorizationInfo controllerInfo = FindController(controllerName);
            if (controllerInfo != null)
            {
                return controllerInfo.FindAction(actionName);
            }

            return null;
        }

        protected ControllerAuthorizationInfo FindController(String controllerName)
        {
            return this.FirstOrDefault(info => info.Name.Equals(controllerName, StringComparison.OrdinalIgnoreCase));
        }
    }
}
