﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Onactuate.DocPlusPlus.RegistrationWeb.Providers;
using RestSharp;
using RestSharp.Authenticators;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public static class NameApiHelper
    {
        private static string _nameApiRestUrl;
        private static string _nameApiTokenName;
        private static string _nameApiToken;
        private static string _nameApiLoginUsername;
        private static string _nameApiLoginPassword;
        private static string _domainName;
        private static string _ipAddress;
        
        internal static async Task<HttpResponseMessage> CreateDnsRecord(string tenantName, IDatabaseConfiguration configuration)
        {
            _nameApiRestUrl = configuration.NameApiRestUrl;
            _nameApiTokenName = configuration.NameApiTokenName;
            _nameApiToken = configuration.NameApiToken;
            _nameApiLoginUsername = configuration.NameApiLoginUsername;
            _nameApiLoginPassword = configuration.NameApiLoginPassword;
            _domainName = configuration.WebDomainName;
            _ipAddress = configuration.MachineIpAddress;

            string endpointUrl = $"{_nameApiRestUrl}/v4/domains/{_domainName}/records";
            using (HttpClient httpClient = new HttpClient())
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                using (HttpRequestMessage request = new HttpRequestMessage(new HttpMethod("POST"), endpointUrl))
                {
                    string base64authorization = Convert.ToBase64String(Encoding.ASCII.GetBytes( $"{_nameApiLoginUsername}:{_nameApiToken}"));
                    request.Headers.TryAddWithoutValidation("Authorization", $"Basic {base64authorization}"); 
                    request.Content = new StringContent($"{{\"host\":\"{tenantName}\",\"type\":\"A\",\"answer\":\"{_ipAddress}\",\"ttl\":300}}", Encoding.UTF8, "application/x-www-form-urlencoded"); 

                    HttpResponseMessage response = await httpClient.SendAsync(request);
                    if (response.IsSuccessStatusCode)  
                    {  
                        return response;
                    }
                    else
                    {
                        throw new Exception(response.StatusCode.ToString());
                    }
                }
            }

            //RestClient restClient = new RestClient(endpointUrl);
            //ServicePointManager.Expect100Continue = true;
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            //RestRequest request = new RestRequest(Method.POST);
            ////string base64authorization = Convert.ToBase64String(Encoding.ASCII.GetBytes("Priyaroop:4964d492c1f8becbd76f3bd103051dc159d050fe"));
            ////request.AddHeader("Authorization", $"Priyaroop:4964d492c1f8becbd76f3bd103051dc159d050fe");

            ////request.Headers.TryAddWithoutValidation("Authorization", $"Basic {base64authorization}"); 
            ////request.AddHeader(NameApiLoginUsername, NameApiToken);

            //request.AddParameter("host", tenantName, ParameterType.RequestBody);
            //request.AddParameter("type", "A", ParameterType.RequestBody);
            //request.AddParameter("answer", IpAddress, ParameterType.RequestBody);
            //request.AddParameter("ttl", "300", ParameterType.RequestBody);
            //IRestResponse response = restClient.Execute(request);
            //var content = response.Content;

            //End point - /v4/domains/{domainName}/records
            //curl -u 'username:token' 'https://api.dev.name.com/v4/domains/example.org/records' -X POST --data '{"host":"www","type":"A","answer":"10.0.0.1","ttl":300}'
        }
    }
}