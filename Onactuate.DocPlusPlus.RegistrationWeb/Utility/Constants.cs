﻿using System.Collections.Generic;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public static class Constants
    {
        public static class Controllers
        {
            public const string HomeController = "Home";
            public const string AdminController = "Admin";
            public const string EnvConfigController = "EnvConfig";
        }

        public static class ControllerActions
        {
            public const string Dashboard = "Dashboard";
            public const string AdminPage = "AdminPage";
            public const string DefaultPage = "Default";
            public const string EditEnvironmentPage = "EditEnvironment";
            public const string EnvironmentPermissionsPage = "EnvironmentPermissions";
        }

        public static class AppSettingsKeys
        {
            public const string EnvConfigRestServiceBaseUrlKey = "EnvConfigRestServiceBaseUrl";
            public const string AuthenticationUsernameKey = "AuthenticationUsername";
            public const string AuthenticationPasswordKey = "AuthenticationPassword";

        }

        public static class EnvironmentTokens
        {
            public const string EmailSettingsToken = "EmailSettings";
            public const string CreateWebSettingsToken = "CreateWebSettings";
        }

        public static class EnvironmentKeys
        {
            public const string DomainForLoginKey = "DomainForLogin";
            public const string WindowsUserNameKey = "WindowsUserName";
            public const string WindowsUserPasswordKey = "WindowsUserPassword";
            public const string WebsiteMetabasePathKey = "WebsiteMetabasePath";
            public const string WebsiteRootPathKey = "WebsiteRootPath";
            public const string PublishWebPathKey = "PublishWebPath";
            public const string MachineIpAddressKey = "MachineIpAddress";
            public const string WebDomainNameKey = "WebDomainName";
            public const string NameApiRestUrlKey = "NameApiRestUrl";
            public const string NameApiTokenNameKey = "NameApiTokenName";
            public const string NameApiTokenKey = "NameApiToken";
            public const string NameApiLoginUsernameKey = "NameApiLoginUsername";
            public const string NameApiLoginPasswordKey = "NameApiLoginPassword";
            public const string DatabaseServerKey = "DatabaseServer";
            public const string DatabaseAdminUserKey = "DatabaseAdminUser";
            public const string DatabaseAdminPasswordKey = "DatabaseAdminPassword";


            public const string NotificationMailFormatConfig = "MailFormat";
            public const string NotificationMailPriorityConfig = "MailPriority";
            public const string NotificationMailEncodingConfig = "MailEncoding";
            public const string NotificationSmtpServerConfig = "SmtpServer";
            public const string NotificationAuthenticationTypeConfig = "AuthenticationType";
            public const string NotificationSmtpPortConfig = "SmtpPort";
            public const string NotificationSmtpUserNameConfig = "SmtpUserName";
            public const string NotificationSmtpPasswordConfig = "SmtpPassword";
            public const string NotificationSmtpMachineDomainConfig = "SmtpMachineDomain";

            public const string ATTACHEMNT_FILE_NOT_FOUND_Config = "ATTACHEMNT_FILE_NOT_FOUND";
            public const string AUTHENTICATION_TYPE_NOT_PROVIDED_Config = "AUTHENTICATION_TYPE_NOT_PROVIDED";
            public const string INVALID_EMAIL_ADDRESS_Config = "INVALID_EMAIL_ADDRESS";
            public const string NO_RECIPIENTS_Config = "NO_RECIPIENTS";
            public const string NO_SENDER_PROVIDED_Config = "NO_SENDER_PROVIDED";
            public const string PASSWORD_NAME_NOT_PROVIDED_Config = "PASSWORD_NAME_NOT_PROVIDED";
            public const string SMTP_SERVER_NOT_PROVIDED_Config = "SMTP_SERVER_NOT_PROVIDED";
            public const string USER_NAME_NOT_PROVIDED_Config = "USER_NAME_NOT_PROVIDED";

            public const string FromEmailAddressConfig = "FromEmailAddress";

            public const string RegistrationSuccessNotificationSubjectConfig = "RegistrationSuccessNotificationSubject";
            public const string RegistrationErrorNotificationSubjectConfig = "RegistrationErrorNotificationSubject";
            public const string RegistrationErrorNotificationBodyConfig = "RegistrationErrorNotificationBody";
            public const string RegistrationSuccessNotificationBodyConfig = "RegistrationSuccessNotificationBody";
            public const string SendRegistrationErrorEmailConfig = "SendRegistrationErrorEmail";
            public const string SendRegistrationSuccessEmailConfig = "SendRegistrationSuccessEmail";
        }

        public const string ExpandIconImageName = "expand.png";
        public const string ExpandIconNoActionImageName = "cannotToggle.png";
        public const string CollapseIconImageName = "collapse.png";

        public const string ActionForExpand = "expand";
        public const string ActionForCollapse = "collapse";

        public static readonly List<string> DisplayImageList = new List<string>() { "product.png", "productLineItem.png", "user.png" };
        public static readonly List<string> CssStyleClasses = new List<string>() { "css_grid_rowLevelOne", "css_grid_rowLevelTwo", "css_grid_rowLevelThree" };

        public const string FlexigridRowIdListPost = "rowidlist";
        public const string FlexigridActionTypeListPost = "actiontypelist";

        public const int MaxQuestionsInQuiz = 10;
    }
}