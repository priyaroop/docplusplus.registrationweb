﻿using System.Web.Mvc;
using Onactuate.DocPlusPlus.RegistrationWeb.Controllers;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public static class ControllerBaseExtension
    {
        private const string HasUnhandledExceptionKey = "HasUnhandledException";

        /// <summary>
        /// Determines whether the hasunhandledexception is present in tempdata
        /// and the value is set to true
        /// </summary>
        /// <param name="viewPage">The view page.</param>
        public static void SetUnhandledException(this ControllerBase viewPage, bool isUnhandled)
        {
            viewPage.TempData[HasUnhandledExceptionKey] = isUnhandled;
        }

        /// <summary>
        /// Gets the errorContext from temp data. This is available purely for the next request
        /// </summary>
        /// <param name="controller"></param>
        /// <returns></returns>
        public static ErrorContext GetCurrentErrorContext(this ControllerBase controller)
        {
            return controller.TempData[ErrorContext.ERRORCONTEXTKEY] as ErrorContext;
        }

         
        /// <summary>
        /// Sets the error context in temp data to be available for the next request
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="errorContext"></param>
        public static void SetCurrentErrorContext(this ControllerBase controller, ErrorContext errorContext)
        {
            controller.TempData[ErrorContext.ERRORCONTEXTKEY] = errorContext;
        }

        /// <summary>
        /// Propogates the errorContext to the next page, since the scope of TEMPDATA is only for a current request at the next, 
        /// we need to set again if we need to propagate
        /// </summary>
        /// <param name="viewPage"></param>
        public static void PropogateErrorContext(this ControllerBase viewPage)
        {
            //to be used only on the partial error views
            ErrorContext context = viewPage.TempData[ErrorContext.ERRORCONTEXTKEY] as ErrorContext;
            if (context != null)
            {
                //context.HasUnhandledException = false;
                viewPage.TempData.Keep(ErrorContext.ERRORCONTEXTKEY);
            }
        }
    }
}
