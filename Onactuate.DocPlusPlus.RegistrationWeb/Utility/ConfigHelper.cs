﻿using System.Configuration;
using System.IO;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public static class ConfigHelper
    {
        
        private static Configuration OpenConfig(string path)
        {
            return ConfigurationManager.OpenMappedExeConfiguration(
                new ExeConfigurationFileMap() {  ExeConfigFilename = path }, 
                ConfigurationUserLevel.None);   
        }

        internal static void ConfigureNewConnectionString(string configFile, string databaseServer, string database, string databaseUser, string databasePassword)
        {
            string connectionString = $"Server={databaseServer};Initial Catalog={database};Persist Security Info=False;User ID={databaseUser};Password={databasePassword};MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;";

            Configuration webConfiguration = OpenConfig(configFile);
            ConnectionStringsSection conSetting = (ConnectionStringsSection)webConfiguration.GetSection("connectionStrings");
            ConnectionStringSettings stringSettings = new ConnectionStringSettings("DbConnectionString", connectionString);
            stringSettings.ProviderName = "System.Data.SqlClient";
            conSetting.ConnectionStrings.Remove(stringSettings);
            conSetting.ConnectionStrings.Add(stringSettings);
            webConfiguration.Save(ConfigurationSaveMode.Modified);
        }

        public static void ConfigureDocumentsPath(string configFile, string registrationDataPhysicalPath)
        {
            Configuration webConfiguration = OpenConfig(configFile);
            webConfiguration.AppSettings.Settings["DocumentRoot"].Value = Path.Combine(registrationDataPhysicalPath,"Documents");
            webConfiguration.Save(ConfigurationSaveMode.Modified);
        }
    }
}