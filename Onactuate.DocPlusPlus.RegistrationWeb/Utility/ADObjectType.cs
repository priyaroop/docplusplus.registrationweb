﻿namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public enum ADObjectType
    {
        UsersAndGroups,
        UsersOnly,
        OneUserOnly
    }
}