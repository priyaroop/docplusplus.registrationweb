﻿using System;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Net.Mime;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public static class DirectoryHelper
    {
        public static void Copy(string sourceDirectory, string targetDirectory)
        {
            DirectoryInfo diSource = new DirectoryInfo(sourceDirectory);
            DirectoryInfo diTarget = new DirectoryInfo(targetDirectory);

            CopyAll(diSource, diTarget);
        }

        public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }

        internal static bool CreateUser(string firstName, string lastName, string emailAddress, string userLogonName, string password, string domain)
        {
            using (PrincipalContext principalContext = new PrincipalContext(ContextType.Domain, domain, $"DC={domain},DC=com"))
            {
                // Check if user object already exists in the store
                using (UserPrincipal usr = UserPrincipal.FindByIdentity(principalContext, userLogonName))
                {
                    if (usr != null)
                    {
                        throw new Exception(userLogonName + " already exists. Please use a different User Logon Name.");
                    }
                }
                    
                // Create the new UserPrincipal object
                using (UserPrincipal userPrincipal = new UserPrincipal(principalContext))
                {
                    if (!string.IsNullOrEmpty(lastName))
                        userPrincipal.Surname = lastName;

                    if (!string.IsNullOrEmpty(firstName))
                        userPrincipal.GivenName = firstName;

                    if (!string.IsNullOrEmpty(emailAddress))
                        userPrincipal.EmailAddress = emailAddress;

                    if (userLogonName.Length > 0)
                        userPrincipal.SamAccountName = userLogonName;

                    userPrincipal.SetPassword(password);
                    userPrincipal.Enabled = true;
                    userPrincipal.PasswordNeverExpires = true;

                    userPrincipal.Save();
                }

            }

            return true;
        }

        internal static bool IsUsernameNew(string userLogonName, string domain)
        {
            using (PrincipalContext principalContext = new PrincipalContext(ContextType.Domain, domain, $"DC={domain},DC=com"))
            {
                // Check if user object already exists in the store
                using (UserPrincipal usr = UserPrincipal.FindByIdentity(principalContext, userLogonName))
                {
                    if (usr != null)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}