﻿using System;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using Microsoft.Practices.Unity.Configuration;
using Unity;
using Unity.Registration;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public static class UnityExtensions
    {
        public static void ConfigureCloudRegistrations(this IUnityContainer container)
        {
            container.ConfigureCloudRegistrations(new string[0]);
        }

        public static void ConfigureCloudRegistrations(this IUnityContainer container, params string[] containerNames)
        {
            Action<string> action = null;
            UnityConfigurationSection section = (UnityConfigurationSection) ConfigurationManager.GetSection("unity");
            if (section != null)
            {
                section.Configure(container, "StandardRegistrations");
                if (action == null)
                {
                    action = (Action<string>) (name => section.Configure(container, name));
                }
                (from name in containerNames
                    where name != "StandardRegistrations"
                    select name).ToList<string>().ForEach(action);
            }
        }

        public static void ConfigureSpecificRegistration(this IUnityContainer container, string containerName)
        {
            UnityConfigurationSection section = (UnityConfigurationSection) ConfigurationManager.GetSection("unity");
            if (section != null)
            {
                section.Configure(container, containerName);
            }
        }

        public static void RegisterWcfServices(this IUnityContainer container)
        {
            container.RegisterWcfServices(Assembly.GetCallingAssembly());
        }

        public static void RegisterWcfServices(this IUnityContainer container, Assembly scanAssembly)
        {
            (from t in scanAssembly.GetTypes()
                let intf = (from i in t.GetInterfaces()
                    where i.GetCustomAttributes(typeof(ServiceContractAttribute), true).Any<object>()
                    select i).FirstOrDefault<Type>()
                where !t.IsAbstract && ((intf != null) || t.GetCustomAttributes(typeof(ServiceContractAttribute), true).Any<object>())
                select new { Interface = intf ?? t, Implementation = t }).ToList().ForEach((service => container.RegisterType(service.Interface, service.Implementation, new InjectionMember[0])));
        }
    }
}

