﻿using System;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public class AuthorizationMappingSection : IConfigurationSectionHandler
    {
        public object Create(Object parent, Object configContext, System.Xml.XmlNode section)
        {
            // Create an instance of XmlSerializer based on the RewriterConfiguration type...
            XmlSerializer ser = new XmlSerializer(typeof(AuthorizationMappingSection));

            // Return the Deserialized object from the Web.config XML
            return ser.Deserialize(new XmlNodeReader(section));
        }

        public static AuthorizationMappingSection GetSettings()
        {
            return (AuthorizationMappingSection)ConfigurationManager.GetSection("AuthorizationMappingSection");
        }

        [XmlAttribute("type")]
        public String Type { get; set; }

        [XmlAttribute("path")]
        public String Path { get; set; }
    }
}