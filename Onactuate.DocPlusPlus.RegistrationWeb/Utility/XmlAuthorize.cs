using System;
using System.Web.Mvc;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public class XmlAuthorize : FilterAttribute, IAuthorizationFilter
    {
        #region Implementation of IAuthorizationFilter

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null) throw new ArgumentNullException("filterContext");

            var authorizer = new MvcXmlAuthorizer();
            if (!authorizer.IsAuthorized(filterContext.RouteData.Values["Controller"].ToString(), filterContext.HttpContext.User))
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        #endregion
    }
}