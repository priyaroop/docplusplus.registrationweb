﻿namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public static class DatabaseHelper
    {
        public static string GetConnectionString(string databaseServer, string databaseName, string databaseUser, string databasePassword)
        {
            string connectionString = $"Server={databaseServer};Initial Catalog={databaseName};User ID={databaseUser};Password={databasePassword};Persist Security Info=False;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;";
            return connectionString;
        }
    }
}