﻿using System;
using System.Data;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public class DpcEventManager
    {
        public DataTable FilteredData(DateTime start, DateTime end)
        {
            DataTable dt = new DataTable("event");
            dt.Columns.Add(new DataColumn("id", DbType.Int32.GetType()));
            dt.Columns.Add(new DataColumn("name", DbType.String.GetType()));
            dt.Columns.Add(new DataColumn("eventstart", DbType.DateTime.GetType()));
            dt.Columns.Add(new DataColumn("eventend", DbType.DateTime.GetType()));
            dt.Columns.Add(new DataColumn("resource", DbType.String.GetType()));

            DataRow row = dt.NewRow();
            row[0] = 1;
            row[1] = "Web Services";
            row[2] = DateTime.Now;
            row[3] = DateTime.Now.AddHours(2);
            row[4] = "Priyaoop Singh";
            dt.Rows.Add(row);
            //dt.Rows

            return dt;
        }
    }
}