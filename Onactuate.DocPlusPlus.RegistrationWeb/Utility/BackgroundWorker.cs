﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using Onactuate.Common.Configuration.ConfigSettings.ConfigEnvironment;
using Onactuate.Common.Configuration.Lektor;
using Onactuate.Common.DataAccess;
using Onactuate.DocPlusPlus.RegistrationWeb.Models;
using Onactuate.DocPlusPlus.RegistrationWeb.Utility.EmailNotifier;
using Onactuate.Notification.Core.Constants;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Platform.Services.SqlTableServices;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public class BackgroundWorker
    {
        #region Private Member Variables
        
        private ITenantMasterService TenantMasterServiceObj { get; set; }
        private TenantRegistrationModel RegistrationData { get; set; }
        private BusinessNotificationAdapter _businessNotificationAdapterObj;

        #endregion

        #region Class Constructor

        // TODO:: Remove this constructor and inject the ITenantMasterService from Unity IoC
        public BackgroundWorker() : this(new TenantMasterService())
        {
        }

        public BackgroundWorker(ITenantMasterService tenantMasterServiceObj)
        {
            TenantMasterServiceObj = tenantMasterServiceObj;
            _businessNotificationAdapterObj = new BusinessNotificationAdapter();
        }

        #endregion

        public async Task RegisterNewTenant(CancellationToken cancellationToken, TenantRegistrationModel registrationData)  
        {  
            try
            {
                RegistrationData = registrationData;

                // Step 1:: Create the website (DONE)
                await UpdateStatusInDatabase(RegistrationStatus.Inprogress, "Website deployment in progress");
                string websiteUrl = CreateWebsite(registrationData.TenantName, registrationData.PhysicalPath);

                // Step 2:: Create the database (DONE)
                await UpdateStatusInDatabase(RegistrationStatus.Inprogress, $"Website deployment complete.{Environment.NewLine} Database deployment in progress.");
                using (CodeFirstContext dbContext = new CodeFirstContext(registrationData.ConnectionString))
                {
                    string serverDbScriptsPath = HostingEnvironment.MapPath("~/DbScripts/");

                    // Step 3a:: Create default user in the database and give full read write access (DONE)
                    dbContext.CreateDatabaseUser(registrationData.TenantDbOwnerUsername, registrationData.TenantDbOwnerPassword);

                    // Step 3b:: Create tables in the database. Run table creation script
                    string tableCreationScriptFile = Path.Combine(serverDbScriptsPath, "CreateDbTables.sql");
                    string tableCreationScript;
                    using (StreamReader sr = new StreamReader(tableCreationScriptFile))
                    {
                        tableCreationScript = sr.ReadToEnd();
                    }
                    dbContext.RunSql(tableCreationScript);

                    // Step 3c:: Create Stored procedures
                    string sprocDbScriptsPath = HostingEnvironment.MapPath("~/DbScripts/Stored Procedures/");
                    DirectoryInfo sprocDirectory = new DirectoryInfo(sprocDbScriptsPath);
                    FileInfo[] sprocFiles = sprocDirectory.GetFiles("*.sql");
                    string sprocCreationScript;
                    foreach(FileInfo sproc in sprocFiles)
                    {
                        using (StreamReader sr = new StreamReader(sproc.FullName))
                        {
                            sprocCreationScript = sr.ReadToEnd();
                        }
                        dbContext.RunSql(sprocCreationScript);
                    }

                    // Step 3d:: Insert master data into tables. Run data insertion script
                    string dataInsertionScriptFile = Path.Combine(serverDbScriptsPath, "InsertMasterData.sql");
                    StringBuilder sqlString = new StringBuilder();
                    string encryptedPassword = EncryptionHelper.GetString(EncryptionHelper.EncryptText(registrationData.Password, EnvironmentSection.GetEnvironmentName));
                    sqlString.AppendLine("SET IDENTITY_INSERT [dbo].[ApplicationPermission] ON ");
                    sqlString.AppendLine($"INSERT [dbo].[ApplicationPermission] ([PermissionId], [UserName], [Password], [IsDeletable], [PermissionTypeId]) VALUES (1, N'{registrationData.Username}', N'{encryptedPassword}', 0, 0)");
                    sqlString.AppendLine("SET IDENTITY_INSERT [dbo].[ApplicationPermission] OFF");
                    using (StreamReader sr = new StreamReader(dataInsertionScriptFile))
                    {
                        sqlString.AppendLine(sr.ReadToEnd());
                    }
                    dbContext.RunSql(sqlString.ToString());
                }

                // Step 4:: Update the database connection string and documents save path in the web.config (DONE)
                string configFile = Path.Combine(registrationData.PhysicalPath, "web.config");
                ConfigHelper.ConfigureNewConnectionString(configFile, registrationData.DatabaseServer, registrationData.TenantDatabaseName, registrationData.TenantDbOwnerUsername, registrationData.TenantDbOwnerPassword);
                ConfigHelper.ConfigureDocumentsPath(configFile, registrationData.PhysicalPath);

                // Step 5:: Create the User in Active directory (DONE)
                // TODO : No more required. The user will be authenticated from the database. Delete this code
                //await UpdateStatusInDatabase(RegistrationStatus.Inprogress, $"Database deployment complete.{Environment.NewLine} User creation in progress.");
                //DirectoryHelper.CreateUser(registrationData.FirstName, registrationData.LastName, registrationData.EmailAddress, registrationData.Username, registrationData.Password, registrationData.DomainForLogin);

                // Step 6:: Create DNS entry in the Name.com (DONE)
                await UpdateStatusInDatabase(RegistrationStatus.Inprogress, $"User creation complete.{Environment.NewLine} Mapping url to DNS.");
                Task<HttpResponseMessage> response = NameApiHelper.CreateDnsRecord(registrationData.TenantName, registrationData.DatabaseConfigurationObj);

                // Step 7:: Make an entry in the CAFR365 Master database (DONE)
                await UpdateStatusInDatabase(RegistrationStatus.Inprogress, $"Finalizing settings. Please wait.");
                string tenantDbConnStr = $"Server={registrationData.DatabaseServer};Initial Catalog={registrationData.TenantDatabaseName};User ID={registrationData.TenantDbOwnerUsername};Password={registrationData.TenantDbOwnerPassword};Persist Security Info=False;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;";

                // TODO:: Remove after testing
                //string websiteUrl = string.Format("{0}.docplusplus.com", registrationData.TenantName);
                TenantMaster tenantMaster = new TenantMaster
                {
                    Firstname = registrationData.FirstName,
                    Lastname = registrationData.LastName,
                    Company = registrationData.CompanyName,
                    CompanyEmail = registrationData.EmailAddress,
                    AdminUsername = registrationData.Username,
                    AdminPassword = registrationData.Password,
                    PhoneNumber = registrationData.PhoneNumber,
                    SubscriptionPlan = Convert.ToInt32(registrationData.SubscriptionPlan),
                    ExpirationDate = DateTime.Now.AddMonths(1),
                    CafrUrl = websiteUrl,
                    CafrDatabase = registrationData.TenantDatabaseName,
                    CafrDatabaseConnStr = tenantDbConnStr
                };
                TenantMasterServiceObj.CreateTenantMaster(tenantMaster);
                RegistrationData.TenantRegistrationObj.TenantMasterId = tenantMaster.Id;

                // Step 8:: Send email
                await UpdateStatusInDatabase(RegistrationStatus.Completed, $"Registration completed successfully. Check your email for details.");
                SendNotificationEmail(
                    registrationData.FirstName,
                    registrationData.LastName,
                    registrationData.TenantName,
                    registrationData.EmailAddress,
                    websiteUrl,
                    registrationData.Username,
                    registrationData.Password,
                    NotificaionType.RegistrationSuccessNotification);
            }  
            catch (Exception ex)  
            {  
                ProcessCancellation(ex);
            }  
        }  
        private void ProcessCancellation(Exception ex)
        {  
            UpdateStatusInDatabase(RegistrationStatus.Error, $"Error in Registration. Contact system administrator.", ex.Message);
            
            // Step 8:: Send email
            SendNotificationEmail(
                RegistrationData.FirstName,
                RegistrationData.LastName,
                RegistrationData.TenantName,
                RegistrationData.EmailAddress,
                "",
                RegistrationData.Username,
                RegistrationData.Password,
                NotificaionType.RegistrationErrorNotification);
        }
 
        #region Private Helper Methods

        private string CreateWebsite(string tenantName, string physicalPath)
        {
            string websiteAddress = string.Empty;
            try
            {
                string dotNetVersion = "";
                string siteName = string.Format("CAFR 365 - {0}", tenantName);
                string hostHeader = string.Format("{0}.docplusplus.com", tenantName);

                string metabasePath = RegistrationData.DatabaseConfigurationObj.WebsiteMetabasePath;
                string publishWebPath = RegistrationData.DatabaseConfigurationObj.PublishWebPath;
                string ipAddress = RegistrationData.DatabaseConfigurationObj.MachineIpAddress;
                string windowsUserName = RegistrationData.DatabaseConfigurationObj.WindowsUserName;
                string windowsUserPassword = RegistrationData.DatabaseConfigurationObj.WindowsUserPassword;

                // Copy the website to the physical path
                Directory.CreateDirectory(physicalPath);
                DirectoryHelper.Copy(publishWebPath, physicalPath);

                using (WindowsSecurityAuthentication security = new WindowsSecurityAuthentication())
                {
                    if(security.ValidateUser(windowsUserName, windowsUserPassword, "", out WindowsIdentity loggedInUser, out _))
                    {
                        loggedInUser.Impersonate();
                        if (!WebsiteHelper.CreateSite(metabasePath, siteName, physicalPath, ipAddress, hostHeader,
                            dotNetVersion, windowsUserName, windowsUserPassword))
                        {
                            // Couldn't create website
                            throw new Exception("Could not create website");
                        }

                        websiteAddress = hostHeader;
                    }
                    else
                    {
                        throw new Exception($"Could not create website. Login failed for the windowsUserName {windowsUserName}");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return websiteAddress;
        }

        private async Task UpdateStatusInDatabase(RegistrationStatus statusCode, string statusMessage, string errorMessage=null)
        {
            RegistrationData.TenantRegistrationObj.RegistrationStatus = Convert.ToInt32(statusCode);
            RegistrationData.TenantRegistrationObj.RegistrationStatusMessage = statusMessage;
            RegistrationData.TenantRegistrationObj.ErrorMessage = errorMessage;

            await Task.Run(() => TenantMasterServiceObj.UpdateTenantRegistration(RegistrationData.TenantRegistrationObj));
        }
        
        private void SendNotificationEmail(
            string firstName, 
            string lastName, 
            string tenantName, 
            string emailAddress, 
            string cafrUrl, 
            string username, 
            string password, 
            NotificaionType notificationType)
        {
            List<KeyValuePair<string, string>> placeholdersDataForSubject = new List<KeyValuePair<string, string>>();
            List<KeyValuePair<string, string>> placeholdersDataForBody = new List<KeyValuePair<string, string>>();
            string dateOfRun = DateTime.Now.ToString("dd-MMM-yyyy - hh:mm:ss");

            placeholdersDataForSubject.Add(new KeyValuePair<string, string>("@dateofrun", dateOfRun));
            placeholdersDataForSubject.Add(new KeyValuePair<string, string>("@companyName", tenantName));
            placeholdersDataForSubject.Add(new KeyValuePair<string, string>("@firstname", firstName));
            placeholdersDataForSubject.Add(new KeyValuePair<string, string>("@lastname", lastName));
            placeholdersDataForSubject.Add(new KeyValuePair<string, string>("@cafrUrl", cafrUrl));
            placeholdersDataForSubject.Add(new KeyValuePair<string, string>("@username", username));
            placeholdersDataForSubject.Add(new KeyValuePair<string, string>("@password", password));

            placeholdersDataForBody.Add(new KeyValuePair<string, string>("@dateofrun", dateOfRun));
            placeholdersDataForBody.Add(new KeyValuePair<string, string>("@companyName", tenantName));
            placeholdersDataForBody.Add(new KeyValuePair<string, string>("@firstname", firstName));
            placeholdersDataForBody.Add(new KeyValuePair<string, string>("@lastname", lastName));
            placeholdersDataForBody.Add(new KeyValuePair<string, string>("@cafrUrl", cafrUrl));
            placeholdersDataForBody.Add(new KeyValuePair<string, string>("@username", username));
            placeholdersDataForBody.Add(new KeyValuePair<string, string>("@password", password));

            switch (notificationType)
            {
                case NotificaionType.RegistrationSuccessNotification:
                    _businessNotificationAdapterObj.SendRegistrationSuccessNotification(
                        emailAddress.Split(',').ToList(),
                        placeholdersDataForSubject,
                        placeholdersDataForBody);
                    break;
                case NotificaionType.RegistrationErrorNotification:
                    _businessNotificationAdapterObj.SendRegistrationErrorNotification(
                        emailAddress.Split(',').ToList(),
                        placeholdersDataForSubject,
                        placeholdersDataForBody);
                    break;
            }
        }

        #endregion
    }
}