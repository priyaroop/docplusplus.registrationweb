﻿using System.Collections.Generic;
using Onactuate.Common.Configuration;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public class ServiceConfigurationProvider
    {
        private readonly IDictionary<string, string> _configuration;
        private readonly string _environment;

        public ServiceConfigurationProvider()
        {
            _environment = GetDefaultEnvironment();
            ConfigurationProviderFactory factory = new ConfigurationProviderFactory();
            IConfigurationProvider envConfig = factory.GetConfigurationProvider();
            _configuration = envConfig.GetProperties(Constants.EnvironmentTokens.EmailSettingsToken, _environment);
        }

        /// <summary>
        /// Retrieves the name of the current environment as defined in Environment configuration's
        /// default configuration provider (namely, via the application domain's app.config)
        /// </summary>
        /// <returns></returns>
        private string GetDefaultEnvironment()
        {
            ConfigurationProviderFactory factory = new ConfigurationProviderFactory();
            IConfigurationProvider envConfig = factory.GetConfigurationProvider();
            return envConfig.EnvironmentName;
        }

        public string EnvironmentName
        {
            get
            {
                return _environment;
            }
        }
    }
}