﻿using System;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace Onactuate.DocPlusPlus.RegistrationWeb.Utility
{
    public class WindowsSecurityAuthentication : IDisposable
    {
        private SafeTokenHandle _safeTokenHandle;
        private const int LOGON32_LOGON_INTERACTIVE = 2;
        private const int LOGON32_PROVIDER_DEFAULT = 0;

        public void Dispose()
        {
            if (this._safeTokenHandle != null)
            {
                this._safeTokenHandle.Dispose();
            }
            GC.SuppressFinalize(this);
        }

        [DllImport("advapi32.dll", CharSet=CharSet.Unicode, SetLastError=true)]
        private static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, out SafeTokenHandle phToken);
        public bool ValidateUser(string userName, string password, string domain, out WindowsIdentity loggedInUser, out string sResult)
        {
            bool flag;
            sResult = string.Empty;
            loggedInUser = WindowsIdentity.GetCurrent();
            try
            {
                flag = LogonUser(userName, domain, password, 2, 0, out this._safeTokenHandle);
                if (flag)
                {
                    using (this._safeTokenHandle)
                    {
                        loggedInUser = new WindowsIdentity(this._safeTokenHandle.DangerousGetHandle());
                    }
                    return flag;
                }
                int num = Marshal.GetLastWin32Error();
                sResult = "LogonUser() failed with error code: " + num + "\r\n";
            }
            catch (Exception exception)
            {
                sResult = "LogonUser() failed with error: " + exception.Message;
                flag = false;
            }
            return flag;
        }
    }
}

