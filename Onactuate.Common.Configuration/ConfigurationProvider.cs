using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Onactuate.Common.Configuration.Lektor;

namespace Onactuate.Common.Configuration
{
    public class ConfigurationProvider : IConfigurationProvider
    {
        public string EnvironmentName { get; set; }
        public string ConnectionString { get; set; }

        internal ConfigurationProvider(string environmentName)
        {
            EnvironmentName = environmentName;
        }

        internal ConfigurationProvider(string environmentName, string connectionStringName) : this(environmentName)
        {
            ConnectionString = GetConnectionStringByName(connectionStringName);
        }

        public IDictionary<string, string> GetProperties(string configToken)
        {
            return GetProperties(configToken, EnvironmentName);
        }

        public IDictionary<string, string> GetProperties(string configToken, string environment)
        {
            IDictionary<string, string> properties = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            if (string.IsNullOrEmpty(ConnectionString))
            {
                ConnectionString = GetConnectionStringByName("ConfigDatabase");
            }
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand("dbo.GetEnvironmentConfigValues", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter parameter = new SqlParameter("EnvironmentName", SqlDbType.NVarChar, 2000) { Value = environment };
                    cmd.Parameters.Add(parameter);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader["TokenName"].ToString() == configToken)
                            {
                                string keyName = reader["KeyName"].ToString();
                                string keyValue = reader["KeyValue"].ToString();
                                bool isEncrypted = Convert.ToBoolean(reader["IsEncrypted"].ToString());

                                if (isEncrypted)
                                {
                                    byte[] valToDecrypt = EncryptionHelper.GetBytes(keyValue);
                                    keyValue = EncryptionHelper.DecryptData(valToDecrypt, environment);
                                }

                                properties.Add(keyName, keyValue);
                            }
                        }
                    }
                }
            }

            return properties;
        }

        /// <summary>
        /// Retrieves a connection string by name. 
        /// Returns null if the name is not found.
        /// </summary>
        /// <param name="name">Name of connection string param in config</param>
        /// <returns></returns>
        static string GetConnectionStringByName(string name)
        {
            // Assume failure.
            string returnValue = null;

            // Look for the name in the connectionStrings section.
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[name];

            // If found, return the connection string.
            if (settings != null) returnValue = settings.ConnectionString;

            return returnValue;
        }

    }
}