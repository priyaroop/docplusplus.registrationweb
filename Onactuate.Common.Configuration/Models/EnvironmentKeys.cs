﻿using System;
using System.Collections.Generic;

namespace Onactuate.Common.Configuration.Models
{
    [Serializable]
    public class EnvironmentKeys
    {
        public Dictionary<string, string> Keys { get; set; }

        public EnvironmentKeys()
        {
        }

        public EnvironmentKeys(Dictionary<string, string> keys)
        {
            Keys = keys;
        }
    }
}