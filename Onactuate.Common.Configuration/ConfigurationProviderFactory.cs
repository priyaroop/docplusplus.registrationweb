using System.Configuration;
using Onactuate.Common.Configuration.ConfigSettings;

namespace Onactuate.Common.Configuration
{
    public class ConfigurationProviderFactory
    {
        public IConfigurationProvider GetConfigurationProvider()
        {
            // Read the environment name from the app.config file
            //EnvironmentSection environmentSection = (EnvironmentSection) ConfigurationManager.GetSection("environmentGroup/environment");
            //return new ConfigurationProvider(environmentSection.EnvironmentName);

            return new ConfigurationProvider(ConfigurationManager.AppSettings["Environment"]);
        }

        public IConfigurationProvider GetConfigurationProvider(string connectionStringName)
        {
            // Read the environment name from the app.config file
            //EnvironmentSection environmentSection = (EnvironmentSection) ConfigurationManager.GetSection("environmentGroup/environment");
            //return new ConfigurationProvider(environmentSection.EnvironmentName);

            return new ConfigurationProvider(ConfigurationManager.AppSettings["Environment"], connectionStringName);
        }
    }
}