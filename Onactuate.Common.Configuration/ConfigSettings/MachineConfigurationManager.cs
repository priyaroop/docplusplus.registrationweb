﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Caching;
using log4net;
using Newtonsoft.Json;
using Formatting = System.Xml.Formatting;

namespace Onactuate.Common.Configuration.ConfigSettings
{
    public class MachineConfigurationManager
    {
        private const string ConfigFileCacheKey = "ConfigFileCacheKey";
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MachineConfigurationManager));

        public static void Init()
        {
            FileInfo file = GetConfigFile();

            if (!file.Exists)
            {
                if (Logger.IsInfoEnabled)
                {
                    Logger.InfoFormat("No machine specific configuration exists. {0} is created.", file.FullName);
                }

                Dictionary<string, string> appSettings = ConfigurationManager.AppSettings.AllKeys.ToDictionary(key => key, key => ConfigurationManager.AppSettings[key]);

                File.WriteAllText(file.FullName, JsonConvert.SerializeObject(appSettings, Newtonsoft.Json.Formatting.Indented));
            }
        }

        public static MachineAppSetting AppSettings => new MachineAppSetting();

        private static FileInfo GetConfigFile()
        {
            return new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\machine." + Environment.MachineName.ToLower() + ".config");
        }

        public class MachineAppSetting
        {
            public string this[string key]
            {
                get
                {
                    Dictionary<string, string> appSettings = (Dictionary<string, string>)HttpRuntime.Cache.Get(ConfigFileCacheKey);
                    if (appSettings == null)
                    {
                        appSettings = JsonConvert.DeserializeObject<Dictionary<string,string>>(File.ReadAllText(GetConfigFile().FullName));
                        HttpRuntime.Cache.Add(
                            ConfigFileCacheKey,
                            appSettings,
                            new CacheDependency(GetConfigFile().FullName),
                            Cache.NoAbsoluteExpiration,
                            TimeSpan.FromDays(1),
                            CacheItemPriority.AboveNormal,
                            delegate(string s, object value, CacheItemRemovedReason reason)
                                {
                                    if (Logger.IsInfoEnabled)
                                    {
                                        Logger.InfoFormat("Machine configuration cache is cleared (Reason: " + reason + ")");
                                    }
                                });
                    }

                    return appSettings.ContainsKey(key) ? appSettings[key] : ConfigurationManager.AppSettings[key];
                }
            }
        }
    }
}