﻿using System;
using System.Configuration;

namespace Onactuate.Common.Configuration.ConfigSettings.ConfigEnvironment
{
    public class EnvironmentElement : ConfigurationElement
    {
        public static string EnvironmentName
        {
            get
            {
                try
                {
                    EnvironmentElement environmentElement = (EnvironmentElement)ConfigurationManager.GetSection("EnvironmentSection");
                    return environmentElement.Environment;
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Environment name not found in the Config file. \n{0}", ex.Message));
                }
            }
        }
        
        [ConfigurationProperty("name", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Environment
        {
            get => (string) base["name"];
            set => base["name"] = value;
        }
    }
}