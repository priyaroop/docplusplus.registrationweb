﻿using System;
using System.Configuration;

namespace Onactuate.Common.Configuration.ConfigSettings.Encryption
{
    public class EncryptionSection : ConfigurationSection
    {
        private static EncryptionSection _instance;
        public static EncryptionSection Instance
        {
            get
            {
                try
                {
                    // Get the machine.config file.
                    System.Configuration.Configuration machineConfig = ConfigurationManager.OpenMachineConfiguration();

                    // Get the machine.config file path.
                    ConfigurationFileMap configFile = new ConfigurationFileMap(machineConfig.FilePath);

                    // Map the application configuration file to the machine configuration file.
                    System.Configuration.Configuration config = ConfigurationManager.OpenMappedMachineConfiguration(configFile);
                    _instance = (EncryptionSection)config.Sections["EncryptionSection"];

                    // TODO:: Uncomment the below line if you want to read from Web.Config or App.Config
                    //return _instance ?? (_instance = (EncryptionSection) ConfigurationManager.GetSection("EncryptionSection"));

                    return _instance;
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Private Key not found in the Machine.Config. \n{0}", ex.Message));
                }
            }
        }


        [ConfigurationProperty("EncryptionSettings", IsDefaultCollection = true)]  
        public CryptoElementCollection EncryptionSettings  
        {  
            get { return (CryptoElementCollection)this["EncryptionSettings"]; }  
            set { this["EncryptionSettings"] = value; }  
        }  
    }
}