﻿using System.Configuration;

namespace Onactuate.Common.Configuration.ConfigSettings.Encryption
{
    public class CryptoElement : ConfigurationElement
    {
        [ConfigurationProperty("environment", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Environment
        {
            get => (string) base["environment"];
            set => base["environment"] = value;
        }

        [ConfigurationProperty("privatekey", DefaultValue = "", IsRequired = true)]
        public string PrivateKey
        {
            get => (string) base["privatekey"];
            set => base["privatekey"] = value;
        }
    }
}