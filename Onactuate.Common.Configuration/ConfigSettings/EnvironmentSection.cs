using System.Configuration;

namespace Onactuate.Common.Configuration.ConfigSettings
{
    public class EnvironmentSection : ConfigurationSection
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string EnvironmentName
        {
            get
            {
                return this["name"].ToString();
            }
            set
            {
                this["name"] = value;
            }
        }
    }
}