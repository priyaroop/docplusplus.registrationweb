﻿using System;
using System.Collections.Generic;
using Onactuate.Notification.Core.Constants;

namespace Onactuate.Notification.Core.NotificationAPI
{
    public class NotificationConfigurationBase : INotificationConfiguration
    {
        protected readonly Dictionary<string, string> _configuration;

        public NotificationConfigurationBase(Dictionary<string, string> configuration)
        {
            _configuration = configuration;
        }

        ///// <summary>
        ///// Initializes a new instance of the NotificationConfigurationBase class
        ///// </summary>
        ///// <param name="configToken"></param>
        ///// <param name="environment"></param>
        //internal NotificationConfigurationBase(string configToken, string environment)
        //{
        //    _configToken = configToken;
        //    _environment = environment;

        //    ConfigurationProviderFactory factory = new ConfigurationProviderFactory();
        //    IConfigurationProvider envConfig = factory.GetConfigurationProvider();

        //    _configuration = envConfig.GetProperties(_configToken, _environment);
        //}

        ///// <summary>
        ///// Initializes a new instance of the NotificationConfigurationBase class
        ///// </summary>
        ///// <param name="configuration"></param>
        //internal NotificationConfigurationBase(IDictionary<string, string> configuration, bool someVal = false)
        //{
        //    _configuration = configuration;
        //}

        ///// <summary>
        ///// Retrieves the name of the current environment as defined in Environment configuration's
        ///// default configuration provider (namely, via the application domain's app.config)
        ///// </summary>
        ///// <returns></returns>
        //protected internal static string GetDefaultEnvironment()
        //{
        //    ConfigurationProviderFactory factory = new ConfigurationProviderFactory();
        //    IConfigurationProvider envConfig = factory.GetConfigurationProvider();
        //    return envConfig.EnvironmentName;
        //}

        #region SMTP Server settings

        public virtual string NotificationMailFormat
        {
            get { return _configuration[NotificationConstants.NotificationMailFormatConfig]; }
        }

        public virtual string NotificationMailPriority
        {
            get { return _configuration[NotificationConstants.NotificationMailPriorityConfig]; }
        }

        public virtual string NotificationMailEncoding
        {
            get { return _configuration[NotificationConstants.NotificationMailEncodingConfig]; }

        }

        public virtual string NotificationSmtpServer
        {
            get { return _configuration[NotificationConstants.NotificationSmtpServerConfig]; }
        }

        public virtual string NotificationAuthenticationType
        {
            get { return _configuration[NotificationConstants.NotificationAuthenticationTypeConfig]; }
        }

        public virtual int NotificationSmtpPort
        {
            get { return Convert.ToInt32(_configuration[NotificationConstants.NotificationSmtpPortConfig]); }
        }

        public virtual string NotificationSmtpUserName
        {
            get { return _configuration[NotificationConstants.NotificationSmtpUserNameConfig]; }
        }

        public virtual string NotificationSmtpPassword
        {
            get { return _configuration[NotificationConstants.NotificationSmtpPasswordConfig]; }
        }

        public virtual string NotificationSmtpMachineDomain
        {
            get { return _configuration[NotificationConstants.NotificationSmtpMachineDomainConfig]; }
        }

        #endregion

        #region Email Error Messages

        public virtual string ATTACHEMNT_FILE_NOT_FOUND
        {
            get { return _configuration[NotificationConstants.ATTACHEMNT_FILE_NOT_FOUND_Config]; }
        }

        public virtual string AUTHENTICATION_TYPE_NOT_PROVIDED
        {
            get { return _configuration[NotificationConstants.AUTHENTICATION_TYPE_NOT_PROVIDED_Config]; }
        }

        public virtual string INVALID_EMAIL_ADDRESS
        {
            get { return _configuration[NotificationConstants.INVALID_EMAIL_ADDRESS_Config]; }
        }

        public virtual string NO_RECIPIENTS
        {
            get { return _configuration[NotificationConstants.NO_RECIPIENTS_Config]; }
        }

        public virtual string NO_SENDER_PROVIDED
        {
            get { return _configuration[NotificationConstants.NO_SENDER_PROVIDED_Config]; }
        }

        public virtual string PASSWORD_NAME_NOT_PROVIDED
        {
            get { return _configuration[NotificationConstants.PASSWORD_NAME_NOT_PROVIDED_Config]; }
        }

        public virtual string SMTP_SERVER_NOT_PROVIDED
        {
            get { return _configuration[NotificationConstants.SMTP_SERVER_NOT_PROVIDED_Config]; }
        }

        public virtual string USER_NAME_NOT_PROVIDED
        {
            get { return _configuration[NotificationConstants.USER_NAME_NOT_PROVIDED_Config]; }
        }

        #endregion

        #region Email Subject and Email body

        public virtual string RegistrationSuccessNotificationSubject
        {
            get { return _configuration[NotificationConstants.RegistrationSuccessNotificationSubjectConfig]; }

        }

        public virtual string RegistrationErrorNotificationSubject
        {
            get { return _configuration[NotificationConstants.RegistrationErrorNotificationSubjectConfig]; }

        }

        public virtual string RegistrationErrorNotificationBody
        {
            get { return _configuration[NotificationConstants.RegistrationErrorNotificationBodyConfig]; }

        }

        public virtual string RegistrationSuccessNotificationBody
        {
            get { return _configuration[NotificationConstants.RegistrationSuccessNotificationBodyConfig]; }

        }

        #endregion

        #region Email Addresses

        public virtual string FromEmailAddress
        {
            get { return _configuration[NotificationConstants.FromEmailAddressConfig]; }
        }
       
        #endregion

        #region Email Send Boolean flags

        public bool RegistrationSuccessNotificationEmail
        {
            get { return Convert.ToBoolean(_configuration[NotificationConstants.SendRegistrationSuccessEmailConfig]); }
        }

        public bool RegistrationErrorNotificationEmail
        {
            get { return Convert.ToBoolean(_configuration[NotificationConstants.SendRegistrationErrorEmailConfig]); }
        }
    
        #endregion
    }    
}