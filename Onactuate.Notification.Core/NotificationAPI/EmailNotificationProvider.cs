﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Onactuate.Notification.Core.Constants;

namespace Onactuate.Notification.Core.NotificationAPI
{
    public class EmailNotificationProvider
    {
        private INotificationConfiguration Config { get; set; }
        private EmailMessage.MailBodyFormat mailFormat;
        private EmailMessage.MailPriority mailPriority;
        private int encoding;

        public EmailNotificationProvider(Dictionary<string, string> configuration)
        {
            ConfigEmailNotificationProvider(configuration);
        }

        private void ConfigEmailNotificationProvider(Dictionary<string, string> configuration)
        {
            Config = new NotificationConfigurationBase(configuration);
            ConfigSettings();
        }
        
        private void ConfigSettings()
        {
            mailFormat =
                (EmailMessage.MailBodyFormat)
                    Enum.Parse(typeof (EmailMessage.MailBodyFormat), Config.NotificationMailFormat);

            mailPriority =
                (EmailMessage.MailPriority) Enum.Parse(typeof (EmailMessage.MailPriority)
                    , Config.NotificationMailPriority);

            encoding =  Convert.ToInt16(Config.NotificationMailEncoding);
        }

        public void SendNotification(NotificaionType notificationType, IList<string> toEmailAddressList, string mailBody, string mailSubject,
            IList<string> mailAttachment)
        {
            string fromAddress = Config.FromEmailAddress;
            List<string> toAddress = toEmailAddressList.ToList();
            List<string> ccAddress = null;
            List<string> bccAddress = null;

            // Check if the send email notification flag for the notification type is enabled or not 
            if (CheckSendNotificationFlag(notificationType))
            {
                this.SendNotification(new EmailMessage(fromAddress, toAddress, ccAddress,
                    bccAddress, mailBody, mailSubject, mailFormat, mailPriority,
                    mailAttachment, encoding, null));
            }
        }

        private bool CheckSendNotificationFlag(NotificaionType notificationType)
        {
            switch (notificationType)
            {
                case NotificaionType.RegistrationErrorNotification:
                    return Config.RegistrationErrorNotificationEmail;
                case NotificaionType.RegistrationSuccessNotification:
                    return Config.RegistrationSuccessNotificationEmail;
                default:
                    return false;
            }
        }
        
        /// <summary>
        /// Sends the notification as per the values set in the EmailMessage class instance.   
        /// </summary>
        /// <param name="message">Details of the email message that has to be sent.</param>
        private void SendNotification(EmailMessage message)
        {
            using (SmtpClient smtpClient = new SmtpClient())
            {

                if (message.SendTo == null || message.SendTo.Count == 0)
                {
                    //check if user did not pass any reciepients, then throw exception
                    //  SendMailFailure(Resources.Resources.RES_NOTIFICATIONSERVICE_NORECIPIENTS, newMessage);
                    throw new ArgumentException(Config.NO_SENDER_PROVIDED);
                }

                MailMessage mailObject = PrepareMailMessage(message);

                string smtpServerName = Config.NotificationSmtpServer;
                smtpClient.Host = smtpServerName;
                smtpClient.Port = Config.NotificationSmtpPort;
                smtpClient.EnableSsl = true;

                if (smtpServerName == null) throw new ArgumentException(Config.SMTP_SERVER_NOT_PROVIDED);
                if (smtpServerName.Length == 0) throw new ArgumentException(Config.SMTP_SERVER_NOT_PROVIDED);
                if (Config.NotificationAuthenticationType == null)
                    throw new ArgumentException(Config.AUTHENTICATION_TYPE_NOT_PROVIDED);

                //if server name is passed , set the smtp client host
                SmtpAuthenticationType authenticationType = (SmtpAuthenticationType) Enum.Parse(
                    typeof (SmtpAuthenticationType), Config.NotificationAuthenticationType);

                switch (authenticationType)
                {
                    case SmtpAuthenticationType.Anonymous:
                        smtpClient.UseDefaultCredentials = true;
                        break;
                    case SmtpAuthenticationType.IntegratedAuthentication:
                        smtpClient.UseDefaultCredentials = true;
                        break;
                    case SmtpAuthenticationType.Basic:
                        string userName = Config.NotificationSmtpUserName;
                        string password = Config.NotificationSmtpPassword;
                        string domain = Config.NotificationSmtpMachineDomain;

                        if (String.IsNullOrEmpty(userName))
                            throw new ArgumentException(Config.USER_NAME_NOT_PROVIDED);
                        if (password == null)
                            throw new ArgumentException(Config.PASSWORD_NAME_NOT_PROVIDED);

                        NetworkCredential networkCredential = new NetworkCredential(userName, password, domain);
                        smtpClient.UseDefaultCredentials = false;
                        smtpClient.Credentials = networkCredential;
                        break;
                }

                smtpClient.Send(mailObject);
            }
        }

        #region Private methods

        /// <summary>
        /// This method checks if the email address is a valid email address
        /// </summary>
        /// <param name="address">Email address</param>
        /// <returns>true if valid, else false</returns>
        private Boolean ValidEmailAddress(string address)
        {
            Regex mailExpression =
                new Regex(
                    @"^\s*(([a-zA-Z0-9]+([_\-\.][a-zA-Z0-9]+)*)@([a-zA-Z0-9]+([_\-\.][a-zA-Z0-9]+)*)\.([a-zA-Z]{2,5}))\s*$");
            return Convert.ToBoolean(mailExpression.IsMatch(address) ? true : false);
        }

        /// <summary>
        /// This method prepares the details of email to be sent
        /// </summary>
        /// <param name="message">The object having the contents to be sent</param>
        /// <returns>Mailmessage in the required format</returns>
        private MailMessage PrepareMailMessage(EmailMessage message)
        {
            MailMessage mailBody = new MailMessage();
            int count;
            if (message.Body != null)
            {
                mailBody.Body = message.Body;
            }
            mailBody.Subject = message.Subject;
            if (ValidEmailAddress(message.SenderName))
            {
                mailBody.From = new MailAddress(message.SenderName);
            }
            else
            {
                throw new ArgumentException(Config.INVALID_EMAIL_ADDRESS);
            }

            if (message.SendTo == null)
            {
                throw new ArgumentException(Config.NO_RECIPIENTS);
            }
            if (message.SendTo.Count == 0)
            {
                throw new ArgumentException(Config.NO_RECIPIENTS);
            }

            //Add the the valid addrees in the send to list to the mail message object
            for (count = 0; count < message.SendTo.Count; count++)
            {
                if (ValidEmailAddress(message.SendTo[count]))
                {
                    mailBody.To.Add(message.SendTo[count]);
                }
            }

            //Add all the valid addresses in the cc list to the mail message
            if (message.CcList != null)
            {
                for (count = 0; count < message.CcList.Count; count++)
                {
                    if (ValidEmailAddress(message.CcList[count]))
                    {
                        mailBody.CC.Add(message.CcList[count]);
                    }
                }
            }
            //Add all the valid addresses in the bcc list to the mail message
            if (message.BccList != null)
            {
                for (count = 0; count < message.BccList.Count; count++)
                {
                    if (ValidEmailAddress(message.BccList[count]))
                    {
                        mailBody.Bcc.Add(message.BccList[count]);
                    }
                }
            }
            //set mail priority

            switch (message.Priority)
            {
                case EmailMessage.MailPriority.High:
                    mailBody.Priority = MailPriority.High;
                    break;
                case EmailMessage.MailPriority.Low:
                    mailBody.Priority = MailPriority.Low;
                    break;
                case EmailMessage.MailPriority.Normal:
                    mailBody.Priority = MailPriority.Normal;
                    break;
            }

            //set mails body format
            //TODO :: get rid of this hard coding
            mailBody.IsBodyHtml = (message.BodyFormat == EmailMessage.MailBodyFormat.HTML);

            //set mail attachments
            if (message.Attachments != null)
            {
                for (count = 0; count < message.Attachments.Count; count++)
                {
                    //check if the file exists...then only proceeds else throws exception
                    if (File.Exists(message.Attachments[count]))
                    {
                        mailBody.Attachments.Add(new Attachment(message.Attachments[count]));
                    }
                    else
                    {
                        throw new ArgumentException(string.Format(Config.ATTACHEMNT_FILE_NOT_FOUND,
                            message.Attachments[count]));
                    }
                }
            }
            //Set message Encoding
            mailBody.BodyEncoding = message.BodyEncoding;
            return mailBody;
        }

        #endregion Private methods
    }

}

