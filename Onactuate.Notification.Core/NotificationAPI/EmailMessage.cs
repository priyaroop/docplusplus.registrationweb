using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

namespace Onactuate.Notification.Core.NotificationAPI
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable()]
    public class EmailMessage 
    {
        #region Enumerations

        /// <summary>
        /// This tells the format of the message. This will be set in configuration.
        /// </summary>
        public enum MailBodyFormat
        {
            /// <summary>
            /// No format specified
            /// </summary>
            None = -1,
            /// <summary>
            /// This is the HTML message type
            /// </summary>
            HTML = 0,
            /// <summary>
            /// This is plane text message type
            /// </summary>
            Text = 1
        }

        /// <summary>
        /// This tells the priority of the email
        /// </summary>
        public enum MailPriority
        {
            /// <summary>
            /// Low priority of email
            /// </summary>
            Low = 0,
            /// <summary>
            /// Normal priority email
            /// </summary>
            Normal = 1,
            /// <summary>
            /// High priority email.
            /// </summary>
            High = 2
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// List of email Id's to which email is to be sent
        /// </summary>
        public IList<string> SendTo
        {
            get;
            set;
        }

        /// <summary>
        /// List of email Id's to which email will be cc
        /// </summary>
        public IList<string> CcList
        {
            get;
            set;
        }

        /// <summary>
        /// List of email Ids to which email will be bcc
        /// </summary>
        public IList<string> BccList
        {
            get;
            set;
        }
        
        /// <summary>
        /// The email body
        /// </summary>
        public string Body
        {
            get;
            set;
        }

        /// <summary>
        /// The email subject
        /// </summary>
        public string Subject
        {
            get;
            set;
        }

        /// <summary>
        /// The email body format
        /// </summary>
        public MailBodyFormat BodyFormat
        {
            get;
            set;
        }
        
        /// <summary>
        /// The email priority
        /// </summary>
        public MailPriority Priority
        {
            get;
            set;
        }

        /// <summary>
        /// The list of attachments
        /// </summary>
        public IList<string> Attachments
        {
            get;
            set;
        }

        /// <summary>
        /// This is the encoding of the message.
        /// </summary>
        public Encoding BodyEncoding
        {
            get;
            set;
        }

        /// <summary>
        /// This is any extra information to be sent in the message.
        /// </summary>
        public NameValueCollection ExtraInformationInHeader
        {
            get;
            set;
        }
        
        /// <summary>
        /// This is the name of the machine from which messsage is sent.
        /// </summary>
        private string machineName = Environment.MachineName;
        /// <summary>
        /// A time stamp with the message
        /// </summary>
        private DateTime createdDateTimeStamp = DateTime.Now;

        /// <summary>
        /// This is the machine name from which message is sent.
        /// </summary>
        public String MachineName
        {
            get
            {
                return machineName;
            }
        }

        /// <summary>
        /// This is the timestamp of the message.
        /// </summary>
        public DateTime CreatedDateTime
        {
            get
            {
                return createdDateTimeStamp;
            }
        }

        /// <summary>
        /// This is the email of the sender of the message.
        /// </summary>
        public String SenderName
        {
            get;set;
        }

        #endregion Properties

        #region Class Constructors

        /// <summary>
        /// This is the constructor for the email message
        /// </summary>
        /// <param name="from">The email sending the message.</param>
        /// <param name="sendTo">The to List to which email is sent</param>
        /// <param name="ccList">The CC lisy of the message.</param>
        /// <param name="bccList">The bcc list of the message</param>
        /// <param name="mailBody">The email body</param>
        /// <param name="mailSubject">The email subject</param>
        /// <param name="mailFormat">The email format</param>
        /// <param name="mailPriority">The email priority</param>
        /// <param name="attachment">The list of attachment</param>
        /// <param name="encoding">The email encoding</param>
        /// <param name="extraInformationInHeader">The extra information in email header.</param>
        public EmailMessage(string from, IList<string> sendTo, IList<string> ccList, IList<string> bccList,
            string mailBody, string mailSubject, MailBodyFormat mailFormat, MailPriority mailPriority,
            IList<string> attachment, int encoding, NameValueCollection extraInformationInHeader)
            
        {
            this.SenderName = from;
            this.SendTo = sendTo;
            this.CcList = ccList;
            this.BccList = bccList;
            this.Body = mailBody;
            this.Subject = mailSubject;
            this.BodyFormat = mailFormat;
            this.Priority = mailPriority;
            this.Attachments = attachment;
            this.ExtraInformationInHeader = extraInformationInHeader;
            this.BodyEncoding = System.Text.Encoding.GetEncoding(encoding);
        }

        #endregion Constructors
    }
}