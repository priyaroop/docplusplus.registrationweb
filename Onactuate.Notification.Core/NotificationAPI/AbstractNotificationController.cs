﻿using System.Collections.Generic;
using Onactuate.Notification.Core.Constants;

namespace Onactuate.Notification.Core.NotificationAPI
{
    /// <summary>
    /// This class provides the basic framework for an email notification system.
    /// </summary>
    public abstract class AbstractNotificationController
    {
        /// <summary>
        /// This method is sent to email messages 
        /// </summary>
        /// <param name="notificationProvider">This is the Email Notification Provider</param>
        /// <param name="typeOfNotification">This is the type of notification</param>
        /// <param name="toEmailAddressList"></param>
        /// <param name="placeholdersDataForBody">This is the data for the email body which will be replaced</param>
        /// <param name="placeholdersDataForSubject">This is the data for the subject</param>
        /// <param name="attachmentList">This is the attachment list</param>
        public void PrepareAndSendEmail(
            EmailNotificationProvider notificationProvider,
            NotificaionType typeOfNotification,
            IList<string> toEmailAddressList,
            IList<KeyValuePair<string, string>> placeholdersDataForBody, 
            IList<KeyValuePair<string, string>> placeholdersDataForSubject,
            IList<string> attachmentList)
        {
            string mailBody = ReplaceBodyPlaceholders(GetBodyTemplate(typeOfNotification), placeholdersDataForBody);
            string subject = ReplaceSubjectPlaceholders(GetSubjectTemplate(typeOfNotification), placeholdersDataForSubject);

            notificationProvider.SendNotification(typeOfNotification, toEmailAddressList, mailBody, subject, attachmentList);
        }
        
        /// <summary>
        /// This method gets the subject from the template data store
        /// </summary>
        /// <param name="typeOfNotification">The enum whichdefines the notification type.</param>
        /// <returns>The subject template</returns>
        protected abstract string GetSubjectTemplate(NotificaionType typeOfNotification);

        /// <summary>
        /// This method gets the body from the template data store
        /// </summary>
        /// <param name="typeOfNotification">The enum whichdefines the notification type.</param>
        /// <returns>The body template</returns>
        protected abstract string GetBodyTemplate(NotificaionType typeOfNotification);

        /// <summary>
        /// This method replaces the placeholder text from subject
        /// </summary>
        /// <param name="subjectText">The subject template text</param>
        /// <param name="placeholdersDataForSubject">The actual data to be filed</param>
        /// <returns>A fully constructed email subject string</returns>
        protected abstract string ReplaceSubjectPlaceholders(string subjectText, IList<KeyValuePair<string, string>> placeholdersDataForSubject);

        /// <summary>
        /// This method replaces the placeholder text from body
        /// </summary>
        /// <param name="bodyText">The body template text</param>
        /// <param name="placeholdersDataForBody">The actual data to be filed</param>
        /// <returns>A fully constructed email body string</returns>
        protected abstract string ReplaceBodyPlaceholders(string bodyText, IList<KeyValuePair<string, string>> placeholdersDataForBody);

    }
}
