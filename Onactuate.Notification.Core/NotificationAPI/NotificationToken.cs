﻿using System;

namespace Onactuate.Notification.Core.NotificationAPI
{
    [AttributeUsage(AttributeTargets.Interface, AllowMultiple = false, Inherited = false)]
    public class NotificationTokenAttribute : Attribute
    {
        private readonly string _token;

        public NotificationTokenAttribute(string token)
        {
            _token = token;
        }

        public string Token
        {
            get { return _token; }
        }
    }
}