﻿namespace Onactuate.Notification.Core.Constants
{
    /// <summary>
    /// This is the authenticaion type available.
    /// </summary>
    public enum SmtpAuthenticationType
    {
        /// <summary>
        /// No authenitcation required by the server
        /// </summary>
        Anonymous,
        /// <summary>
        /// Basic user name / password authentication
        /// </summary>
        Basic,
        /// <summary>
        /// Integrated authentication
        /// </summary>
        IntegratedAuthentication
    }
}