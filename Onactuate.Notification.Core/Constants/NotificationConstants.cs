﻿namespace Onactuate.Notification.Core.Constants
{
    public static class NotificationConstants
    {
        public const string NotificationMailFormatConfig = "MailFormat";
        public const string NotificationMailPriorityConfig = "MailPriority";
        public const string NotificationMailEncodingConfig = "MailEncoding";
        public const string NotificationSmtpServerConfig = "SmtpServer";
        public const string NotificationAuthenticationTypeConfig = "AuthenticationType";
        public const string NotificationSmtpPortConfig = "SmtpPort";
        public const string NotificationSmtpUserNameConfig = "SmtpUserName";
        public const string NotificationSmtpPasswordConfig = "SmtpPassword";
        public const string NotificationSmtpMachineDomainConfig = "SmtpMachineDomain";

        public const string ATTACHEMNT_FILE_NOT_FOUND_Config = "ATTACHEMNT_FILE_NOT_FOUND";
        public const string AUTHENTICATION_TYPE_NOT_PROVIDED_Config = "AUTHENTICATION_TYPE_NOT_PROVIDED";
        public const string INVALID_EMAIL_ADDRESS_Config = "INVALID_EMAIL_ADDRESS";
        public const string NO_RECIPIENTS_Config = "NO_RECIPIENTS";
        public const string NO_SENDER_PROVIDED_Config = "NO_SENDER_PROVIDED";
        public const string PASSWORD_NAME_NOT_PROVIDED_Config = "PASSWORD_NAME_NOT_PROVIDED";
        public const string SMTP_SERVER_NOT_PROVIDED_Config = "SMTP_SERVER_NOT_PROVIDED";
        public const string USER_NAME_NOT_PROVIDED_Config = "USER_NAME_NOT_PROVIDED";

        public const string FromEmailAddressConfig = "FromEmailAddress";

        public static string RegistrationSuccessNotificationSubjectConfig = "RegistrationSuccessNotificationSubject";
        public static string RegistrationErrorNotificationSubjectConfig = "RegistrationErrorNotificationSubject";
        public static string RegistrationErrorNotificationBodyConfig = "RegistrationErrorNotificationBody";
        public static string RegistrationSuccessNotificationBodyConfig = "RegistrationSuccessNotificationBody";
        public static string SendRegistrationErrorEmailConfig = "SendRegistrationErrorEmail";
        public static string SendRegistrationSuccessEmailConfig = "SendRegistrationSuccessEmail";
    }
}