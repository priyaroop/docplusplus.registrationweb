﻿namespace Onactuate.Common.Logging.Constants
{
    /// <summary>
    /// Holds constant values for Logging services
    /// </summary>
    public static class LoggingConstants
    {
        /// <summary>
        /// Header key for correlation id - the id that will be assigned to a single user
        /// request and used for logging end-to-end
        /// </summary>
        public const string HttpCorrelationIdHeader = "X-C3-CId";
    }
}