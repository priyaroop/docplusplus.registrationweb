﻿using System;
using Onactuate.Common.Logging.Contracts;

namespace Onactuate.Common.Logging
{
    /// <summary>
    /// Default implementation of LoggingService. This class will use the LoggingWriter(writes to log4Net)
    /// to do raw log output.
    /// update logwriter to prevent log4net usage from surfacing unhandled exceptions
    /// </summary>
    /// 
    /// 
    /// Created By      Created On      Modified By     Modified On         Purpose
    /// =========================================================================================
    /// Priyaroop       06-02-2015                                          Baseline Version
    ///     
    public class LoggingService : ILoggingService
    {
        #region Private Member Variables

        private ILoggingContextProvider _loggingContextProvider;

        // move this internal class to an doubleerface if we need to make this more pluggable
        // currently this class is created for logical separation - not meant for extensibility
        private ILoggingWriter _loggingWriter;

        #endregion

        #region Class Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingService"/> class.
        /// </summary>
        /// <param name="logContextProvider">The log context provider.</param>
        public LoggingService(ILoggingContextProvider logContextProvider)
        {
            _loggingContextProvider = logContextProvider;
            _loggingWriter = new LoggingWriter();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingService"/> class.
        /// </summary>
        /// <param name="logContextProvider">The log context provider.</param>
        /// <param name="loggingWriter">The logging writer.</param>
        public LoggingService(
            ILoggingContextProvider logContextProvider,
            ILoggingWriter loggingWriter)
        {
            _loggingContextProvider = logContextProvider;
            _loggingWriter = loggingWriter;
        }

        #endregion

        #region Debug Methods

        /// <summary>
        /// Write to log writer with Debug priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        public void Debug(string source, string message)
        {
            WriteToLogWriter(source, Priority.Debug, message, true, 0, null);
        }

        /// <summary>
        /// Write to log writer with Debug priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        /// <param name="durationInMilliseconds">The duration in milliseconds</param>
        public void Debug(string source, string message, double durationInMilliseconds)
        {
            WriteToLogWriter(source, Priority.Debug, message, true, durationInMilliseconds, null);
        }

        /// <summary>
        /// Write to log writer with Debug priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        /// <param name="durationInMilliseconds">The duration in milliseconds</param>
        /// <param name="correlationId">Correlation Id. A unique id to corellate multiple messages</param>
        public void Debug(string source, string message, double durationInMilliseconds, Guid correlationId)
        {
            WriteToLogWriter(source, Priority.Debug, message, true, durationInMilliseconds, correlationId);
        }

        #endregion

        #region Info Methods

        /// <summary>
        /// Write to log writer with Info priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        public void Info(string source, string message)
        {
            WriteToLogWriter(source, Priority.Info, message, true, 0, null);
        }

        /// <summary>
        /// Write to log writer with Info priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        /// <param name="durationInMilliseconds">The duration in milliseconds</param>
        public void Info(string source, string message, double durationInMilliseconds)
        {
            WriteToLogWriter(source, Priority.Info, message, true, durationInMilliseconds, null);
        }

        /// <summary>
        /// Write to log writer with Info priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        /// <param name="durationInMilliseconds">The duration in milliseconds</param>
        /// <param name="correlationId">Correlation Id. A unique id to corellate multiple messages</param>
        public void Info(string source, string message, double durationInMilliseconds, Guid correlationId)
        {
            WriteToLogWriter(source, Priority.Info, message, true, durationInMilliseconds, correlationId);
        }

        #endregion

        #region Warning Methods

        /// <summary>
        /// Write to log writer with Warning priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        public void Warning(string source, string message)
        {
            WriteToLogWriter(source, Priority.Warning, message, false, 0, null);
        }

        /// <summary>
        /// Write to log writer with Warning priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        /// <param name="durationInMilliseconds">The duration in milliseconds</param>
        public void Warning(string source, string message, double durationInMilliseconds)
        {
            WriteToLogWriter(source, Priority.Warning, message, false, durationInMilliseconds, null);
        }

        /// <summary>
        /// Write to log writer with Warning priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        /// <param name="durationInMilliseconds">The duration in milliseconds</param>
        /// <param name="correlationId">Correlation Id. A unique id to corellate multiple messages</param>
        public void Warning(string source, string message, double durationInMilliseconds, Guid correlationId)
        {
            WriteToLogWriter(source, Priority.Warning, message, false, durationInMilliseconds, correlationId);
        }

        #endregion

        #region Error Methods
        
        /// <summary>
        /// Write to log writer with Error priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        public void Error(string source, string message)
        {
            WriteToLogWriter(source, Priority.Error, message, false, 0, null);
        }

        /// <summary>
        /// Write to log writer with Error priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        /// <param name="durationInMilliseconds">The duration in milliseconds</param>
        public void Error(string source, string message, double durationInMilliseconds)
        {
            WriteToLogWriter(source, Priority.Error, message, false, durationInMilliseconds, null);
        }

        /// <summary>
        /// Write to log writer with Error priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        /// <param name="durationInMilliseconds">The duration in milliseconds</param>
        /// <param name="correlationId">Correlation Id. A unique id to corellate multiple messages</param>
        public void Error(string source, string message, double durationInMilliseconds, Guid correlationId)
        {
            WriteToLogWriter(source, Priority.Error, message, false, durationInMilliseconds, correlationId);
        }

        #endregion

        #region Fatal Methods

        /// <summary>
        /// Write to log writer with Fatal priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        public void Fatal(string source, string message)
        {
            WriteToLogWriter(source, Priority.Fatal, message, false, 0, null);
        }

        /// <summary>
        /// Write to log writer with Fatal priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        /// <param name="durationInMilliseconds">The duration in milliseconds</param>
        public void Fatal(string source, string message, double durationInMilliseconds)
        {
            WriteToLogWriter(source, Priority.Fatal, message, false, durationInMilliseconds, null);
        }

        /// <summary>
        /// Write to log writer with Fatal priority
        /// </summary>
        /// <param name="source">Source of message</param>
        /// <param name="message">Message to log</param>
        /// <param name="durationInMilliseconds">The duration in milliseconds</param>
        /// <param name="correlationId">Correlation Id. A unique id to corellate multiple messages</param>
        public void Fatal(string source, string message, double durationInMilliseconds, Guid correlationId)
        {
            WriteToLogWriter(source, Priority.Fatal, message, false, durationInMilliseconds, correlationId);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Writes to log writer using additional data from ILoggingContextProvider
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="priorty">The priorty.</param>
        /// <param name="statusDescription">The status description.</param>
        /// <param name="success">if set to <c>true</c> [success].</param>
        /// <param name="durationInMilliseconds">The duration in milliseconds.</param>
        /// <param name="correlationId">The correlation id.</param>
        private void WriteToLogWriter(
            string source,
            Priority priorty,
            string statusDescription,
            bool success,
            double durationInMilliseconds,
            Guid? correlationId)
        {
            _loggingWriter.WriteToLog(
                priority: priorty,
                serviceName: _loggingContextProvider.ServiceName,
                correlationId: correlationId ?? _loggingContextProvider.CorrelationId,
                source: source,
                success: success,
                userId: _loggingContextProvider.UserId,
                serverIP: _loggingContextProvider.ServerIP,
                clientIP: _loggingContextProvider.ClientIP,
                statusDescription: statusDescription,
                duration: durationInMilliseconds);
        }

        #endregion

    }
}
