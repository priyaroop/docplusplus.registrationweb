﻿using System;
using System.Globalization;

namespace Onactuate.Common.Logging
{
    /// <summary>
    /// Helper class for converting objects to messages
    /// </summary>
    /// 
    /// 
    /// Created By      Created On      Modified By     Modified On         Purpose
    /// =========================================================================================
    /// Priyaroop       06-02-2015                                          Baseline Version
    ///  
    public sealed class LoggingMessageHelper
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="LoggingMessageHelper"/> class from being created.
        /// </summary>
        private LoggingMessageHelper()
        {
        }

        /// <summary>
        /// Serializes the exception.
        /// </summary>
        /// <param name="exception">The e.</param>
        /// <param name="exceptionMessage">The exception message.</param>
        /// <returns></returns>
        public static string SerializeException(Exception exception, string exceptionMessage)
        {
            string message = string.Empty;
            if (exception != null)
            {
                message = string.Format(
                    CultureInfo.InvariantCulture,
                    "{0}{1}{2}<br>{3}",
                    exceptionMessage,
                    string.IsNullOrEmpty(exceptionMessage) ? string.Empty : "<br><br>",
                    exception.Message,
                    exception.StackTrace);
            }
            else
            {
                message = exceptionMessage;
            }

            return message;
        }

        /// <summary>
        /// Serializes the exception.
        /// </summary>
        /// <param name="exception">The e.</param>
        /// <returns></returns>
        public static string SerializeException(Exception exception)
        {
            return SerializeException(exception, string.Empty);
        }
    }
}
