﻿using System.Web;

namespace Onactuate.Common.Logging.LoggingContextProviders
{
    public class HttpContextProvider
    {
        public virtual HttpContextBase CurrentHttpContext()
        {

                return System.Web.HttpContext.Current != null
                           ? new HttpContextWrapper(System.Web.HttpContext.Current)
                           : null;
        }
    }
}
