﻿using System.Diagnostics;
using Onactuate.Common.Logging.Contracts;

namespace Onactuate.Common.Logging.LoggingContextProviders
{
    /// <summary>
    /// This context provider can only return information for the machine
    /// </summary>
    /// 
    /// 
    /// 
    /// Created By      Created On      Modified By     Modified On         Purpose
    /// =========================================================================================
    /// Priyaroop       06-02-2015                                          Baseline Version
    ///  
    public class StaticLoggingContextProvider : ILoggingContextProvider
    {
        /// <summary>
        /// Gets the correlation id - the unifying Id that will be used end-to-end
        /// for identifying a single user transaction.
        /// 
        /// We are using activity manager's traceid
        /// </summary>
        public System.Guid CorrelationId
        {
            get { return Trace.CorrelationManager.ActivityId; }
        }

        /// <summary>
        /// Gets the overall service(application) that the log originated from
        /// </summary>
        public string ServiceName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the user id - the target on behalf of whom the action was performed
        /// </summary>
        public string UserId
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the server ip
        /// </summary>
        public virtual string ServerIP
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the client ip
        /// </summary>
        public virtual string ClientIP
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StaticLoggingContextProvider"/> class.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        public StaticLoggingContextProvider(string serviceName)
        {
            this.ServiceName = serviceName;
        }
    }
}