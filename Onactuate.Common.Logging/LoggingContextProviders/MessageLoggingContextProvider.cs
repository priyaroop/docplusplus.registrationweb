﻿using System.ServiceModel.Channels;

namespace Onactuate.Common.Logging.LoggingContextProviders
{
    /// <summary>
    /// Message logging context provider - transforms message's info into
    /// logging context
    /// </summary>
    public class MessageLoggingContextProvider : StaticLoggingContextProvider
    {
        private readonly MessageProperties _messageProperties;

        /// <summary>
        /// Gets the server ip
        /// </summary>
        public override string ServerIP
        {
            get
            {
                // extract client ip from message properties
                string serverIp = string.Empty;

                if (_messageProperties != null && _messageProperties.ContainsKey(HttpRequestMessageProperty.Name))
                {
                    var httpRequestProperty = _messageProperties[HttpRequestMessageProperty.Name]
                        as HttpRequestMessageProperty;

                    if (httpRequestProperty != null && httpRequestProperty.Headers != null)
                    {
                        serverIp = httpRequestProperty.Headers[System.Net.HttpRequestHeader.Host];
                    }
                }

                return serverIp;
            }
        }

        /// <summary>
        /// Gets the client ip
        /// </summary>
        public override string ClientIP
        {
            get
            {
                // extract client ip from message properties
                string clientIp = string.Empty;

                if (_messageProperties != null &&
                    _messageProperties.Via != null)
                {
                    clientIp = _messageProperties.Via.Host;
                }

                return clientIp;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageLoggingContextProvider"/> class.
        /// </summary>
        /// <param name="messageProperties">The message property.</param>
        /// <param name="serviceName">Name of the service.</param>
        public MessageLoggingContextProvider(MessageProperties messageProperties, string serviceName) : base(serviceName)
        {
            _messageProperties = messageProperties;
        }
    }
}
