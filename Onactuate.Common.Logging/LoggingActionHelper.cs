﻿using System;
using Onactuate.Common.Logging.Contracts;

namespace Onactuate.Common.Logging
{
    /// <summary>
    /// Helper class for logging actions
    /// </summary>
    public sealed class LoggingActionHelper
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="LoggingActionHelper"/> class from being created.
        /// </summary>
        private LoggingActionHelper()
        {
        }

        /// <summary>
        /// Logs the action and rethrow.
        /// </summary>
        /// <param name="loggingService">The logging service.</param>
        /// <param name="sourceName">Name of the source.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <param name="action">The action.</param>
        public static void LogActionAndRethrow(
            ILoggingService loggingService,
            string sourceName,
            string actionName,
            Action action)
        {
            if (loggingService != null && action != null)
            {
                DateTime startTime = DateTime.Now;
                try
                {
                    action();

                    loggingService.Info(
                        sourceName,
                        actionName + "() completed",
                        (DateTime.Now - startTime).TotalMilliseconds);
                }
                catch (Exception exception)
                {
                    loggingService.Error(
                        sourceName,
                        LoggingMessageHelper.SerializeException(exception, actionName+"() failed"),
                        (DateTime.Now - startTime).TotalMilliseconds);

                    throw;
                }
            }
        }
    }
}
