﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using log4net.Core;
using log4net.Repository;
using Onactuate.Common.Logging.Contracts;

namespace Onactuate.Common.Logging
{
    /// <summary>
    /// Internal class to handle log4net writing of log data
    /// </summary>    
    /// 
    /// 
    /// Created By      Created On      Modified By     Modified On         Purpose
    /// =========================================================================================
    /// Priyaroop       06-02-2015                                          Baseline Version
    ///    
    internal class LoggingWriter : ILoggingWriter
    {
        private static object _loggingServiceSyncObject = new object();
        private static ILogger _log4netWriter = null;
        private const string _loggingConfigurationName = "Logging.config";

        /// <summary>
        /// Attempts to initialize LoggingService writter
        /// </summary>
        public LoggingWriter()
        {
            // we are using a separate log4net repository for diagnostic logging
            // this eliminates confusions and user errors if they have to merge and manage
            // a single log4net configuration file
            if (_log4netWriter == null)
            {
                lock (_loggingServiceSyncObject)
                {
                    if (_log4netWriter == null)
                    {
                        //TODO: Use embedded resource config as final backup if we can't find a configuration
                        string logFilePath = Path.Combine(
                            AppDomain.CurrentDomain.BaseDirectory,
                            _loggingConfigurationName);
                        if (!File.Exists(logFilePath))
                        {
                            logFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _loggingConfigurationName);
                        }

                        //TODO: Log error for log file not found failure
                        //TODO: Log initialization failure error for configuration file is of bad format
                        ILoggerRepository repository = LoggerManager.CreateRepository("OnactuateLoggingService");
                        log4net.Config.XmlConfigurator.ConfigureAndWatch(
                            repository,
                            new System.IO.FileInfo(logFilePath));

                        _log4netWriter = repository.GetLogger("");
                    }
                }
            }
        }

        /// <summary>
        /// Incoming parameters will be validated and sanitized
        /// If required fields are missing, we will still attempt at best effort write to the
        /// log4net writer
        /// </summary>
        /// <param name="priority">The priority.</param>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="correlationId">The source.</param>
        /// <param name="source">The status description.</param>
        /// <param name="success">Is successful?.</param>
        /// <param name="userId">User Id.</param>
        /// <param name="serverIP">Server IP Address.</param>
        /// <param name="clientIP">Client IP Address.</param>
        /// <param name="statusDescription">The status description.</param>
        /// <param name="duration">Duration.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage(
            "Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification=@"We want to keep the design of the logging writer to be an instance
                            object. Exposing this as static method will remove that requirement")]
        public void WriteToLog(
            Priority priority,
            string serviceName,
            Guid correlationId,
            string source,
            bool success,
            string userId,
            string serverIP,
            string clientIP,
            string statusDescription,
            double duration)
        {
            // we should not omit logging data if we only have partial data. WE still should do our best
            // effort to log the data and flag this as a severe error that MUST be addressed.
            try
            {
                ValidateLoggingParameters(
                    priority,
                    serviceName,
                    source,
                    statusDescription);
            }
            catch (InvalidCastException)
            {
                // TODO: write this validation error to event log as serious error
                // Create/Write as LoggingException
            }
            catch (ArgumentNullException)
            {
                // TODO: write this validation error to event log as serious error
                // Create/Write as LoggingException
            }

            int loggingParameterIndex = 0;
            string[] loggingParameters = new string[12];
            loggingParameters[loggingParameterIndex++] = DateTime.Now.ToUniversalTime().ToString(
                "o", CultureInfo.InvariantCulture);
            loggingParameters[loggingParameterIndex++] = serviceName;
            loggingParameters[loggingParameterIndex++] = source;
            loggingParameters[loggingParameterIndex++] = success.ToString();
            loggingParameters[loggingParameterIndex++] = userId;
            loggingParameters[loggingParameterIndex++] = correlationId.ToString("N");
            loggingParameters[loggingParameterIndex++] = serverIP;
            loggingParameters[loggingParameterIndex++] = clientIP;
            loggingParameters[loggingParameterIndex++] = statusDescription;
            loggingParameters[loggingParameterIndex++] = duration.ToString(CultureInfo.InvariantCulture);

            SanitizeParameterList(loggingParameters);

            LoggingEventData loggingEventData = new log4net.Core.LoggingEventData()
            {
               Level = GetLevelFromPriority(priority),
               TimeStamp = DateTime.Now,
               Message = string.Join("|", loggingParameters),
               Properties = new log4net.Util.PropertiesDictionary()
            };

            loggingEventData.Properties["serviceName"] = serviceName;
            loggingEventData.Properties["hostName"] = System.Environment.MachineName;
            loggingEventData.Properties["correlationId"] = correlationId.ToString("N");

            _log4netWriter.Log(
                new log4net.Core.LoggingEvent(loggingEventData));
        }

        /// <summary>
        /// Sanitizes the parameter list. Removes newline, pipes, and trailing white spaces
        /// </summary>
        /// <param name="loggingParameters">The logging parameters.</param>
        private static void SanitizeParameterList(IList<string> loggingParameters)
        {
            if (loggingParameters == null)
            {
                throw new ArgumentNullException("loggingParameters");
            }

            for (int i = 0; i < loggingParameters.Count; i++ )
            {
                // remove pipes and new lines
                if (!string.IsNullOrEmpty(loggingParameters[i]))
                {
                    loggingParameters[i] = loggingParameters[i].Replace('|', ' ');
                    loggingParameters[i] = loggingParameters[i].Replace(Environment.NewLine, "<br>");
                    loggingParameters[i] = loggingParameters[i].Trim();
                }
            }
        }

        /// <summary>
        /// Gets the log4net level from loggingService priority.
        /// </summary>
        /// <param name="loggingPriorty">The logging priorty.</param>
        private static Level GetLevelFromPriority(Priority loggingPriorty)
        {
            switch (loggingPriorty)
            {
                case Priority.Info:
                    return Level.Info;
                case Priority.Debug:
                    return Level.Debug;
                case Priority.Warning:
                    return Level.Warn;
                case Priority.Error:
                    return Level.Error;
                case Priority.Fatal:
                    return Level.Fatal;
                default:
                    return Level.Info;
                    //TODO:yaqing log priority not found
            }
        }

        /// <summary>
        /// Validates the logging parameters.
        /// </summary>
        /// <param name="priority">The priority.</param>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="source">The source.</param>
        /// <param name="statusDescription">The status description.</param>
        private static void ValidateLoggingParameters(
            Priority priority,
            string serviceName,
            string source,
            string statusDescription)
        {
            if (!Enum.IsDefined(typeof(Priority), priority))
            {
                throw new InvalidCastException("priority");
            }

            if (string.IsNullOrEmpty(serviceName))
            {
                throw new ArgumentNullException("serviceName");
            }

            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentNullException("source");
            }

            if (string.IsNullOrEmpty(statusDescription))
            {
                throw new ArgumentNullException("statusDescription");
            }
        }
    }
}
