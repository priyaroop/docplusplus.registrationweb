﻿using System;

namespace Onactuate.Common.Logging.Contracts
{
    /// <summary>
    /// Provides additional context to the logging request. Used by ILoggingService implementation
    /// to request for more fields for logging
    /// </summary>
    public interface ILoggingContextProvider
    {
        /// <summary>
        /// Gets the correlation id - the unifying Id that will be used end-to-end
        /// for identifying a single user transaction
        /// </summary>
        Guid CorrelationId { get; }

        /// <summary>
        /// Gets the overall service(application) that the log originated from
        /// </summary>
        string ServiceName { get; }

        /// <summary>
        /// Gets the user id - the target on behalf of whom the action was performed
        /// </summary>
        string UserId { get; }

        /// <summary>
        /// Gets the server ip
        /// </summary>
        string ServerIP { get; }

        /// <summary>
        /// Gets the client ip
        /// </summary>
        string ClientIP { get; }
    }
}
