﻿// -----------------------------------------------------------------------
// <copyright file="Priority.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Onactuate.Common.Logging.Contracts
{
    /// <summary>
    /// The priority type of the log entry
    /// </summary>
    public enum Priority
    {
        Info,
        Debug,
        Warning,
        Error,
        Fatal
    }
}
