﻿using System;

namespace Onactuate.Common.Logging.Contracts
{
    /// <summary>
    /// Logging service will be used by components to do varies priority of logging
    /// Debug and Info are typically for developer usage
    /// Warning, Error, and Fatal are for support usage
    /// 
    /// Parameter definition:
    /// Source - the service wrapper or the module that the log is being written from
    /// Message - detailed message of the log, can contain exception info/stack trace
    ///
    /// Optional params:
    /// DurationInMilliseconds - the time it took for the operation to complete, can be
    /// rendering task or service call duration
    /// CorrelationId - By default the logger attempts to grab the correlationId from the
    /// present context. This param allows for an override for the correlationId
    /// 
    /// If duration is not applicable in a case where you want to override correlationId -
    /// use zero for duration
    /// </summary>
    public interface ILoggingService
    {
        void Debug(string source, string message);
        void Debug(string source, string message, double durationInMilliseconds);
        void Debug(string source, string message, double durationInMilliseconds, Guid correlationId);

        void Info(string source, string message);
        void Info(string source, string message, double durationInMilliseconds);
        void Info(string source, string message, double durationInMilliseconds, Guid correlationId);

        void Warning(string source, string message);
        void Warning(string source, string message, double durationInMilliseconds);
        void Warning(string source, string message, double durationInMilliseconds, Guid correlationId);

        void Error(string source, string message);
        void Error(string source, string message, double durationInMilliseconds);
        void Error(string source, string message, double durationInMilliseconds, Guid correlationId);

        void Fatal(string source, string message);
        void Fatal(string source, string message, double durationInMilliseconds);
        void Fatal(string source, string message, double durationInMilliseconds, Guid correlationId);
    }
}
