﻿using System;
using System.Diagnostics;
using System.Web;
using Onactuate.Common.Logging.Constants;

namespace Onactuate.Common.Logging.Extensions
{
    /// <summary>
    /// This module is used for HttpApplications that wish to consume and propagate correlationId
    /// </summary>
    public class CorrelationIdHttpModule : IHttpModule
    {
        /// <summary>
        /// Disposes of the resources (other than memory) used by the module that implements <see cref="T:System.Web.IHttpModule"/>.
        /// </summary>
        public void Dispose()
        {
            // nothing to clean up
        }

        /// <summary>
        /// Inits the specified application.
        /// </summary>
        /// <param name="context">The Http application.</param>
        public void Init(HttpApplication context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            context.BeginRequest += (sender, e) =>
                OnBeginRequest(new HttpContextWrapper(((HttpApplication)sender).Context));
        }

        /// <summary>
        /// Handles the BeginRequest event of the Application control.
        /// Check and see if we can retrieve a correlationId from request header, if not
        /// generate it, then assign a valid correlationid to the respone http header
        /// </summary>
        /// <param name="context">The context.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification="Making this static would allow for unintented usage")]
        private void OnBeginRequest(HttpContextBase context)
        {
            Guid requestCorrelationId;
            if (!string.IsNullOrEmpty(context.Request.Headers[LoggingConstants.HttpCorrelationIdHeader]))
            {
                if (!Guid.TryParse(
                        context.Request.Headers[LoggingConstants.HttpCorrelationIdHeader],
                        out requestCorrelationId))
                {
                    // failed to retrieve create a new one
                    requestCorrelationId = Guid.NewGuid();
                }
            }
            else
            {
                requestCorrelationId = Guid.NewGuid();
            }

            Trace.CorrelationManager.ActivityId = requestCorrelationId;

            // Emit correlationId back to the client in http response header
            if (context.Response != null)
            {
                context.Response.Headers[LoggingConstants.HttpCorrelationIdHeader] =
                    requestCorrelationId.ToString("N");
            }
        }
    }
}
