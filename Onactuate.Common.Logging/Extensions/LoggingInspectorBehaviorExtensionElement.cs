﻿using System;
using System.Configuration;
using System.ServiceModel.Configuration;

namespace Onactuate.Common.Logging.Extensions
{
    /// <summary>
    /// Allows for configuring Logging
    /// </summary>
    public class LoggingInspectorBehaviorExtensionElement : BehaviorExtensionElement
    {
        /// <summary>
        /// Gets or sets the name of the overall service name which 
        /// will be used in logging unhandled exceptions
        /// </summary>
        /// <value>
        /// The name of the service.
        /// </value>
        [ConfigurationProperty("serviceName", IsRequired = true)]
        public string ServiceName
        {
            get { return (string)this["serviceName"]; }
            set { this["serviceName"] = value; }
        }

        public override Type BehaviorType
        {
            get { return typeof(LoggingInspectorServiceBehavior); }
        }

        protected override object CreateBehavior()
        {
            return new LoggingInspectorServiceBehavior(this.ServiceName);
        }
    }
}
