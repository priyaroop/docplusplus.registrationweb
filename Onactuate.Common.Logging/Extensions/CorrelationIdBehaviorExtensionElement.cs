﻿using System;
using System.ServiceModel.Configuration;

namespace Onactuate.Common.Logging.Extensions
{
    /// <summary>
    /// This allows the definition of behaviors through .configs. This wraps
    /// the instantiation of CorrelationIdInspectorServiceBehavior
    /// </summary>
    public class CorrelationIdBehaviorExtensionElement : BehaviorExtensionElement
    {
        public override Type BehaviorType
        {
            get { return typeof(CorrelationIdInspectorServiceBehavior); }
        }

        protected override object CreateBehavior()
        {
            return new CorrelationIdInspectorServiceBehavior();
        }
    }
}
