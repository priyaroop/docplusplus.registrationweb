﻿using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using Onactuate.Common.Logging.Contracts;

namespace Onactuate.Common.Logging.Extensions
{
    /// <summary>
    /// Extension to be used for WCF services that wants to log all uncaught exceptions
    /// </summary>
    public class ErrorLoggingHandler : IErrorHandler
    {
        private readonly ILoggingService _loggingSevice;
        private const string LoggingSource = "ErrorLoggingHandler";

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorLoggingHandler"/> class.
        /// </summary>
        /// <param name="loggingService">The logging service.</param>
        public ErrorLoggingHandler(ILoggingService loggingService)
        {
            _loggingSevice = loggingService;
        }

        /// <summary>
        /// Enables error-related processing and returns a value that indicates whether the dispatcher aborts the session and the instance context in certain cases.
        /// </summary>
        /// <param name="error">The exception thrown during processing.</param>
        /// <returns>
        /// true if  should not abort the session (if there is one) and instance context if the instance context is not <see cref="F:System.ServiceModel.InstanceContextMode.Single"/>; otherwise, false. The default is false.
        /// </returns>
        public bool HandleError(Exception error)
        {
            _loggingSevice.Error(LoggingSource, LoggingMessageHelper.SerializeException(error, "Uncaught WCF exception"));

            // indicates the error is NOT handled and may abort session
            return false;
        }

        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            // NO OP, we are not implementing custom fault exceptions in this helper extension
        }
    }
}
