﻿using System;
using System.Diagnostics;
using System.Net;
using System.Reflection;
using Onactuate.Common.Logging.Constants;

namespace Onactuate.Common.Logging.Extensions
{
    /// <summary>
    /// This module can be used to inspect and perform custom work on all out going http requests
    /// In the create process, we will assign the HttpCorrelationIdHeader using the
    /// current Activity Id if it is not empty. This allows the propagation of Activity Ids
    /// to other HTTP services
    /// </summary>
    public class CorrelationIdWebRequestCreator : IWebRequestCreate
    {
        /// <summary>
        /// Creates the specified URI
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns>A web request with the headers assigned for correlation id
        /// and GZIP encoding acceptance</returns>
        public WebRequest Create(Uri uri)
        {
            if (uri == null)
            {
                throw new ArgumentNullException("uri");
            }

            // need to use reflection to create the base request would otherwise
            // create self recursive call
            HttpWebRequest httpWebRequest =
                Activator.CreateInstance(typeof (HttpWebRequest),
                    BindingFlags.CreateInstance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
                    null, new object[] {uri, null}, null) as HttpWebRequest;

            if (httpWebRequest == null)
            {
                return null;
            }

            // by default WCF requests don't set the compression encoding headers, adding this
            // will allow for slight perf improvements
            httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            if (Trace.CorrelationManager.ActivityId != Guid.Empty)
            {
                httpWebRequest.Headers[LoggingConstants.HttpCorrelationIdHeader] = Trace.CorrelationManager.ActivityId.ToString("N");
            }

            return httpWebRequest;
        }
    }
}
