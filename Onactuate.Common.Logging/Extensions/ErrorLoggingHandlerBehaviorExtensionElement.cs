﻿using System;
using System.Configuration;
using System.ServiceModel.Configuration;
using Onactuate.Common.Logging.LoggingContextProviders;

namespace Onactuate.Common.Logging.Extensions
{
    /// <summary>
    /// This class is used in configuration files for setting up the ErrorLoggingHandler behavior
    /// </summary>
    public class ErrorLoggingHandlerBehaviorExtensionElement : BehaviorExtensionElement
    {
        /// <summary>
        /// Gets or sets the name of the overall service name which 
        /// will be used in logging unhandled exceptions
        /// </summary>
        /// <value>
        /// The name of the service.
        /// </value>
        [ConfigurationProperty("serviceName", IsRequired = true)]
        public string ServiceName
        {
            get { return (string)this["serviceName"]; }
            set { this["serviceName"] = value; }
        }

        public override Type BehaviorType
        {
            get { return typeof(ErrorLoggingHandlerBehavior); }
        }

        /// <summary>
        /// Creates a behavior extension based on the current configuration settings.
        /// </summary>
        /// <returns>
        /// The behavior extension.
        /// </returns>
        protected override object CreateBehavior()
        {
            // we must construct the actual implementation of loggingService because
            // our app currently does not support dependency injection for WCF behaviors
            StaticLoggingContextProvider contextProvider = new StaticLoggingContextProvider(this.ServiceName);
            LoggingService loggingService = new LoggingService(contextProvider);

            return new ErrorLoggingHandlerBehavior(loggingService);
        }
    }
}
