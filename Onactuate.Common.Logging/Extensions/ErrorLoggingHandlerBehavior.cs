﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using Onactuate.Common.Logging.Contracts;

namespace Onactuate.Common.Logging.Extensions
{
    /// <summary>
    /// ErrorLoggingHandlerBehavior for adding ErrorLoggingHandler to a service
    /// </summary>
    public class ErrorLoggingHandlerBehavior : IServiceBehavior
    {
        private readonly ILoggingService _loggingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorLoggingHandlerBehavior"/> class.
        /// </summary>
        /// <param name="loggingService">The logging service.</param>
        public ErrorLoggingHandlerBehavior(ILoggingService loggingService)
        {
            _loggingService = loggingService;
        }

        public void AddBindingParameters(
            ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase,
            System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints,
            BindingParameterCollection bindingParameters)
        {
            // NO OP not using this extension method
        }

        /// <summary>
        /// Provides the ability to change run-time property values or
        /// insert custom extension objects such as error handlers,
        /// message or parameter interceptors, security extensions, and other custom extension objects.
        /// </summary>
        /// <param name="serviceDescription">The service description.</param>
        /// <param name="serviceHostBase">The host that is currently being built.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1", Justification="WCF pipeline guarantees the existence of the arguments")]
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher channelDispatcher in serviceHostBase.ChannelDispatchers)
            {
                channelDispatcher.ErrorHandlers.Add(new ErrorLoggingHandler(_loggingService));
            }
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            // NO OP not using this extension method
        }
    }
}
