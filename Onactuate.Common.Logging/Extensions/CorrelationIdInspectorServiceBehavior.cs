﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Onactuate.Common.Logging.Extensions
{
    /// <summary>
    /// This is the boiler plate code necessary to allow for assigning
    /// CorrelationIdInspectorBehavior as a Service-wide behavior
    /// </summary>
    public class CorrelationIdInspectorServiceBehavior : IEndpointBehavior, IServiceBehavior
    {
        #region Endpoint behavior requirements

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            if (endpointDispatcher == null ||
                endpointDispatcher.DispatchRuntime == null ||
                endpointDispatcher.DispatchRuntime.MessageInspectors == null)
            {
                throw new ArgumentNullException("endpointDispatcher");
            }

            // assigns correlationidinspector for this endpoint
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new CorrelationIdInspectorBehavior());
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        #endregion

        #region Service behavior requirements

        public void AddBindingParameters(
            ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase,
            Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            if (serviceDescription == null)
            {
                throw new ArgumentNullException("serviceDescription");
            }

            foreach (var endpoint in serviceDescription.Endpoints)
            {
                // assigns this class as the behavior extender for all endpoints that are exposed
                // for this service
                endpoint.Behaviors.Add(new CorrelationIdInspectorServiceBehavior());
            }
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        #endregion
    }
}
