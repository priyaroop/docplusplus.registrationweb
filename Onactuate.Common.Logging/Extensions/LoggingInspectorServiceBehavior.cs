﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Onactuate.Common.Logging.Extensions
{
    public class LoggingInspectorServiceBehavior : IEndpointBehavior, IServiceBehavior
    {
        private readonly string _serviceName;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingInspectorServiceBehavior"/> class.
        /// </summary>
        public LoggingInspectorServiceBehavior(string serviceName)
        {
            _serviceName = serviceName;
        }

        #region Endpoint behavior requirements

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            if (endpointDispatcher == null ||
                endpointDispatcher.DispatchRuntime == null ||
                endpointDispatcher.DispatchRuntime.MessageInspectors == null)
            {
                throw new ArgumentNullException("endpointDispatcher");
            }

            // assigns correlationidinspector for this endpoint
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new LoggingInspectorBehavior(_serviceName));
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        #endregion

        #region Service behavior requirements

        public void AddBindingParameters(
            ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase,
            Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            if (serviceDescription == null)
            {
                throw new ArgumentNullException("serviceDescription");
            }

            foreach (var endpoint in serviceDescription.Endpoints)
            {
                // assigns this class as the behavior extender for all endpoints that are exposed
                // for this service
                endpoint.Behaviors.Add(new LoggingInspectorServiceBehavior(_serviceName));
            }
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        #endregion
    }
}
