﻿using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Impl;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public class TenantMasterService : ITenantMasterService
    {
        private ITenantMasterRepository TenantMasterRepositoryObj { get; set; }

        //TODO: Remove this and inject from Unity IoC
        public TenantMasterService() : this(new TenantMasterRepository())
        {
        }

        public TenantMasterService(ITenantMasterRepository tenantMasterRepositoryObj)
        {
            this.TenantMasterRepositoryObj = tenantMasterRepositoryObj;
        }

        public void CreateTenantMaster(TenantMaster tenantMaster)
        {
            TenantMasterRepositoryObj.CreateTenantMaster(tenantMaster);
        }

        public void CreateTenantRegistration(TenantRegistration tenantRegistration)
        {
            TenantMasterRepositoryObj.CreateTenantRegistration(tenantRegistration);
        }

        public void UpdateTenantRegistration(TenantRegistration tenantRegistration)
        {
            TenantMasterRepositoryObj.UpdateTenantRegistration(tenantRegistration);
        }

        public TenantRegistration GetTenantRegistrationById(int tenantRegistrationId)
        {
            return TenantMasterRepositoryObj.GetTenantRegistrationById(tenantRegistrationId);
        }

        public bool IsEmailAddressUnique(string emailAddress)
        {
            return TenantMasterRepositoryObj.IsEmailAddressUnique(emailAddress);
        }
    }
}