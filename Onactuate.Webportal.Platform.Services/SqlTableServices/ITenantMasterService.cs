﻿using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Platform.Services.SqlTableServices
{
    public interface ITenantMasterService
    {
        void CreateTenantMaster(TenantMaster tenantMaster);
        void CreateTenantRegistration(TenantRegistration tenantRegistration);
        void UpdateTenantRegistration(TenantRegistration tenantRegistration);
        TenantRegistration GetTenantRegistrationById(int tenantRegistrationId);
        bool IsEmailAddressUnique(string emailAddress);
    }
}