using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Layout;

namespace Onactuate.Common.Logger
{
    public static class Log
    {
        private static readonly Dictionary<Type, ILog> _loggers = new Dictionary<Type, ILog>();
        private static bool _logInitialized;
        private static readonly object _lock = new object();


        public static string SerializeObject(object objectToSerialize)
        {
            return SerializeObject(objectToSerialize, 0);
        }
        private static string SerializeObject(object objectToSerialize, int indentSize)
        {
            var objectProperties = new StringBuilder();
            PropertyInfo[] infos = objectToSerialize.GetType().GetProperties();
            foreach (PropertyInfo info in infos)
            {
                try
                {
                    object childObject = info.GetValue(objectToSerialize, new object[0]);
                    string childObjectValue = childObject == null ? "null" : childObject.ToString();
                    var name = objectToSerialize.GetType().Name;
                    objectProperties.Append(name.PadLeft(name.Length + indentSize) + "." + info.Name + "=" + childObjectValue + "\n");
                    if (childObject == null
                        || childObject is bool
                        || childObject is byte || childObject is sbyte
                        || childObject is short || childObject is ushort
                        || childObject is int || childObject is uint
                        || childObject is long || childObject is ulong
                        || childObject is float || childObject is double || childObject is decimal
                        || childObject is string || childObject is char
                        || childObject is Guid
                        || childObject is DateTime)
                    {
                        continue;
                    }

                    objectProperties.Append(SerializeObject(childObject, indentSize + name.Length + 1));
                }
                catch
                {
                }
            }
            return objectProperties.ToString();
        }

        public static string SerializeException(Exception e)
        {
            return SerializeException(e, string.Empty);
        }

        private static string SerializeException(Exception e, string exceptionMessage)
        {
            if (e == null) return string.Empty;

            exceptionMessage = string.Format(
                "{0}{1}{2}\n{3}",
                exceptionMessage,
                (exceptionMessage == string.Empty) ? string.Empty : "\n\n",
                e.Message,
                e.StackTrace);

            if (e.InnerException != null)
                exceptionMessage = SerializeException(e.InnerException, exceptionMessage);

            return exceptionMessage;
        }

        private static ILog getLogger(Type source)
        {
            lock (_lock)
            {
                if (_loggers.ContainsKey(source))
                {
                    return _loggers[source];
                }
                ILog logger = LogManager.GetLogger(source);
                _loggers.Add(source, logger);
                return logger;
            }
        }

        /* Log a message object */

        public static void Debug(object source, object message)
        {
            Debug(source.GetType(), message);
        }

        public static void Debug(Type source, object message)
        {
            getLogger(source).Debug(message);
        }

        public static void Info(object source, object message)
        {
            Info(source.GetType(), message);
        }

        public static void Info(Type source, object message)
        {
            getLogger(source).Info(message);
        }

        public static void Warn(object source, object message)
        {
            Warn(source.GetType(), message);
        }

        public static void Warn(Type source, object message)
        {
            getLogger(source).Warn(message);
        }

        public static void Error(object source, object message)
        {
            Error(source.GetType(), message);
        }

        public static void Error(Type source, object message)
        {
            getLogger(source).Error(message);
        }

        public static void Fatal(object source, object message)
        {
            Fatal(source.GetType(), message);
        }

        public static void Fatal(Type source, object message)
        {
            getLogger(source).Fatal(message);
        }

        /* Log a message object and exception */

        public static void Debug(object source, object message, Exception exception)
        {
            Debug(source.GetType(), message, exception);
        }

        public static void Debug(Type source, object message, Exception exception)
        {
            getLogger(source).Debug(message, exception);
        }

        public static void Info(object source, object message, Exception exception)
        {
            Info(source.GetType(), message, exception);
        }

        public static void Info(Type source, object message, Exception exception)
        {
            getLogger(source).Info(message, exception);
        }

        public static void Warn(object source, object message, Exception exception)
        {
            Warn(source.GetType(), message, exception);
        }

        public static void Warn(Type source, object message, Exception exception)
        {
            getLogger(source).Warn(message, exception);
        }

        public static void Error(object source, object message, Exception exception)
        {
            Error(source.GetType(), message, exception);
        }

        public static void Error(Type source, object message, Exception exception)
        {
            getLogger(source).Error(message, exception);
        }

        public static void Fatal(object source, object message, Exception exception)
        {
            Fatal(source.GetType(), message, exception);
        }

        public static void Fatal(Type source, object message, Exception exception)
        {
            getLogger(source).Fatal(message, exception);
        }

        private static void initialize()
        {
            string logFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log4Net.config");
            if (!File.Exists(logFilePath))
            {
                logFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Log4Net.config");
            }

            XmlConfigurator.ConfigureAndWatch(new FileInfo(logFilePath));
        }

        public static void EnsureInitialized()
        {
            if (!_logInitialized)
            {
                initialize();
                _logInitialized = true;
            }
        }

        public static void EnsureInitializedForTesting()
        {
            if (!_logInitialized)
            {
                var appender1 = new OutputDebugStringAppender { Layout = new PatternLayout("%-5p %m - %c -%n") };
                BasicConfigurator.Configure(appender1);

                var appender2 = new TraceAppender
                {
                    Layout = new PatternLayout("%d [%t] %-5p %c - %m%n")
                };
                BasicConfigurator.Configure(appender2);

                _logInitialized = true;
            }
        }
    }
}