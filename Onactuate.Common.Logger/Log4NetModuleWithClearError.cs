using System;
using System.Web;

namespace Onactuate.Common.Logger
{
    public class Log4NetModuleWithClearError : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            Log.EnsureInitialized();
            context.Error += context_Error;
        }

        void context_Error(object sender, EventArgs e)
        {
            Exception exception = HttpContext.Current.Server.GetLastError();
            Log.Fatal(this, exception.Message, exception);
            HttpContext.Current.Server.ClearError();
            HttpContext.Current.Response.Redirect("~/Error.aspx");
        }

        public void Dispose()
        {
        }
    }
}