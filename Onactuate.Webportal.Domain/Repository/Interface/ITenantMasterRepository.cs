﻿using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Repository.Interface
{
    public interface ITenantMasterRepository
    {
        void CreateTenantMaster(TenantMaster tenantMaster);
        void CreateTenantRegistration(TenantRegistration tenantRegistration);
        void UpdateTenantRegistration(TenantRegistration tenantRegistration);
        TenantRegistration GetTenantRegistrationById(int tenantRegistrationId);
        bool IsEmailAddressUnique(string emailAddress);
    }
}