using System;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Transactions;
using Onactuate.Common.DataAccess;
using Onactuate.Webportal.Domain.Model;
using Onactuate.Webportal.Domain.Repository.Interface;

namespace Onactuate.Webportal.Domain.Repository.Impl
{
    public class TenantMasterRepository : RepositoryBase, ITenantMasterRepository
    {
        public TenantMasterRepository()
        {
        }

        protected TenantMasterRepository(ObjectContext objectContext) : base(objectContext)
        {
        }

        private TransactionScope CreateTransactionScope()
        {
            TransactionOptions transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }
        
        public void CreateTenantMaster(TenantMaster tenantMaster)
        {
            base.SaveChanges<TenantMaster>(tenantMaster);
        }

        public void CreateTenantRegistration(TenantRegistration tenantRegistration)
        {
            base.SaveChanges<TenantRegistration>(tenantRegistration);
        }

        public void UpdateTenantRegistration(TenantRegistration tenantRegistration)
        {
            base.Update<TenantRegistration>(tenantRegistration);
        }

        public TenantRegistration GetTenantRegistrationById(int tenantRegistrationId)
        {
            return base.GetQuery<TenantRegistration>(t => t.Id.Equals(tenantRegistrationId)).FirstOrDefault();
        }
        
        public bool IsEmailAddressUnique(string emailAddress)
        {
            return !base.GetQuery<TenantRegistration>(t => t.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase)).Any();
        }

        #region EXAMPLES - TO DELETE LATER
        //public void CreateTaskMaster(TaskMaster taskMaster)
        //{
        //    base.SaveChanges<TaskMaster>(taskMaster);
        //}

        //public void UpdateTaskMaster(TaskMaster taskMaster)
        //{
        //    base.Update<TaskMaster>(taskMaster);
        //}

        //public void DeleteTaskMaster(TaskMaster taskMaster)
        //{
        //    Delete<TaskMaster>(taskMaster);
        //}

        //public void DeleteTaskMasterList(IList<TaskMaster> taskMasterList)
        //{
        //    TransactionScope scope = this.CreateTransactionScope();
        //    try
        //    {
        //        foreach (TaskMaster taskMaster in taskMasterList)
        //        {
        //            DeleteTaskMaster(taskMaster);
        //        }

        //        scope.Complete();
        //    }
        //    catch (Exception exception)
        //    {
        //        string message = exception.Message;
        //        if (exception.InnerException != null)
        //        {
        //            message = exception.InnerException.Message;
        //        }
        //        throw new Exception(message, exception);
        //    }
        //    finally
        //    {
        //        if (scope != null)
        //        {
        //            scope.Dispose();
        //        }
        //    }
        //}

        //public IList<TaskMaster> GetTaskMasterListByPermissionId(int permissionId)
        //{
        //    return base.GetQuery<TaskMaster>(p => p.OwnerPermissionId.Equals(permissionId) || p.AssignedToPermissionId.Equals(permissionId)).ToList();
        //}

        //public PagingResultset<TaskMaster> GetPagedTaskMasterList(PagingParameters<TaskMaster> parameters)
        //{
        //    return base.GetPagedResultset(parameters);
        //}

        //public TaskMaster GetPagedTaskMasterById(int taskId)
        //{
        //    return base.GetQuery<TaskMaster>(t => t.Id.Equals(taskId)).FirstOrDefault();
        //}

        //public TaskMaster GetTaskMasterById(int taskId)
        //{
        //    return base.GetQuery<TaskMaster>(t => t.Id.Equals(taskId)).FirstOrDefault();
        //}
        #endregion
    }
}