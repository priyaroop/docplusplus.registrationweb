﻿using System.Data.Entity.ModelConfiguration;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class TenantRegistrationMapping : EntityTypeConfiguration<TenantRegistration>
    {
        public TenantRegistrationMapping()
        {
            base.HasKey<int>(t => t.Id);
            base.Property(t => t.EmailAddress);
            base.Property(t => t.TenantName);
            base.Property(t => t.RegistrationStatus);
            base.Property(t => t.RegistrationStatusMessage);
            base.Property(t => t.TenantMasterId);
            base.Property(t => t.CreationTime);
            base.Property(t => t.LastUpdated);
            base.Property(t => t.ErrorMessage);

            base.ToTable("TenantRegistration");
        }
    }
}