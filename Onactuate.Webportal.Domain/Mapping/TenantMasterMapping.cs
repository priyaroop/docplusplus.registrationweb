﻿using System.Data.Entity.ModelConfiguration;
using Onactuate.Webportal.Domain.Model;

namespace Onactuate.Webportal.Domain.Mapping
{
    public class TenantMasterMapping : EntityTypeConfiguration<TenantMaster>
    {
        public TenantMasterMapping()
        {
            base.HasKey<int>(t => t.Id);
            base.Property(t => t.Firstname);
            base.Property(t => t.Lastname);
            base.Property(t => t.Company);
            base.Property(t => t.CompanyEmail);
            base.Property(t => t.AdminUsername);
            base.Property(t => t.AdminPassword);
            base.Property(t => t.PhoneNumber);
            base.Property(t => t.SubscriptionPlan);
            base.Property(t => t.ExpirationDate);
            base.Property(t => t.CafrUrl);
            base.Property(t => t.CafrDatabase);
            base.Property(t => t.CafrDatabaseConnStr);

            base.ToTable("TenantMaster");
        }
    }
}