﻿using System;

namespace Onactuate.Webportal.Domain.Model
{
    public class TenantRegistration
    {
        public virtual int Id { get; set; }
        public virtual string EmailAddress { get; set; }
        public virtual string TenantName { get; set; }
        public virtual int RegistrationStatus { get; set; }
        public virtual string RegistrationStatusMessage { get; set; }
        public virtual int TenantMasterId { get; set; }
        public virtual DateTime CreationTime { get; set; }
        public virtual DateTime LastUpdated { get; set; }
        public virtual string ErrorMessage { get; set; }
    }
}