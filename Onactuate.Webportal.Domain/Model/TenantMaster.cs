﻿using System;

namespace Onactuate.Webportal.Domain.Model
{
    public class TenantMaster
    {
        public virtual int Id { get; set; }
        public virtual string Firstname { get; set; }
        public virtual string Lastname { get; set; }
        public virtual string Company { get; set; }
        public virtual string CompanyEmail { get; set; }
        public virtual string AdminUsername { get; set; }
        public virtual string AdminPassword { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual int SubscriptionPlan { get; set; }
        public virtual DateTime ExpirationDate { get; set; }
        public virtual string CafrUrl { get; set; }
        public virtual string CafrDatabase { get; set; }
        public virtual string CafrDatabaseConnStr { get;set; }
    }
}