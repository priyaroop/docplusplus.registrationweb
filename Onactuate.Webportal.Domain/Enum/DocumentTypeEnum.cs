﻿namespace Onactuate.Webportal.Domain.Enum
{
    public enum DocumentTypeEnum
    {
        Word = 1,
        SharePoint = 2,
        OneDrive = 3
    }
}