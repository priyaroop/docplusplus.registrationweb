﻿namespace Onactuate.Webportal.Domain.Enum
{
    public enum PermissionTypeEnum
    {
        AuthorizedWithDelegation = 0,
        Authorized = 1,
        NotAuthorized = 2
    }
}