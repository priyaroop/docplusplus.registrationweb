﻿
Create PROCEDURE [dbo].[sp_insertColumn] 
	@ColumnName nvarchar(250)
	
AS
BEGIN
	
	SET NOCOUNT ON;

	Insert into [dbo].[ReportColumns]([Name])
	Values (@ColumnName)
    
END
