﻿
CREATE PROCEDURE [dbo].[sp_insertReportCategoryMap] 
	@reportId int,
	@categoryId int,
	@subCategoryId int,
	@CurrentDate DateTime
	
AS
BEGIN
	
	SET NOCOUNT ON;

	INSERT INTO [dbo].[ReportCategoryMap] (ReportId, CategoryId , SubCategoryId,CurrentDate) 
    VALUES (@reportId, @categoryId, @subCategoryId,@CurrentDate)
    
END
