﻿
CREATE PROCEDURE [dbo].[sp_getReportTemplateData]
	
AS
BEGIN
	
	SET NOCOUNT ON;

select DISTINCT rc.Name as Category,
rsc.Name as SubCategory, 
rcol.Name as ColumnName, 
Sum(rtb.closingBalance) as ClosingBalance,
rcm.reportId as ReportId
from [dbo].[ReportCafrMap] as rcm
JOIN [dbo].[ReportCategory] as rc
ON rc.Id = rcm.CategoryId
JOIN [dbo].[ReportSubCategory] as rsc
ON rsc.Id = rcm.SubCategoryId
JOIN [dbo].[ReportColumns] as rcol
ON rcol.Id = rcm.ColumnId
JOIN [dbo].[ReportTrialBalance] as rtb
ON rtb.Id = rcm.TrialBalanceId
GROUP BY rc.Name,rsc.Name,rcol.Name,rcm.reportId
Having rcm.reportId=1

END
