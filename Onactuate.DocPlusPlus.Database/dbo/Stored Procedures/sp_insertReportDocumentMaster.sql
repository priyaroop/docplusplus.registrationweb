﻿
CREATE PROCEDURE [dbo].[sp_insertReportDocumentMaster] 
	@DocumentName nvarchar(500),
	@DocumentDescription nvarchar(500),
	@UploadFileName nvarchar(2000),
	@UploadedOn datetime,
	@UploadedByPermissionId int,
	@HasBeenOpened bit,
	@DocumentType int
	
AS
BEGIN
	
	SET NOCOUNT ON;

	Insert into [dbo].[DocumentMaster] ([DocumentName],[DocumentDescription],[UploadFileName],[UploadedOn],[UploadedByPermissionId],
	[HasBeenOpened],[DocumentType])
	Values (@DocumentName,@DocumentDescription,@UploadFileName,@UploadedOn,@UploadedByPermissionId,@HasBeenOpened,@DocumentType)
    
END
