﻿
CREATE PROCEDURE [dbo].[sp_getAllCategoryDetails]
	
AS
BEGIN
	
	SET NOCOUNT ON;

    select 
rs.Id as SubCategoryId,
rc.Id as CategoryId, 
rc.Name as CategoryName,
rs.Name as SubCategoryName 
from [dbo].[ReportSubCategory] as rs
JOIN [dbo].[ReportCategory] as rc 
ON rs.CategoryId = rc.Id

END
