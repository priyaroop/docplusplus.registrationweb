﻿
CREATE PROCEDURE [dbo].[sp_getReportTemplate]
	
AS
BEGIN
	
	SET NOCOUNT ON;

SELECT DISTINCT columnId,columnName,categoryname,subcategoryname,closingBalance from
(
SELECT DISTINCT
rc.ColumnId as ColumnId, rc.ReportId,rcol.Name as columnName,
sum(tb.ClosingBalance) as ClosingBalance
from [dbo].[ReportTrialBalance]  as tb
INNER JOIN [dbo].[ReportColumnTB] as rc
ON tb.Id = rc.TrialBalanceId
INNER JOIN [dbo].[ReportColumns] as rcol
ON rcol.Id = rc.ColumnId
GROUP BY rc.ColumnId,rc.ReportId,rcol.Name) 
as cbal
Inner join (select Subcategoryname,Categoryname,rcmReportId
from (
select rsc.Name as Subcategoryname,rc.Name as Categoryname,rcm.ReportId as rcmreportid
from [dbo].[ReportCategoryMap] as rcm
INNER JOIN [dbo].[ReportSubCategory] as rsc
ON rcm.SubCategoryId = rsc.Id
INNER JOIN [dbo].[ReportCategory] as rc
ON rc.Id = rsc.CategoryId) as rsc
)dd 
on rcmReportId=ReportId

END
