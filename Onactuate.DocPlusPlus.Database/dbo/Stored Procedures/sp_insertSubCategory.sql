﻿
CREATE PROCEDURE [dbo].[sp_insertSubCategory] 
    @CategoryId int,
	@SubCategoryName nvarchar(500)
	
AS
BEGIN
	
	SET NOCOUNT ON;

	Insert into [dbo].[ReportSubCategory]([CategoryId],[Name])
	Values ( @CategoryId, @SubCategoryName)
    
END
