﻿CREATE TABLE [dbo].[CafrTemplateMaster] (
    [Id]                     INT             IDENTITY (1, 1) NOT NULL,
    [TemplateName]           NVARCHAR (500)  NOT NULL,
    [TemplateDescription]    NVARCHAR (500)  NULL,
    [UploadFileName]         NVARCHAR (2000) NOT NULL,
    [UploadFilePath]         NVARCHAR (4000) NOT NULL,
    [UploadedOn]             DATETIME        NOT NULL,
    [UploadedByPermissionId] INT             NOT NULL,
    CONSTRAINT [PK_CafrTemplateMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

