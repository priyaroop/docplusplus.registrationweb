﻿CREATE TABLE [dbo].[ReportSubCategory] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [CategoryId] INT            NOT NULL,
    [Name]       NVARCHAR (500) NULL,
    CONSTRAINT [PK_ReportSubCategory] PRIMARY KEY CLUSTERED ([Id] ASC)
);

