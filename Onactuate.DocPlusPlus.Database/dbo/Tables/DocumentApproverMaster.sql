﻿CREATE TABLE [dbo].[DocumentApproverMaster] (
    [Id]           INT IDENTITY (1, 1) NOT NULL,
    [DocumentId]   INT NOT NULL,
    [PermissionId] INT NOT NULL,
    CONSTRAINT [PK_DocumentApproverMaster] PRIMARY KEY CLUSTERED ([Id] ASC, [DocumentId] ASC, [PermissionId] ASC),
    CONSTRAINT [FK_DocumentApproverMaster_ApplicationPermission] FOREIGN KEY ([PermissionId]) REFERENCES [dbo].[ApplicationPermission] ([PermissionId]),
    CONSTRAINT [FK_DocumentApproverMaster_DocumentMaster] FOREIGN KEY ([DocumentId]) REFERENCES [dbo].[DocumentMaster] ([Id])
);

