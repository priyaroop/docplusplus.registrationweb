﻿CREATE TABLE [dbo].[WorkflowMaster] (
    [Id]                  INT             IDENTITY (1, 1) NOT NULL,
    [WorkflowName]        NVARCHAR (250)  NOT NULL,
    [WorkflowDescription] NVARCHAR (2000) NULL,
    CONSTRAINT [PK_WorkflowMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

