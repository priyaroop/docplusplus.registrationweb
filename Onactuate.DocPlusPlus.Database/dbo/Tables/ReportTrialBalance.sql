﻿CREATE TABLE [dbo].[ReportTrialBalance] (
    [Id]             INT             IDENTITY (1, 1) NOT NULL,
    [SubCategoryId]  INT             NULL,
    [Fund]           INT             NULL,
    [MainAccount]    INT             NULL,
    [Division]       INT             NULL,
    [Name]           NVARCHAR (MAX)  NULL,
    [OpeningBalance] DECIMAL (18, 2) NULL,
    [Debit]          DECIMAL (18, 2) NULL,
    [Credit]         DECIMAL (18, 2) NULL,
    [ClosingBalance] DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_TrialBalance] PRIMARY KEY CLUSTERED ([Id] ASC)
);

