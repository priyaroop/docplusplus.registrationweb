﻿CREATE TABLE [dbo].[TaskComment] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [TaskId]    INT            NOT NULL,
    [Comments]  NVARCHAR (MAX) NOT NULL,
    [CreatedOn] DATETIME       NULL,
    [CreatedBy] INT            NULL,
    CONSTRAINT [PK_TaskComment] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TaskComment_TaskMaster] FOREIGN KEY ([TaskId]) REFERENCES [dbo].[TaskMaster] ([Id])
);

