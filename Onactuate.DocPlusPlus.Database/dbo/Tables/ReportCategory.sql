﻿CREATE TABLE [dbo].[ReportCategory] (
    [Id]   INT            IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (250) NULL,
    CONSTRAINT [PK_ReportCategory] PRIMARY KEY CLUSTERED ([Id] ASC)
);

