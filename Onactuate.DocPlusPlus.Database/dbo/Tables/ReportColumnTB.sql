﻿CREATE TABLE [dbo].[ReportColumnTB] (
    [Id]             INT      IDENTITY (1, 1) NOT NULL,
    [ReportId]       INT      NULL,
    [ColumnId]       INT      NULL,
    [TrialBalanceId] INT      NULL,
    [CurrentDate]    DATETIME NULL,
    CONSTRAINT [PK_ReportColumnTB] PRIMARY KEY CLUSTERED ([Id] ASC)
);

