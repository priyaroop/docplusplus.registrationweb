﻿CREATE TABLE [dbo].[CafrDataReports] (
    [Id]                     INT             IDENTITY (1, 1) NOT NULL,
    [DataReportName]         NVARCHAR (500)  NOT NULL,
    [DataReportDescription]  NVARCHAR (2000) NULL,
    [UploadFileName]         NVARCHAR (2000) NOT NULL,
    [UploadFilePath]         NVARCHAR (4000) NOT NULL,
    [UploadedOn]             DATETIME        NOT NULL,
    [UploadedByPermissionId] INT             NOT NULL,
    CONSTRAINT [PK_CafrDataReports] PRIMARY KEY CLUSTERED ([Id] ASC)
);

