﻿CREATE TABLE [dbo].[ApproverGroupMaster] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [GroupName] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ApproverGroupMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

