﻿CREATE TABLE [dbo].[SharepointMaster] (
    [Id]                           INT             IDENTITY (1, 1) NOT NULL,
    [DocumentMasterId]             INT             NOT NULL,
    [SharepointUrl]                NVARCHAR (MAX)  NOT NULL,
    [SharepointUsername]           NVARCHAR (MAX)  NOT NULL,
    [SharepointPassword]           VARBINARY (MAX) NOT NULL,
    [SharepointRelativeFolderPath] NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_SharepointMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SharepointMaster_DocumentMaster] FOREIGN KEY ([DocumentMasterId]) REFERENCES [dbo].[DocumentMaster] ([Id])
);

