﻿CREATE TABLE [dbo].[DocumentType] (
    [Id]           INT           NOT NULL,
    [DocumentType] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_DocumentType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

