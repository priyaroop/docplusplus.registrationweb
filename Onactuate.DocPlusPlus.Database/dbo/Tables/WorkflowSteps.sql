﻿CREATE TABLE [dbo].[WorkflowSteps] (
    [Id]               INT             IDENTITY (1, 1) NOT NULL,
    [WorkflowMasterId] INT             NOT NULL,
    [StepName]         NVARCHAR (250)  NOT NULL,
    [StepDescription]  NVARCHAR (2000) NULL,
    CONSTRAINT [PK_WorkflowSteps] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_WorkflowSteps_WorkflowMaster] FOREIGN KEY ([WorkflowMasterId]) REFERENCES [dbo].[WorkflowMaster] ([Id])
);

