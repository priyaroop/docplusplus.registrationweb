﻿CREATE TABLE [dbo].[ReportCategoryMap] (
    [Id]            INT      IDENTITY (1, 1) NOT NULL,
    [ReportId]      INT      NULL,
    [CategoryId]    INT      NULL,
    [SubCategoryId] INT      NULL,
    [CurrentDate]   DATETIME NULL,
    CONSTRAINT [PK_ReportCategoryMap] PRIMARY KEY CLUSTERED ([Id] ASC)
);

