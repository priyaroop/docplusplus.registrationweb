﻿CREATE TABLE [dbo].[WorkflowStepApprovers] (
    [Id]             INT IDENTITY (1, 1) NOT NULL,
    [WorkflowStepId] INT NOT NULL,
    [PermissionId]   INT NOT NULL,
    CONSTRAINT [PK_WorkflowStepApprovers] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_WorkflowStepApprovers_ApproverGroupMaster] FOREIGN KEY ([PermissionId]) REFERENCES [dbo].[ApplicationPermission] ([PermissionId]),
    CONSTRAINT [FK_WorkflowStepApprovers_WorkflowSteps] FOREIGN KEY ([WorkflowStepId]) REFERENCES [dbo].[WorkflowSteps] ([Id])
);

