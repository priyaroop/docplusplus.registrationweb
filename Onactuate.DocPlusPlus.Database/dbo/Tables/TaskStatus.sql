﻿CREATE TABLE [dbo].[TaskStatus] (
    [Id]                    INT            NOT NULL,
    [TaskStatusDescription] NVARCHAR (500) NOT NULL,
    CONSTRAINT [PK_TaskStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

