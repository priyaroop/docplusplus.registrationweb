﻿CREATE TABLE [dbo].[WorkflowStatus] (
    [ID]         INT           NOT NULL,
    [StatusName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_WorkflowStatus] PRIMARY KEY CLUSTERED ([ID] ASC)
);

