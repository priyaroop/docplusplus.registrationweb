﻿CREATE TABLE [dbo].[ReportCafr] (
    [Id]   INT            IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (250) NULL,
    [Text] NVARCHAR (500) NULL,
    CONSTRAINT [PK_ReportCafr] PRIMARY KEY CLUSTERED ([Id] ASC)
);

