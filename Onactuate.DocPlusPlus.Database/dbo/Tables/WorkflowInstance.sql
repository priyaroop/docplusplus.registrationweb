﻿CREATE TABLE [dbo].[WorkflowInstance] (
    [Id]                    INT IDENTITY (1, 1) NOT NULL,
    [WorkflowMasterId]      INT NOT NULL,
    [CurrentWorkflowStepId] INT NOT NULL,
    [CurrentStatusId]       INT NOT NULL,
    [PermissionId]          INT NOT NULL,
    CONSTRAINT [PK_WorkflowInstance] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_WorkflowInstance_ApplicationPermission] FOREIGN KEY ([PermissionId]) REFERENCES [dbo].[ApplicationPermission] ([PermissionId]),
    CONSTRAINT [FK_WorkflowInstance_WorkflowMaster] FOREIGN KEY ([WorkflowMasterId]) REFERENCES [dbo].[WorkflowMaster] ([Id]),
    CONSTRAINT [FK_WorkflowInstance_WorkflowStatus] FOREIGN KEY ([CurrentStatusId]) REFERENCES [dbo].[WorkflowStatus] ([ID]),
    CONSTRAINT [FK_WorkflowInstance_WorkflowSteps] FOREIGN KEY ([CurrentWorkflowStepId]) REFERENCES [dbo].[WorkflowSteps] ([Id])
);

