﻿CREATE TABLE [dbo].[ApproverGroupMember] (
    [Id]              INT IDENTITY (1, 1) NOT NULL,
    [ApproverGroupId] INT NOT NULL,
    [PermissionId]    INT NOT NULL,
    CONSTRAINT [PK_ApproverGroupMember] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ApproverGroupMember_ApplicationPermission] FOREIGN KEY ([PermissionId]) REFERENCES [dbo].[ApplicationPermission] ([PermissionId]),
    CONSTRAINT [FK_ApproverGroupMember_ApproverGroupMaster] FOREIGN KEY ([ApproverGroupId]) REFERENCES [dbo].[ApproverGroupMaster] ([Id])
);

