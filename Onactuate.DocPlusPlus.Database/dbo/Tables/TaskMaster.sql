﻿CREATE TABLE [dbo].[TaskMaster] (
    [Id]                     INT             IDENTITY (1, 1) NOT NULL,
    [TaskName]               NVARCHAR (1000) NOT NULL,
    [OwnerPermissionId]      INT             NOT NULL,
    [AssignedToPermissionId] INT             NOT NULL,
    [CompletionDate]         DATETIME        NOT NULL,
    [TaskStatusId]           INT             NOT NULL,
    [IsNewTask]              BIT             CONSTRAINT [DF_TaskMaster_IsNewTask] DEFAULT ((1)) NOT NULL,
    [TaskDocumentName]       NVARCHAR (1000) NOT NULL,
    [TaskDocumentCopyName]   NVARCHAR (1000) NULL,
    [CreatedOn]              DATETIME        NOT NULL,
    [MasterDocumentId]       INT             NOT NULL,
    [ApproverGroupId]        INT             NOT NULL,
    CONSTRAINT [PK_TaskMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TaskMaster_ApplicationPermission] FOREIGN KEY ([OwnerPermissionId]) REFERENCES [dbo].[ApplicationPermission] ([PermissionId]),
    CONSTRAINT [FK_TaskMaster_ApplicationPermission1] FOREIGN KEY ([AssignedToPermissionId]) REFERENCES [dbo].[ApplicationPermission] ([PermissionId]),
    CONSTRAINT [FK_TaskMaster_DocumentMaster] FOREIGN KEY ([MasterDocumentId]) REFERENCES [dbo].[DocumentMaster] ([Id]),
    CONSTRAINT [FK_TaskMaster_TaskStatus] FOREIGN KEY ([TaskStatusId]) REFERENCES [dbo].[TaskStatus] ([Id])
);

