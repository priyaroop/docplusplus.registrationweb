﻿CREATE TABLE [dbo].[ReportCafrMap] (
    [Id]             INT      IDENTITY (1, 1) NOT NULL,
    [ReportId]       INT      NULL,
    [CategoryId]     INT      NULL,
    [SubCategoryId]  INT      NULL,
    [ColumnId]       INT      NULL,
    [TrialBalanceId] INT      NULL,
    [CurrentDate]    DATETIME NULL,
    CONSTRAINT [PK_ReportCafrMap] PRIMARY KEY CLUSTERED ([Id] ASC)
);

