﻿CREATE TABLE [dbo].[ApplicationPermission] (
    [PermissionId]     INT             IDENTITY (1, 1) NOT NULL,
    [UserName]         NVARCHAR (500)  NOT NULL,
    [Password]         NVARCHAR (2000) NOT NULL,
    [IsDeletable]      BIT             NULL,
    [PermissionTypeId] INT             NULL,
    CONSTRAINT [PK_ApplicationPermission] PRIMARY KEY CLUSTERED ([PermissionId] ASC)
);

