﻿CREATE TABLE [dbo].[ReportColumns] (
    [Id]   INT            IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (100) NULL,
    CONSTRAINT [PK_ReportColumns] PRIMARY KEY CLUSTERED ([Id] ASC)
);

