﻿CREATE TABLE [dbo].[DocumentMaster] (
    [Id]                     INT             IDENTITY (1, 1) NOT NULL,
    [DocumentName]           NVARCHAR (500)  NOT NULL,
    [DocumentDescription]    NVARCHAR (500)  NULL,
    [UploadFileName]         NVARCHAR (2000) NOT NULL,
    [UploadedOn]             DATETIME        NOT NULL,
    [UploadedByPermissionId] INT             NOT NULL,
    [HasBeenOpened]          BIT             CONSTRAINT [DF_DocumentMaster_HasBeenOpened] DEFAULT ((0)) NOT NULL,
    [DocumentType]           INT             NOT NULL,
    CONSTRAINT [PK_DocumentMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DocumentMaster_ApplicationPermission] FOREIGN KEY ([UploadedByPermissionId]) REFERENCES [dbo].[ApplicationPermission] ([PermissionId]),
    CONSTRAINT [FK_DocumentMaster_DocumentType] FOREIGN KEY ([DocumentType]) REFERENCES [dbo].[DocumentType] ([Id])
);

