﻿CREATE TABLE [dbo].[UserDocuments] (
    [Id]         INT      IDENTITY (1, 1) NOT NULL,
    [DocumentId] INT      NULL,
    [UserId]     INT      NULL,
    [CreatedOn]  DATETIME NULL,
    [CreatedBy]  INT      NULL,
    CONSTRAINT [PK_UserDocuments] PRIMARY KEY CLUSTERED ([Id] ASC)
);

