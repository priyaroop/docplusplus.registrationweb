﻿namespace Onactuate.Common.Utilities.ObjectStore
{
    public interface IStore<T, K>
    {
        K Get(T t);
        void Put(T key, K purchaseModel, int expirtyMinutes);
        K Remove(T key);
    }
}

