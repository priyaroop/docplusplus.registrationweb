﻿using System;
using System.Web;
using System.Web.Caching;

namespace Onactuate.Common.Utilities.ObjectStore
{
    public class CacheObjectStore<T, K> : IStore<T, K>
    {
        private readonly Cache _cache;

        public CacheObjectStore()
        {
            if (HttpContext.Current != null)
            {
                this._cache = HttpContext.Current.Cache;
            }
        }

        public K Get(T t)
        {
            return (K) this._cache.Get(t.ToString());
        }

        public void Put(T key, K purchaseModel, int expirtyMinutes)
        {
            this._cache.Insert(key.ToString(), purchaseModel, null, DateTime.Now.AddMinutes((double) expirtyMinutes), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
        }

        public K Remove(T key)
        {
            K local = this.Get(key);
            HttpContext.Current.Cache.Remove(key.ToString());
            return local;
        }
    }
}

