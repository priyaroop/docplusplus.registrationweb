using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Onactuate.Common.Utilities.Extensions
{
    public static class MemberInfoExtensions
    {
        /// <summary>
        /// Returns the single specified TAttribute decorating the decoratedMemberInfo
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="decoratedMemberInfo"></param>
        /// <returns></returns>
        public static TAttribute GetAttribute<TAttribute>(this MemberInfo decoratedMemberInfo)
            where TAttribute : Attribute
        {
            return (TAttribute) (Attribute.GetCustomAttribute(decoratedMemberInfo, typeof (TAttribute), inherit: false));
        }

        /// <summary>
        /// Returns all the TAttributes decorating the decoratedMemberInfo
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="decoratedMemberInfo"></param>
        /// <returns></returns>
        public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this MemberInfo decoratedMemberInfo)
            where TAttribute : Attribute
        {
            return Attribute.GetCustomAttributes(decoratedMemberInfo, typeof(TAttribute), inherit: false).Cast<TAttribute>();
        }
    }
}