using System;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace Onactuate.Common.Utilities.Wcf
{
    /// <summary>
    /// To use this class for installing windows service, you need a project installer (which inherits from DynamicInstaller) added to your assembly
    /// </summary>
    public class WindowsServiceInstallUtil
    {
        /// <summary>
        /// Path to folder where InstallUtil (.Net SDK) stored
        /// </summary>
        public static string InstallUtilPath = System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory(); //@"C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727";

        protected WindowsServiceInstallInfo WsInstallInfo;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wsInstallInfo"></param>
        public WindowsServiceInstallUtil(WindowsServiceInstallInfo wsInstallInfo)
        {
            WsInstallInfo = wsInstallInfo;
        }


        /// <summary>
        /// Run InstallUtil with specific params
        /// </summary>
        /// <param name="installUtilArguments">CommandLine params</param>
        /// <returns>Status of installation</returns>
        private static bool CallInstallUtil(string installUtilArguments)
        {
            Process proc = new Process();
            proc.StartInfo.FileName = Path.Combine(InstallUtilPath, "installutil.exe");
            proc.StartInfo.Arguments = installUtilArguments;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.UseShellExecute = false;

            proc.Start();
            string outputResult = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();

            // check result
            if (proc.ExitCode != 0)
            {
                //Console.WriteLine("{0} : failed with code {1}", DateTime.Now, proc.ExitCode);
                //Console.WriteLine(outputResult);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Install windows service
        /// </summary>
        /// <returns></returns>
        public bool Install()
        {
            return Install("");
        }
        /// <summary>
        /// Install windows service
        /// </summary>
        /// <param name="logFilePath">Log file to store installation information</param>
        /// <returns></returns>
        public virtual bool Install(string logFilePath)
        {
            string installUtilArguments = GenerateInstallutilInstallArgs(logFilePath);

            return CallInstallUtil(installUtilArguments);
        }
        protected string GenerateInstallutilInstallArgs(string logFilePath)
        {
            string installUtilArguments = " /account=\"" + WsInstallInfo.WsAccountType + "\"";
            if (WsInstallInfo.WindowsServiceName != "")
            {
                installUtilArguments += " /name=\"" + WsInstallInfo.WindowsServiceName + "\"";
            }
            if (WsInstallInfo.Description != "")
            {
                installUtilArguments += " /desc=\"" + WsInstallInfo.Description + "\"";
            }
            if (WsInstallInfo.WsAccountType == WindowsServiceAccountType.User)
            {
                installUtilArguments += " /user=\"" + WsInstallInfo.WsAccountUserName + "\" /password=\"" + WsInstallInfo.WsAccountPassword + "\"";
            }
            installUtilArguments += " \"" + Path.Combine(WsInstallInfo.WindowsServicePath, WsInstallInfo.WindowsServiceAssemblyName) + "\"";
            if (logFilePath.Trim() != "")
            {
                installUtilArguments += " /LogFile=\"" + logFilePath + "\"";
            }

            return installUtilArguments;
        }

        /// <summary>
        /// Uninstall windows service
        /// </summary>
        /// <returns></returns>
        public bool Uninstall()
        {
            return Uninstall("");
        }
        /// <summary>
        /// Uninstall windows service
        /// </summary>
        /// <param name="logFilePath">Log file to store installation information</param>
        /// <returns></returns>
        public virtual bool Uninstall(string logFilePath)
        {
            string installUtilArguments = GenerateInstallutilUninstallArgs(logFilePath);

            return CallInstallUtil(installUtilArguments);
        }

        protected string GenerateInstallutilUninstallArgs(string logFilePath)
        {
            string installUtilArguments = " /u ";
            if (WsInstallInfo.WindowsServiceName != "")
            {
                installUtilArguments += " /name=\"" + WsInstallInfo.WindowsServiceName + "\"";
            }
            installUtilArguments += " \"" + Path.Combine(WsInstallInfo.WindowsServicePath, WsInstallInfo.WindowsServiceAssemblyName) + "\"";
            if (logFilePath.Trim() != "")
            {
                installUtilArguments += " /LogFile=\"" + logFilePath + "\"";
            }

            return installUtilArguments;
        }
    }

    /// <summary>
    /// Install windows service in local machine or remote machine
    /// To use this class for installing windows service, you need a project installer module added to your assembly
    /// </summary>
    public class RemoteWindowsServiceInstallUtil : WindowsServiceInstallUtil
    {
        private readonly string _remoteMachine;

        public RemoteWindowsServiceInstallUtil(WindowsServiceInstallInfo wsInstallInfo) : this("localhost", wsInstallInfo) { }
        public RemoteWindowsServiceInstallUtil(string remoteMachine, WindowsServiceInstallInfo wsInstallInfo)
            : base(wsInstallInfo)
        {
            _remoteMachine = remoteMachine;
        }

        private static string _psToolsPath = Path.Combine(Directory.GetCurrentDirectory(), "PsTools");
        ///<summary>
        ///</summary>
        public static string PsToolsPath
        {
            get { return _psToolsPath; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value", "Cannot provide a null PS tools path.");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Cannot provide an empty PS tools path.", "value");

                if (_psToolsPath == value)
                    return;

                string psExecPath = Path.Combine(value, "psexec.exe");

                if (!File.Exists(psExecPath))
                    throw new FileNotFoundException("Cannot file the PS exec file: " + psExecPath);

                //string psKillPath = Path.Combine(value, "pskill.exe");

                //if (!File.Exists(psKillPath))
                //    throw new FileNotFoundException("Cannot file the PS kill file: " + psKillPath);

                _psToolsPath = value;
            }
        }

        /// <summary>
        /// Check if param passed in is a local machine
        /// </summary>
        /// <param name="machineNameOrIpAddress"></param>
        /// <returns>true : local machine, false : Remote Machine</returns>
        public static bool IsLocalMachine(string machineNameOrIpAddress)
        {
            if (machineNameOrIpAddress == "localhost") return true;

            if (machineNameOrIpAddress == null) throw new NullReferenceException("Machine name cannot be null");

            string tmpMachine = machineNameOrIpAddress.ToLower();

            if (tmpMachine == Dns.GetHostName().ToLower()) return true;

            foreach (IPAddress ipaddress in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (tmpMachine == ipaddress.ToString().ToLower()) return true;
            }

            return false;
        }

        /// <summary>
        /// Run InstallUtil in remote machine with specific params
        /// </summary>
        /// <param name="installUtilArguments">CommandLine params</param>
        /// <returns>Status of installation</returns>
        private bool CallRemoteInstallUtil(string installUtilArguments)
        {
            string loginInfo = "\\\\" + _remoteMachine;
            loginInfo += (WsInstallInfo.WsAccountUserName != String.Empty)
                         ? " -u \"" + WsInstallInfo.WsAccountUserName + "\" -p \"" + WsInstallInfo.WsAccountPassword + "\""
                         : "";

            Process proc = new Process();
            proc.StartInfo.FileName = Path.Combine(PsToolsPath, "psexec.exe");
            proc.StartInfo.Arguments = loginInfo + " \"" + Path.Combine(InstallUtilPath, "installutil.exe") + "\" " + installUtilArguments;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            //proc.StartInfo.RedirectStandardOutput = true; //DONT DO THIS, BECAUSE Windows Service uses this class will hang (psexec doesn't exist)
            proc.StartInfo.UseShellExecute = false;

            //Console.WriteLine(proc.StartInfo.FileName + " " + proc.StartInfo.Arguments);

            proc.Start();
            //string outputResult = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();

            // check result
            if (proc.ExitCode != 0)
            {
                //Console.WriteLine("{0} : failed with code {1}", DateTime.Now, proc.ExitCode);
                //Console.WriteLine(outputResult);
                return false;
            }

            return true;
        }

        public override bool Install(string logFilePath)
        {
            if (IsLocalMachine(_remoteMachine))
            {
                //Call InstallUtil in the same machine
                return base.Install(logFilePath);
            }
            else
            {
                //Call InstallUtil in the remote machine
                string installUtilArguments = GenerateInstallutilInstallArgs(logFilePath);
                return CallRemoteInstallUtil(installUtilArguments);
            }
        }

        public override bool Uninstall(string logFilePath)
        {
            if (IsLocalMachine(_remoteMachine))
            {
                //Call InstallUtil in the same machine
                return base.Uninstall(logFilePath);
            }
            else
            {
                //Call InstallUtil in the remote machine
                string installUtilArguments = GenerateInstallutilUninstallArgs(logFilePath);
                return CallRemoteInstallUtil(installUtilArguments);
            }
        }
    }

}