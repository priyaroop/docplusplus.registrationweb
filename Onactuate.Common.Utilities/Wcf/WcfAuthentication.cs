using System.Security.Authentication;
using System.Security.Principal;
using System.Threading;

namespace Onactuate.Common.Utilities.Wcf
{
    public static class WcfAuthentication
    {
        /// <summary>
        /// Gets the authenticated caller for the current thread.
        /// </summary>
        /// <returns>The authenticated caller's name</returns>
        public static string AssertAuthenticatedUser()
        {
            //Adapted from Programming Wcf Service by Juval Lowy page 485

            //Get the identity from the thread current principal rather than WindowsIdentity.GetCurrent()
            //because we are not impersonating. The WindowsIdentity.GetCurrent() will just tell us the
            //name of the account running the service.
            IPrincipal threadIdent = Thread.CurrentPrincipal;
            if (threadIdent == null || !threadIdent.Identity.IsAuthenticated)
            {
                throw new AuthenticationException("Could not authenticate user");
            }

            return threadIdent.Identity.Name;
        }
    }
}