using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace Onactuate.Common.Utilities.Wcf
{
    public class WcfClient<T> where T : class
    {
        /// <summary>
        /// A cache of ChannelFactories, stored by "Environment.Token". 
        /// Channel Factories are heavyweight, so they need to be cached globally as much as possible.
        /// Since it's hashed on the environment and the token, you could
        /// simultaniously access multiple services using the same interface.
        /// </summary>
        protected static readonly Dictionary<string, ChannelFactory<T>> ChannelFactoryCache =
            new Dictionary<string, ChannelFactory<T>>();

        protected static readonly object ChannelLock = new object();
        protected readonly object CallbackInstance;

        private readonly string _configToken;
        private readonly string _environment;
        protected TimeSpan _timeOut = new TimeSpan(0, 1, 0); // default of 1 minute

        private IWcfConfiguration Config;

        public WcfClient()
            : this(WcfConfigurationBase.GetConfigToken(typeof(T)))
        {}

        public WcfClient(string configToken)
            : this(configToken, WcfConfigurationBase.GetDefaultEnvironment())
        {}

        public WcfClient(string configToken, string environment)
            : this(configToken, environment, null)
        {}

        public WcfClient(object callbackInstance)
            : this(WcfConfigurationBase.GetConfigToken(typeof(T)))
        {}

        public WcfClient(string configToken, object callbackInstance)
            : this(configToken, WcfConfigurationBase.GetDefaultEnvironment())
        {}

        public WcfClient(string configToken, string environment, object callbackInstance)
        {
            _configToken = configToken;
            _environment = environment;
            CallbackInstance = callbackInstance;
            Config = new WcfConfigurationBase(typeof (T), _configToken, _environment);
        }

        public WcfClient(IWcfConfiguration config)
        {
            Config = config;
        }

        public TimeSpan TimeOut
        {
            get { return _timeOut; }
            set { _timeOut = value; }
        }

        /// <summary>
        /// The ChannelFactory is an alternative to svcutil.exe or using vs to generate client libraries.
        /// The benifits of this system are:
        ///     + The actual C# interface used by the service is distributed with the data contracts, preserving xml docs
        ///     + Client Configutation is stored in a centralized place and can be updated for every client at the same time
        ///     + Even after the changes to the interface or data contracts, there is no need to regenerate the code
        ///     + There have been issues with the built in .Net 3.0 ChannelFactory caching - by explicitly doing it, we avoid this.
        /// </summary>
        protected ChannelFactory<T> ChannelFactory
        {
            get
            {
                string tokenHash = WcfConfigurationBase.GetTokenHash(_configToken, _environment);
                ChannelFactory<T> result;
                if (ChannelFactoryCache.TryGetValue(tokenHash, out result))
                    return result;

                lock (ChannelLock)
                {
                    if (ChannelFactoryCache.TryGetValue(tokenHash, out result))
                        return result;

                    ServiceEndpoint mainEndpoint = Config.GetEndpoints().First();
                    ForceFallbackToNtlmAuth(mainEndpoint);

                    result = (CallbackInstance == null)
                                 ? new ChannelFactory<T>(mainEndpoint)
                                 : new DuplexChannelFactory<T>(CallbackInstance, mainEndpoint);

                    ChannelFactoryCache.Add(tokenHash, result);

                    if (Config.SecurityBinding == WcfSecurityBindingType.MutualCertificate)
                    {
                        result.Credentials.ClientCertificate.Certificate = Config.GetClientCert();
                        result.Credentials.ServiceCertificate.DefaultCertificate = Config.GetServerCert();
                        
                        // http://msdn.microsoft.com/en-us/library/system.servicemodel.endpointidentity.creatednsidentity.aspx
                        if (!string.IsNullOrEmpty(Config.GetDNSAlias()))
                        {
                            mainEndpoint.Address = new EndpointAddress(mainEndpoint.Address.Uri,
                                                                       EndpointIdentity.CreateDnsIdentity(
                                                                           Config.GetDNSAlias()));
                        }
                    }
                }
                return result;
            }
        }

        private static void ForceFallbackToNtlmAuth(ServiceEndpoint endpoint)
        {
            // TODO: Check why is this not working when we pass blank string as dns name
            EndpointIdentity fakeIdentity = EndpointIdentity.CreateDnsIdentity("localhost");
            endpoint.Address = new EndpointAddress(endpoint.Address.Uri, fakeIdentity);
        }

        public ChannelProvider<T> CreateChannelProvider()
        {
            ChannelProvider<T> channelProvider = new ChannelProvider<T>(ChannelFactory.CreateChannel());
            channelProvider.InnerChannel.OperationTimeout = _timeOut;

            return channelProvider;
        }

        /// <summary>
        /// Calls the service method and returns the result
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="serviceCall"></param>
        /// <returns></returns>
        public TResult Call<TResult>(Func<T, TResult> serviceCall)
        {
            // this is the preferred method of opening and closing the client... IClientChannel.Dispose() is broken
            // see http://www.iserviceoriented.com/blog/post/Indisposable+WCF_Gotcha+1.aspx
            using (ChannelProvider<T> provider = CreateChannelProvider())
            {
                return serviceCall(provider.Channel);
            }
        }

        /// <summary>
        /// Calls a void service method.
        /// </summary>
        /// <param name="serviceCall"></param>
        public void Call(Action<T> serviceCall)
        {
            // this is the preferred method of opening and closing the client... IClientChannel.Dispose() is broken
            // see http://www.iserviceoriented.com/blog/post/Indisposable+WCF_Gotcha+1.aspx
            using (ChannelProvider<T> provider = CreateChannelProvider())
            {
                serviceCall(provider.Channel);
            }            
        }
    }
}