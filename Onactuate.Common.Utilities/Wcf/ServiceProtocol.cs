namespace Onactuate.Common.Utilities.Wcf
{
    public enum ServiceProtocol
    {
        Tcp,
        Http,
        Https
    }
}