namespace Onactuate.Common.Utilities.Wcf
{
    public enum WcfSecurityBindingType
    {
        None,
        MutualCertificate
    }
}