using System;
using System.ServiceModel;

namespace Onactuate.Common.Utilities.Wcf
{
    public class ChannelProvider<T> : IDisposable
    {
        private readonly T _channel;

        public ChannelProvider(T channel)
        {
            _channel = channel;
        }

        public virtual T Channel
        {
            get { return _channel; }
        }

        public virtual IClientChannel InnerChannel
        {
            get { return (IClientChannel) _channel; }
        }

        #region IDisposable Members

        public virtual void Dispose()
        {
            ICommunicationObject communicationObject = (ICommunicationObject) Channel;
            CommunicationState state = communicationObject.State;

            switch(state)
            {
                case CommunicationState.Faulted:
                    communicationObject.Abort();
                    break;
                case CommunicationState.Closed:
                    break;
                case CommunicationState.Created:
                case CommunicationState.Opened:
                    communicationObject.Close();
                    break;
                default: // Closing, Opening
                    throw new Exception("Disposing communication object in an invalid state " + state);
            }
        }

        #endregion
    }
}