using System.Net;

//using Noemax.WCFX.Channels;
//using Noemax.WCFX.Configuration;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;


namespace Onactuate.Common.Utilities.Wcf
{
    public class WS2007WithIisAuthHttpBinding : WS2007HttpBinding
    {
        public WS2007WithIisAuthHttpBinding(SecurityMode securityMode, bool useCompression, WcfSecurityBindingType securityBindingType)
            : base (securityMode)
        {
            UseCompression = useCompression;
            SecurityBindingType = securityBindingType;
            SetTransportAuthentication();
        }

        public WS2007WithIisAuthHttpBinding(SecurityMode securityMode)
            : this(securityMode, false)
        {}

        public WS2007WithIisAuthHttpBinding(SecurityMode securityMode, bool useCompression)
            : this(securityMode, useCompression, WcfSecurityBindingType.None)
        {}

        public bool UseCompression { get; set; }

        /// <summary>
        /// If not-null, engages the FastInfoSet encoder from Noemax.
        /// </summary>
        //public XmlFastInfosetWriterProfile FastInfoSetEncoding { get; set; }

        public WcfSecurityBindingType SecurityBindingType { get; set; }

        public void SetTransportAuthentication()
        {
            ((HttpTransportBindingElement) GetTransport()).AuthenticationScheme = AuthenticationSchemes.Ntlm;
        }

        public override BindingElementCollection CreateBindingElements()
        {
            BindingElementCollection bindingElements = base.CreateBindingElements();

            if (SecurityBindingType == WcfSecurityBindingType.MutualCertificate)
            {
                AsymmetricSecurityBindingElement secureBindingElement =
                    (AsymmetricSecurityBindingElement)
                    SecurityBindingElement.CreateMutualCertificateBindingElement(
                        MessageSecurityVersion.
                            WSSecurity10WSTrust13WSSecureConversation13WSSecurityPolicy12BasicSecurityProfile10);

                secureBindingElement.DefaultAlgorithmSuite = SecurityAlgorithmSuite.Basic128;
                secureBindingElement.MessageProtectionOrder = MessageProtectionOrder.SignBeforeEncrypt;

                bindingElements[0] = secureBindingElement;
            }

            //if (FastInfoSetEncoding != null)
            //{
            //    FiMessageEncodingBindingElement encodingElement = new FiMessageEncodingBindingElement()
            //                                                          {
            //                                                              //WriterProfile = FastInfoSetEncoding,
            //                                                              MaxVocabularyStringLength = 32
            //                                                          };

            //    bindingElements = encodingElement.PlugIn(bindingElements);
            //}

            //if (UseCompression)
            //{
            //    ContentNegotiationBindingElement contentNegotiation = new ContentNegotiationBindingElement()
            //                                                              {
            //                                                                  CompressionMode = SmartCompressionMode.Pessimistic,
            //                                                                  CompressionLevel = 3,
            //                                                                  CompressionAlgorithms = CompressionAlgorithms.DeflateGZip
            //                                                              };

            //    bindingElements = contentNegotiation.PlugIn(bindingElements);
            //}

            return bindingElements;
        }
    }
}