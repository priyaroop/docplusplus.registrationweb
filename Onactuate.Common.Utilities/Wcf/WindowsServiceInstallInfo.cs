using System;

namespace Onactuate.Common.Utilities.Wcf
{
    /// <summary>
    /// 
    /// </summary>
    public class WindowsServiceInstallInfo
    {
        private string _windowsServiceName;
        private string _wsDescription;
        private readonly string _windowsServicePath;
        private readonly string _windowsServiceAssemblyName;
        private readonly WindowsServiceAccountType _wsAccountType;
        private readonly string _wsAccountUserName = "";
        private readonly string _wsAccountPassword = "";

        /// <summary>
        /// Please pass the windows service information in
        /// </summary>
        /// <param name="windowsServicePath">Path to folder where windows service assembly stored</param>
        /// <param name="windowsServiceAssemblyName">Windows Service Assembly Name</param>
        /// <param name="wsAccountType">Windows Service Account Type (not USER type)</param>
        public WindowsServiceInstallInfo(string windowsServicePath, string windowsServiceAssemblyName, WindowsServiceAccountType wsAccountType)
            : this("", windowsServicePath, windowsServiceAssemblyName, wsAccountType) { }

        /// <summary>
        /// Please pass the windows service information in
        /// </summary>
        /// <param name="windowsServiceName">Name of windows service</param>
        /// <param name="windowsServicePath">Path to folder where windows service assembly stored</param>
        /// <param name="windowsServiceAssemblyName">Windows Service Assembly Name</param>
        /// <param name="wsAccountType">Windows Service Account Type (not USER type)</param>
        public WindowsServiceInstallInfo(string windowsServiceName, string windowsServicePath, string windowsServiceAssemblyName, WindowsServiceAccountType wsAccountType)
            : this(windowsServiceName, "", windowsServicePath, windowsServiceAssemblyName, wsAccountType) { }

        /// <summary>
        /// Please pass the windows service information in
        /// </summary>
        /// <param name="windowsServiceName">Name of windows service</param>
        /// <param name="description">Description of windows service</param>
        /// <param name="windowsServicePath">Path to folder where windows service assembly stored</param>
        /// <param name="windowsServiceAssemblyName">Windows Service Assembly Name</param>
        /// <param name="wsAccountType">Windows Service Account Type (not USER type)</param>
        public WindowsServiceInstallInfo(string windowsServiceName, string description, string windowsServicePath, string windowsServiceAssemblyName, WindowsServiceAccountType wsAccountType)
            : this(windowsServiceName, description, windowsServicePath, windowsServiceAssemblyName, wsAccountType, "", "") { }

        /// <summary>
        /// Please pass the windows service information in
        /// </summary>
        /// <param name="windowsServicePath">Path to folder where windows service assembly stored</param>
        /// <param name="windowsServiceAssemblyName">Windows Service Assembly Name</param>        
        /// <param name="wsAccountUserName">Username of Windows Service when Account Type is USER</param>
        /// <param name="wsAccountPassword">Password of Windows Service when Account Type is USER</param>
        public WindowsServiceInstallInfo(string windowsServicePath, string windowsServiceAssemblyName, string wsAccountUserName, string wsAccountPassword)
            : this("", windowsServicePath, windowsServiceAssemblyName, wsAccountUserName, wsAccountPassword) { }

        /// <summary>
        /// Please pass the windows service information in
        /// </summary>
        /// <param name="windowsServiceName">Name of windows service</param>
        /// <param name="windowsServicePath">Path to folder where windows service assembly stored</param>
        /// <param name="windowsServiceAssemblyName">Windows Service Assembly Name</param>
        /// <param name="wsAccountUserName">Username of Windows Service when Account Type is USER</param>
        /// <param name="wsAccountPassword">Password of Windows Service when Account Type is USER</param>
        public WindowsServiceInstallInfo(string windowsServiceName, string windowsServicePath, string windowsServiceAssemblyName, string wsAccountUserName, string wsAccountPassword)
            : this(windowsServiceName, "", windowsServicePath, windowsServiceAssemblyName, wsAccountUserName, wsAccountPassword) { }

        /// <summary>
        /// Please pass the windows service information in
        /// </summary>
        /// <param name="windowsServiceName">Name of windows service</param>
        /// <param name="description">Description of windows service</param>
        /// <param name="windowsServicePath">Path to folder where windows service assembly stored</param>
        /// <param name="windowsServiceAssemblyName">Windows Service Assembly Name</param>        
        /// <param name="wsAccountUserName">Username of Windows Service when Account Type is USER</param>
        /// <param name="wsAccountPassword">Password of Windows Service when Account Type is USER</param>
        public WindowsServiceInstallInfo(string windowsServiceName, string description, string windowsServicePath, string windowsServiceAssemblyName, string wsAccountUserName, string wsAccountPassword)
            : this(windowsServiceName, description, windowsServicePath, windowsServiceAssemblyName, WindowsServiceAccountType.User, wsAccountUserName, wsAccountPassword) { }

        /// <summary>
        /// Please pass the windows service information in
        /// </summary>
        /// <param name="windowsServiceName">Name of windows service</param>
        /// <param name="description">Description of windows service</param>
        /// <param name="windowsServicePath">Path to folder where windows service assembly stored</param>
        /// <param name="windowsServiceAssemblyName">Windows Service Assembly Name</param>
        /// <param name="wsAccountType">Windows Service Account Type</param>
        /// <param name="wsAccountUserName">Username of Windows Service when Account Type is USER</param>
        /// <param name="wsAccountPassword">Password of Windows Service when Account Type is USER</param>
        private WindowsServiceInstallInfo(string windowsServiceName, string description, string windowsServicePath, string windowsServiceAssemblyName, WindowsServiceAccountType wsAccountType, string wsAccountUserName, string wsAccountPassword)
        {
            _windowsServiceName = windowsServiceName.Trim();
            _wsDescription = description.Trim();
            _windowsServicePath = windowsServicePath;
            _windowsServiceAssemblyName = windowsServiceAssemblyName;
            _wsAccountType = wsAccountType;
            _wsAccountUserName = wsAccountUserName;
            _wsAccountPassword = wsAccountPassword;

            if (_wsAccountType == WindowsServiceAccountType.User && _wsAccountUserName == "")
            {
                throw new Exception("Username has to be provided if AccountType to start the windows service is USER");
            }
        }

        /// <summary>
        /// Name of windows service
        /// </summary>
        public string WindowsServiceName
        {
            get { return _windowsServiceName; }
            set { _windowsServiceName = value; }
        }

        /// <summary>
        /// Description of windows service
        /// </summary>
        public string Description
        {
            get { return _wsDescription; }
            set { _wsDescription = value; }
        }

        /// <summary>
        /// Path to folder which contains windows service binary
        /// </summary>
        public string WindowsServicePath
        {
            get { return _windowsServicePath; }
        }
        /// <summary>
        /// Windows service binary file name
        /// </summary>
        public string WindowsServiceAssemblyName
        {
            get { return _windowsServiceAssemblyName; }
        }
        /// <summary>
        /// Account type to start windows service
        /// </summary>
        public WindowsServiceAccountType WsAccountType
        {
            get { return _wsAccountType; }
        }
        /// <summary>
        /// Username to start windows service (if account type is User)
        /// </summary>
        public string WsAccountUserName
        {
            get { return _wsAccountUserName; }
        }
        /// <summary>
        /// Password of username to start windows service (if account type is User)
        /// </summary>
        public string WsAccountPassword
        {
            get { return _wsAccountPassword; }
        }
    }
}