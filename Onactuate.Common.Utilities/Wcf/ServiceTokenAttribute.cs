using System;

namespace Onactuate.Common.Utilities.Wcf
{
    [AttributeUsage(AttributeTargets.Interface, AllowMultiple = false, Inherited = false)]
    public class ServiceTokenAttribute : Attribute
    {
        private readonly string _token;

        public ServiceTokenAttribute(string token)
        {
            _token = token;
        }

        public string Token
        {
            get { return _token; }
        }
    }
}