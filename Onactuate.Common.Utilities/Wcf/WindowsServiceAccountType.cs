namespace Onactuate.Common.Utilities.Wcf
{
    /// <summary>
    /// 
    /// </summary>
    public enum WindowsServiceAccountType
    {
        /// <summary></summary>
        LocalService,
        /// <summary></summary>
        NetworkService,
        /// <summary></summary>
        LocalSystem,
        /// <summary></summary>
        User
    }
}