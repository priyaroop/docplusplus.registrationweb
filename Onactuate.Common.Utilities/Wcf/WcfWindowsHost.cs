using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;
using log4net;

namespace Onactuate.Common.Utilities.Wcf
{
    /// <summary>
    /// Generic class designed to host WCF services within non-IIS Windows processes
    /// (e.g. in a Windows Service or a Windows Console process).
    /// </summary>
    public class WcfWindowsHost : ServiceBase
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Func<IEnumerable<IWcfService>> _provideServices;
        private readonly ManualResetEvent _stopEvent;
        private readonly object _wcfServiceCreationLock = new object();
        private List<IWcfService> _hostedServicesList;

        public WcfWindowsHost(IEnumerable<IWcfService> services) : this(() => services)
        {}

        public WcfWindowsHost(Func<IEnumerable<IWcfService>> provideServices)
        {
            Log.Info("WcfWindowsHost created");
            ServiceName = "WcfDefaultService";
            _provideServices = provideServices;
            _hostedServicesList = new List<IWcfService>();
            _stopEvent = new ManualResetEvent(false);
        }

        public static void RunAsConsole(params IWcfService[] services)
        {
            RunAsConsole(() => services);
        }

        public static void RunAsService(params IWcfService[] services)
        {
            RunAsService(() => services);
        }

        public static void RunAsConsole(Func<IEnumerable<IWcfService>> provideServices)
        {
            Log.Info("Running service in service console");

            using (WcfWindowsHost host = new WcfWindowsHost(provideServices))
            {
                host.PublishServices();

                Log.Info("All services started and actively listening.");
                Console.WriteLine("Press any key to exit.");
                Console.ReadLine();
                host.Stop();
            }
        }

        public static void RunAsService(Func<IEnumerable<IWcfService>> provideServices)
        {
            Log.Info("Running service in service mode");

            using (WcfWindowsHost host = new WcfWindowsHost(provideServices){CanPauseAndContinue = true})
            {
                Run(host);
            }
        }

        protected override void OnStart(string[] args)
        {
            Log.Info("OnStart method called");
            Start();
        }

        protected override void OnStop()
        {
            Log.Info("OnStop method called");
            Stop();
        }

        protected override void OnPause()
        {
            Log.Info("OnPause method called");
            Stop();
        }

        protected override void OnContinue()
        {
            Log.Info("OnContinue method called");
            Start();
        }

        public new void Stop()
        {
            Log.Info("Sending stop notification...");
            _stopEvent.Set();

            lock (_wcfServiceCreationLock)
            {
                _hostedServicesList.ForEach((IWcfService wcfService) => wcfService.Dispose());
            }
            Log.Info("Stop notification sent.");
        }

        private void Start()
        {
            Log.Info("Starting service thread...");
            Thread serviceThread = new Thread(HostServices) {IsBackground = true};
            serviceThread.Start();
            Log.Info("Service thread started.");
        }

        private void HostServices()
        {
            PublishServices();
            _stopEvent.WaitOne();
        }

        private void PublishServices()
        {
            Log.Info("Creating service list.");
            lock (_wcfServiceCreationLock)
            {
                _hostedServicesList = _provideServices().ToList();
            }

            Log.Info("Publishing service...");
            _hostedServicesList.ForEach((IWcfService wcfService) => wcfService.Open());
            Log.Info("Service published");
        }
    }
}