﻿namespace Onactuate.Common.Utilities.Wcf
{
    public class WcfConstants
    {
        public const string ServiceBaseAddressConfig = "ServiceBaseAddress";
        public const string MexBaseAddressConfig = "MexBaseAddress";
    }
}