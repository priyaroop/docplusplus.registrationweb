using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace Onactuate.Common.Utilities.Wcf
{
    public class WcfService : ServiceHost, IWcfService
    {
        public WcfService(Type contract, ServiceHost serviceHost, string configToken, string environment)
            :this(serviceHost.SingletonInstance ?? serviceHost.Description.ServiceType, configToken, environment)
        {}

        public WcfService(Type implementationClass)
            : this(implementationClass, WcfConfigurationBase.GetConfigToken(implementationClass))
        {}

        public WcfService(Type implementationClass, string configToken)
            : this(implementationClass, configToken, WcfConfigurationBase.GetDefaultEnvironment())
        {}

        public WcfService(Type implementationClass, string configToken, string environment)
            : base(implementationClass)
        {
            ConfigureService(configToken, environment);
        }

        public WcfService(Type implementationClass, string configToken, string environment, IDictionary<string, string> configuration)
            : base(implementationClass)
        {
            ConfigureService(configuration);
        }

        public WcfService(Type implementationClass, IWcfConfiguration config)
            : base(implementationClass)
        {
            ConfigureService(config);
        }

        public WcfService(Type implementationClass, IDictionary<string, string> configuration)
            : base(implementationClass)
        {
            ConfigureService(configuration);
        }

        public WcfService(object singletonServiceInstance, string configToken, string environment)
            : base(singletonServiceInstance)
        {
            ConfigureService(configToken, environment);
        }

        private IWcfConfiguration Config { get; set; }

        #region Configuration

        private void ValidateContract()
        {
            if (ImplementedContracts.Count == 0)
            {
                throw new ServiceActivationException(
                    string.Format(
                        "Service instance type {0} must implement an interface decorated with [ServiceContract]",
                        Description.ServiceType.Name));
            }
        }

        private void CheckForXmlConfig()
        {
            if (Service.Description.Endpoints.Count > 0)
            {
                throw new ServiceActivationException(
                    "Detected auto-configuration of this service from a configuration file, which would conflict with creating the service programatically");
            }
        }

        private void CheckForExplicitNamespaces()
        {
            string contractNamespace = null;

            foreach (ContractDescription contract in Config.ContractDescriptions.Values)
            {
                string thisContractNamespace =
                    Utils.GetAttribute<ServiceContractAttribute>(contract.ContractType).Namespace;

                if (thisContractNamespace == null)
                {
                    throw new ServiceActivationException(
                        string.Format("The service contract namespace must be set on the service {0}",
                                      contract.ContractType));
                }

                if (string.IsNullOrEmpty(contractNamespace))
                {
                    contractNamespace = thisContractNamespace;
                }
                else
                {
                    if (contractNamespace != thisContractNamespace)
                    {
                        throw new ServiceActivationException(
                            string.Format(
                                "Service contract {0} has a namespace of {1}, which does not match the other contracts ({2}). They should match to aid thestandards-compatible generation of the XML-formatted service messages.",
                                contract.ContractType, thisContractNamespace, contractNamespace));
                    }
                }
            }

            ServiceBehaviorAttribute serviceBehavior =
                Utils.GetAttribute<ServiceBehaviorAttribute>(Service.Description.ServiceType);

            if (serviceBehavior == null || serviceBehavior.Namespace != contractNamespace)
            {
                throw new ServiceActivationException(
                    string.Format(
                        "Namespace on the service implementation library must match the namespace on the contract ({0})",
                        contractNamespace));
            }
        }

        private void CheckForCallbackOnHttp()
        {
            if (Config.ServiceProtocol != ServiceProtocol.Tcp
                && Config.ContractDescriptions.Values.Any(c => c.CallbackContractType != null))
            {
                throw new ServiceActivationException("Duplex services are currently only supported on the net.tcp protocol");
            }
        }

        private void ConfigureMainEndpoints()
        {
            foreach (ServiceEndpoint endpoint in Config.GetEndpoints())
            {
                Service.Description.Endpoints.Add(endpoint);
            }
        }

        private void ConfigureMex()
        {
            if (Config.ServiceProtocol == ServiceProtocol.Tcp
                && Config.GetTcpMexUri(Description.Name) == null)
            {
                return;
            }

            ServiceMetadataBehavior mexBehavior = GetOrAddBehavior<ServiceMetadataBehavior>();

            switch(Config.ServiceProtocol)
            {
                case ServiceProtocol.Tcp:
                    if (Config.GetTcpMexUri(Description.Name).Port == Config.GetServiceUri(Description.Name).Port)
                    {
                        throw new ServiceActivationException(
                            "The mex endpoint should be on the same computer as the main NetTcp endpoint, but can't share the same port.");
                    }
                    Service.AddServiceEndpoint(typeof (IMetadataExchange),
                                               MetadataExchangeBindings.CreateMexHttpBinding(),
                                               Config.GetTcpMexUri(Description.Name));
                    break;

                case ServiceProtocol.Http:
                    mexBehavior.HttpGetEnabled = true;
                    mexBehavior.HttpGetUrl = Config.GetServiceUri(Description.Name);
                    break;

                case ServiceProtocol.Https:
                    mexBehavior.HttpsGetEnabled = true;
                    mexBehavior.HttpsGetUrl = Config.GetServiceUri(Description.Name);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private TBehavior GetOrAddBehavior<TBehavior>() where TBehavior : class, IServiceBehavior, new()
        {
            TBehavior behavior = Service.Description.Behaviors.Find<TBehavior>();
            if (behavior != null)
                return behavior;

            behavior = new TBehavior();
            Service.Description.Behaviors.Add(behavior);
            return behavior;
        }

        private void ConfigureDebugBehavior()
        {
            ServiceDebugBehavior debugBehavior = GetOrAddBehavior<ServiceDebugBehavior>();
            debugBehavior.IncludeExceptionDetailInFaults = true;
        }

        private void ConfigureSecurityAuditBehavior()
        {
            ServiceSecurityAuditBehavior behavior = GetOrAddBehavior<ServiceSecurityAuditBehavior>();
            behavior.AuditLogLocation = AuditLogLocation.Application;
            behavior.MessageAuthenticationAuditLevel = AuditLevel.SuccessOrFailure;
            behavior.ServiceAuthorizationAuditLevel = AuditLevel.SuccessOrFailure;
        }

        private void ConfigureThrottlingBehavior()
        {
            ServiceThrottlingBehavior throttle = GetOrAddBehavior<ServiceThrottlingBehavior>();

            throttle.MaxConcurrentCalls = WcfConfigurationBase.MaxConnections;
            throttle.MaxConcurrentInstances = WcfConfigurationBase.MaxConnections;
            throttle.MaxConcurrentSessions = WcfConfigurationBase.MaxConnections;
        }

        private void ConfigureErrorLoggingBehavior()
        {
            WcfHostErrorLogBehavior behavior = GetOrAddBehavior<WcfHostErrorLogBehavior>();
        }

        #endregion

        #region IWcfService Members

        /// <summary>
        /// Exposes a handle to the Wcf Service Host
        /// </summary>
        public ServiceHost Service
        {
            get { return this; }
        }

        public void Dispose()
        {
            if (Service == null)
            {
                return;
            }

            if (Service.State == CommunicationState.Opened)
            {
                Service.Close();
            }
            else
            {
                Service.Abort();
            }
        }

        #endregion

        private void ConfigureService(IWcfConfiguration config)
        {
            ValidateContract();
            Config = config;
            ValidateConfig();
        }

        private void ConfigureService(IDictionary<string, string> configuration)
        {
            ValidateContract();
            Config = new WcfConfigurationBase(ImplementedContracts.Values, configuration);
            ValidateConfig();
        }

        private void ConfigureService(string configToken, string environment)
        {
            ValidateContract();
            Config = new WcfConfigurationBase(ImplementedContracts.Values, configToken, environment);
            ValidateConfig();
        }

        private void ValidateConfig()
        {
            CheckForXmlConfig();
            CheckForExplicitNamespaces();
            CheckForCallbackOnHttp();
            ConfigureMainEndpoints();
            ConfigureMex();
            ConfigureDebugBehavior();
            ConfigureThrottlingBehavior();
            ConfigureErrorLoggingBehavior();
        }
    }
}