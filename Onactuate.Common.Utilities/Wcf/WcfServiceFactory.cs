using System;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace Onactuate.Common.Utilities.Wcf
{
    public class WcfServiceFactory : ServiceHostFactory 
    {
        public ServiceHost CreateServiceHost(Type serviceType)
        {
            return CreateServiceHost(serviceType, new Uri[0]);
        }

        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            WcfService service = new WcfService(serviceType);
            return service.Service;
        }
    }
}