﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Onactuate.Common.Utilities.Wcf;
using RestSharp;
using RestSharp.Deserializers;

namespace Onactuate.Common.Utilities.RESTClient
{
    public class SimpleRESTClient
    {
        private Uri RestServiceBaseUrl { get; }
        private string AuthenticationUsername { get; }
        private string AuthenticationPassword { get; }
        private string GrantType { get; }

        public SimpleRESTClient(Uri restServiceBaseUrl, string authenticationUsername, string authenticationPassword)
        {
            RestServiceBaseUrl = restServiceBaseUrl;
            AuthenticationUsername = authenticationUsername;
            AuthenticationPassword = authenticationPassword;
            GrantType = "password";
        }

        
        /// <summary>
        /// Calls the service method and returns the result
        /// </summary>
        /// <param name="apiMethodName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public T CallHttpGetMethod<T>(string apiMethodName, RequestParameters parameters)
        {
            string token = GetAuthorizationToken();
            string queryString = GetUrlQueryString(parameters);

            Uri methodUri = new Uri(RestServiceBaseUrl, $"api/{apiMethodName}?{queryString}");
            RestClient client = new RestClient(methodUri);
            RestRequest request = new RestRequest(Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + token), ParameterType.HttpHeader);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Error while executing the request \n" + response.Content);
            }

            JsonDeserializer jsonDeserializer = new JsonDeserializer();
            T tokenResponse = jsonDeserializer.Deserialize<T>(response);

            return tokenResponse;
        }

        private string GetUrlQueryString(RequestParameters parameters)
        {
            StringBuilder urlQueryString = new StringBuilder();
            foreach (KeyValuePair<string, string> parameter in parameters.ParameterList)
            {
                urlQueryString.AppendFormat("{0}={1}&", parameter.Key, parameter.Value);
            }

            return urlQueryString.ToString();
        }

        private string GetAuthorizationToken()
        {
            Uri tokenUri = new Uri(RestServiceBaseUrl, "token");
            RestClient client = new RestClient(tokenUri);
            RestRequest request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("application/x-www-form-urlencoded", $"grant_type={GrantType}&Username={AuthenticationUsername}&Password={AuthenticationPassword}", ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Could not obtain Access Token for this request \n" + response.Content);
            }

            JsonDeserializer jsonDeserializer = new JsonDeserializer();
            TokenValidationResponse tokenResponse = jsonDeserializer.Deserialize<TokenValidationResponse>(response);

            return tokenResponse.access_token;
        }
    }
}