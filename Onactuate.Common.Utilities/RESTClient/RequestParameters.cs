﻿using System.Collections;
using System.Collections.Generic;

namespace Onactuate.Common.Utilities.RESTClient
{
    public class RequestParameters
    {
        public IDictionary<string, string> ParameterList { get; private set; }

        public RequestParameters(IDictionary<string, string> parameterList)
        {
            ParameterList = parameterList;
        }
    }
}