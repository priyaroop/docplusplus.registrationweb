﻿namespace Onactuate.Common.Utilities
{
    public interface ICookieStore1<T>
    {
        void ClearAll();
        void ClearCookie(T val);
        T Create(T val);
        T Get(string cookieName);
        void Save(T val);
    }
}

