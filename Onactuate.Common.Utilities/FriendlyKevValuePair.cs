﻿using System;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace Onactuate.Common.Utilities
{
    [Serializable, StructLayout(LayoutKind.Sequential), XmlType(TypeName="Setting")]
    public struct FriendlyKevValuePair<K, V>
    {
        public FriendlyKevValuePair(K key, V value)
        {
            this = new FriendlyKevValuePair<K, V> {Key = key, Value = value};
        }

        public K Key { get; set; }
        public V Value { get; set; }
    }
}

