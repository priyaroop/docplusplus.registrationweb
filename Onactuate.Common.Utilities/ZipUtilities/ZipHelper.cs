﻿using System;
using System.Collections.Generic;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;

namespace Onactuate.Common.Utilities.ZipUtilities
{
    public static class ZipHelper
    {
        /// <summary>
        /// Extracts the files in the zip file
        /// </summary>
        /// <param name="zipFile"></param>
        /// <returns></returns>
        public static void UnzipFile(string zipFile)
        {
            if (!File.Exists(zipFile))
            {
                throw new FileNotFoundException("The zip file does not exist. Please make sure you give the correct file name with full path");
            }

            using (ZipInputStream s = new ZipInputStream(File.OpenRead(zipFile)))
            {
                ZipEntry theEntry;
                while ((theEntry = s.GetNextEntry()) != null)
                {
                    string directoryName = Path.GetDirectoryName(theEntry.Name);
                    string fileName = Path.GetFileName(theEntry.Name);

                    // Create directory
                    if (directoryName.Length > 0)
                    {
                        Directory.CreateDirectory(directoryName);
                    }

                    if (fileName != String.Empty)
                    {
                        using (FileStream streamWriter = File.Create(theEntry.Name))
                        {
                            int size = 2048;
                            byte[] data = new byte[2048];
                            while (true)
                            {
                                size = s.Read(data, 0, data.Length);
                                if (size > 0)
                                {
                                    streamWriter.Write(data, 0, size);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Tries to zip the files in the dirPath and only zips files in the filesToZip list
        /// Returns the name of the generated zip file.
        /// Returns true if successfully able to zip the directory.
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="filesToZip"></param>
        /// <param name="zipFileName"></param>
        /// <returns></returns>
        public static bool TryZipFiles(string dirPath, List<string> filesToZip, out string zipFileName)
        {
            bool isSuccess = true;
            Guid guid = Guid.NewGuid();
            zipFileName = Path.Combine(dirPath, guid + ".zip");

            try
            {
                // Depending on the directory this could be very large and could require more attention
                string[] filenames = Directory.GetFiles(dirPath);

                // 'using' statements guarantee the stream is closed properly which is a big source
                // of problems otherwise.  Its exception safe as well, which is great.
                using (ZipOutputStream s = new ZipOutputStream(File.Create(zipFileName)))
                {
                    s.SetLevel(5); // 0 - store only to 9 - means best compression
                    byte[] buffer = new byte[4096];
                    foreach (string file in filenames)
                    {
                        // Using GetFileName makes the result compatible with XP
                        // as the resulting path is not absolute.
                        ZipEntry entry = new ZipEntry(Path.GetFileName(file));

                        if (filesToZip.Contains(entry.Name))
                        {
                            // Setup the entry data as required.
                            // Crc and size are handled by the library for seakable streams
                            // so no need to do them here.
                            // Could also use the last write time or similar for the file.
                            entry.DateTime = DateTime.Now;
                            s.PutNextEntry(entry);

                            using (FileStream fs = File.OpenRead(file))
                            {
                                // Using a fixed size buffer here makes no noticeable difference for output
                                // but keeps a lid on memory usage.
                                int sourceBytes;
                                do
                                {
                                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                    s.Write(buffer, 0, sourceBytes);
                                } while (sourceBytes > 0);
                            }
                        }
                    }

                    // Finish/Close arent needed strictly as the using statement does this automatically
                    // Finish is important to ensure trailing information for a Zip file is appended.  Without this
                    // the created file would be invalid.
                    s.Finish();

                    // Close is important to wrap things up and unlock the file.
                    s.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception during processing {0}", ex);
                isSuccess = false;
                zipFileName = string.Empty;
            }

            return isSuccess;
        }

        /// <summary>
        /// Tries to zip the files in the dirPath and returns the name of the generated zip file.
        /// Returns true if successfully able to zip the directory.
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="zipFileName"></param>
        /// <returns></returns>
        public static bool TryZipFiles(string dirPath, out string zipFileName)
        {
            bool isSuccess = true;
            Guid guid = Guid.NewGuid();
            zipFileName = Path.Combine(dirPath, guid + ".zip");

            try
            {
                // Depending on the directory this could be very large and could require more attention
                string[] filenames = Directory.GetFiles(dirPath);

                // 'using' statements guarantee the stream is closed properly which is a big source
                // of problems otherwise.  Its exception safe as well, which is great.
                using (ZipOutputStream s = new ZipOutputStream(File.Create(zipFileName)))
                {
                    s.SetLevel(5); // 0 - store only to 9 - means best compression
                    byte[] buffer = new byte[4096];
                    foreach (string file in filenames)
                    {
                        // Using GetFileName makes the result compatible with XP
                        // as the resulting path is not absolute.
                        ZipEntry entry = new ZipEntry(Path.GetFileName(file));

                        // Setup the entry data as required.
                        // Crc and size are handled by the library for seakable streams
                        // so no need to do them here.
                        // Could also use the last write time or similar for the file.
                        entry.DateTime = DateTime.Now;
                        s.PutNextEntry(entry);

                        using (FileStream fs = File.OpenRead(file))
                        {
                            // Using a fixed size buffer here makes no noticeable difference for output
                            // but keeps a lid on memory usage.
                            int sourceBytes;
                            do
                            {
                                sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                s.Write(buffer, 0, sourceBytes);
                            } while (sourceBytes > 0);
                        }
                    }

                    // Finish/Close arent needed strictly as the using statement does this automatically
                    // Finish is important to ensure trailing information for a Zip file is appended.  Without this
                    // the created file would be invalid.
                    s.Finish();

                    // Close is important to wrap things up and unlock the file.
                    s.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception during processing {0}", ex);
                isSuccess = false;
                zipFileName = string.Empty;
            }

            return isSuccess;
        }
    }
}
