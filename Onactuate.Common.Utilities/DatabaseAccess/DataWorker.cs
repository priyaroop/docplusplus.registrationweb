﻿using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Onactuate.Common.Utilities.DatabaseAccess
{
    public class DataWorker
    {
        private static Database _database;

        public DataWorker(DbaseType dbaseType, string connectionString)
        {
            _database = DbFactory.CreateDatabase(dbaseType, connectionString);
        }

        public static Database database
        {
            get { return _database; }
        }
    }
}