﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Onactuate.Common.Configuration;

namespace Onactuate.Common.Utilities.DatabaseAccess
{
    public sealed class DbFactory
    {
        private DbFactory() { }

        public static Database CreateDatabase(DbaseType dbaseType, string connectionString)
        {
            try
            {
                // Find the database class
                Type database = null;
                switch(dbaseType)
                {
                    case DbaseType.SqlDatabase:
                        database = typeof(Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase);
                        break;
                    case DbaseType.OracleDatabase:
                        database = typeof(Microsoft.Practices.EnterpriseLibrary.Data.Oracle.OracleDatabase);
                        break;
                }               

                // Get it's constructor (with one string - connectionstring parameter)
                ConstructorInfo constructor = database.GetConstructor(new Type[] { typeof(string) });

                // Invoke it's constructor, which returns an instance.
                List<string> parameters = new List<string> {connectionString};
                Database createdObject = (Database)constructor.Invoke(parameters.ToArray());

                // Pass back the instance as a Database
                return createdObject;
            }
            catch (Exception excep)
            {
                throw new Exception(string.Format("Error instantiating database {0}. {1}", dbaseType, excep.Message));
            }
        }

        public static Database CreateDatabase(string databaseToken)
        {
            // Try reading the config values from the configuration database
            IConfigurationProvider configurationProvider = new ConfigurationProviderFactory().GetConfigurationProvider();
            IDictionary<string, string> properties = configurationProvider.GetProperties(databaseToken);

            string connectionString = GetConnectionString(properties);
            DbaseType dbaseType = (DbaseType)Enum.Parse(typeof(DbaseType), properties[DbConstants.DatabaseTypeKeyName]);

            return CreateDatabase(dbaseType, connectionString);
        }

        private static string GetConnectionString(IDictionary<string, string> configProperties)
        {
            StringBuilder connString = new StringBuilder();
            if (configProperties.ContainsKey(DbConstants.DatabaseTypeKeyName))
            {
                string dbaseType = configProperties[DbConstants.DatabaseTypeKeyName];
                switch (dbaseType)
                {
                    case "OracleDatabase":
                        // Build the Oracle connection string
                        if (configProperties.ContainsKey(DbConstants.DatabaseServerKeyName))
                        {
                            if (configProperties.ContainsKey(DbConstants.DatabaseDbNameKeyName))
                            {
                                connString.AppendFormat("Data Source={0}.{1};"
                                                        , configProperties[DbConstants.DatabaseServerKeyName]
                                                        , configProperties[DbConstants.DatabaseDbNameKeyName]);

                            }
                            else
                            {
                                connString.AppendFormat("Data Source={0};"
                                                        , configProperties[DbConstants.DatabaseServerKeyName]);
                            }
                        }
                        else
                        {
                            throw new Exception("Database configuration error. Please provide Server and DbaseType key values");
                        }

                        if (configProperties.ContainsKey(DbConstants.DatabaseUsernameKeyName)
                            && configProperties.ContainsKey(DbConstants.DatabasePasswordKeyName))
                        {
                            connString.AppendFormat("User ID={0};Password={1};"
                                                    , configProperties[DbConstants.DatabaseUsernameKeyName]
                                                    , configProperties[DbConstants.DatabasePasswordKeyName]);
                        }
                        else
                        {
                            connString.AppendFormat("Integrated Security=Yes;");
                        }
                        break;

                    case "SqlDatabase":
                        // Build the Sql Server connection string
                        if (configProperties.ContainsKey(DbConstants.DatabaseServerKeyName)
                            && configProperties.ContainsKey(DbConstants.DatabaseDbNameKeyName))
                        {
                            connString.AppendFormat("Data Source={0};Initial Catalog={1};"
                                                    , configProperties[DbConstants.DatabaseServerKeyName]
                                                    , configProperties[DbConstants.DatabaseDbNameKeyName]);
                        }
                        else
                        {
                            throw new Exception("Database configuration error. Please provide Server, Database and DbaseType key values");
                        }

                        if (configProperties.ContainsKey(DbConstants.DatabaseUsernameKeyName)
                            && configProperties.ContainsKey(DbConstants.DatabasePasswordKeyName))
                        {
                            connString.AppendFormat("User ID={0};Password={1};"
                                                    , configProperties[DbConstants.DatabaseUsernameKeyName]
                                                    , configProperties[DbConstants.DatabasePasswordKeyName]);
                        }
                        else
                        {
                            connString.AppendFormat("Integrated Security=SSPI;");
                        }
                        break;

                    default:
                        throw new Exception(string.Format("Database configuration error. Incorrect database type specified - {0}.", dbaseType));
                }
            }
            else
            {
                throw new Exception("Database configuration error. Must provide DbaseType key values");
            }

            return connString.ToString();
        }
    }
}