﻿namespace Onactuate.Common.Utilities.DatabaseAccess
{
    public class DbConstants
    {
        public const string DatabaseServerKeyName = "Server";
        public const string DatabaseDbNameKeyName = "Database";
        public const string DatabaseTypeKeyName = "DbaseType";
        public const string DatabaseUsernameKeyName = "Username";
        public const string DatabasePasswordKeyName = "Password";

    }
}