namespace Onactuate.Common.Utilities.Lektor
{
    public enum PaddingMode
    {
        None,
        PKCS7,
        Zeros,
        ANSIX923,
        ISO10126
    }
}