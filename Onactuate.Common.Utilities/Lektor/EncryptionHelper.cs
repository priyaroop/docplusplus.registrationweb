﻿using System.Configuration;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Onactuate.Common.Utilities.Lektor
{
    public static class EncryptionHelper
    {
        private const string PrivateKeyBackup = "87E65B3DAFE970AF49528DF38D082AFE592D7F6C804D1690E4ADA1531B6DEAA1CB2C7D3B55E2D3CCA58A8B3C638D96C6BB5D0B4E501B48B7C3F9865207FCA5A8";

        /// <summary>
        /// Method to encrypt string data.
        /// </summary>
        /// <param name="dataToEncrypt"></param>
        /// <returns></returns>
        public static byte[] EncryptText(string dataToEncrypt)
        {
            string privateKey = PrivateKeyBackup;
            string environment = ConfigurationManager.AppSettings["Environment"];
            XDocument xdoc = XDocument.Load(RuntimeEnvironment.SystemConfigurationFile);
            var element = xdoc.XPathSelectElement(string.Format("//Trafigura.Encryption/add[@key='{0}']", environment));
            if (element != null)
                privateKey = (string)element.Attribute("value");

            byte[] dataBytes = Encoding.ASCII.GetBytes(dataToEncrypt);
            byte[] encryptedPassword = CryptoHelper.EncryptData(privateKey, AutoSaltSizes.Salt64, dataBytes,
                SymmetricCryptoAlgorithm.AES_256_CBC);
            return encryptedPassword;
        }

        /// <summary>
        /// Method to decrypt the byte array data and return the decrypted text string
        /// </summary>
        /// <param name="dataToDecrypt"></param>
        /// <returns></returns>
        public static string DecryptData(byte[] dataToDecrypt)
        {
            string privateKey = PrivateKeyBackup;
            string environment = ConfigurationManager.AppSettings["Environment"];
            XDocument xdoc = XDocument.Load(RuntimeEnvironment.SystemConfigurationFile);
            var element = xdoc.XPathSelectElement(string.Format("//Trafigura.Encryption/add[@key='{0}']", environment));
            if (element != null)
                privateKey = (string)element.Attribute("value");

            byte[] decryptedPasswordAutoSalt = CryptoHelper.DecryptData(privateKey, AutoSaltSizes.Salt64, dataToDecrypt, SymmetricCryptoAlgorithm.AES_256_CBC);
            return Encoding.ASCII.GetString(decryptedPasswordAutoSalt);
        }

        public static void EncryptFile(string filePath)
        {
            byte[] fileBytes = File.ReadAllBytes(filePath);
            string fileContent = Encoding.ASCII.GetString(fileBytes);
            byte[] encryptedText = EncryptText(fileContent);

            File.WriteAllBytes(filePath, encryptedText);
        }

        public static byte[] DecryptFile(string filePath)
        {
            byte[] fileContents = ReadFile(filePath);
            string decryptedText = DecryptData(fileContents);
            byte[] toBytes = Encoding.ASCII.GetBytes(decryptedText);

            return toBytes;
        }

        private static byte[] ReadFile(string filePath)
        {
            byte[] buffer;
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            try
            {
                int length = (int)fileStream.Length;  // get file length
                buffer = new byte[length];            // create buffer
                int count;                            // actual number of bytes read
                int sum = 0;                          // total number of bytes read

                // read until Read method returns 0 (end of the stream has been reached)
                while ((count = fileStream.Read(buffer, sum, length - sum)) > 0)
                    sum += count;  // sum is a buffer offset for next reading
            }
            finally
            {
                fileStream.Close();
            }
            return buffer;
        }

    }
}