﻿using System.Globalization;
using System.Linq;

namespace Onactuate.Common.Utilities.FormatData
{
    public static class FormatDataExtensions
    {
        public static string AsFormmatedCurrency(this double value, string currency)
        {
            int num = Enumerable.SkipWhile<char>(value.ToString(), c => c != '.').Skip<char>(1).Count<char>();
            string format = "C" + ((num < 2) ? 2 : num);
            return value.ToString(format, new CultureInfo(getCulture(currency)));
        }

        public static string AsFormmatedCurrencySummary(this double value, string currency)
        {
            return (value.ToString("C2", new CultureInfo(getCulture(currency))) + " " + currency);
        }

        private static string getCulture(string currency)
        {
            string str = "en-US";
            string str3 = currency.ToUpper();
            if (str3 == null)
            {
                return str;
            }
            if (!(str3 == "USD"))
            {
                if (str3 != "GBP")
                {
                    if (str3 != "EUR")
                    {
                        return str;
                    }
                    return "fr-FR";
                }
            }
            else
            {
                return "en-US";
            }
            return "en-GB";
        }
    }
}

