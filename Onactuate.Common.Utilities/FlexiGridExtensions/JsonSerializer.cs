﻿using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Onactuate.Common.Utilities.FlexiGridExtensions
{
    public class JsonSerializer
    {
        public static string ToJsonObject(object obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            using (MemoryStream stream = new MemoryStream())
            {
                serializer.WriteObject(stream, obj);
                StringBuilder builder = new StringBuilder();
                builder.Append(Encoding.UTF8.GetString(stream.ToArray()));
                return builder.ToString();
            }
        }
    }
}

