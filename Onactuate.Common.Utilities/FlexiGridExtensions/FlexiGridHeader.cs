﻿namespace Onactuate.Common.Utilities.FlexiGridExtensions
{
    public class FlexiGridHeader
    {
        public FlexiGridHeader()
        {
            this.HeaderDisplay = string.Empty;
            this.ColumnName = string.Empty;
            this.ColumnWidth = 50;
            this.IsSortable = false;
            this.IsHidden = false;
            this.HeaderTextAlign = "left";
            this.TextAlign = "left";
            this.HeaderStyle = "";
            this.ColumnDataType = FlexiGridDataType.FlexiGridString;
        }

        public FlexiGridHeader(string headerDisplay, string columnName, bool isHidden)
        {
            this.HeaderDisplay = headerDisplay;
            this.ColumnName = columnName;
            this.IsHidden = isHidden;
            this.ColumnWidth = 0;
            this.IsSortable = false;
            this.HeaderTextAlign = string.Empty;
            this.TextAlign = string.Empty;
            this.HeaderStyle = string.Empty;
            this.ColumnDataType = FlexiGridDataType.FlexiGridString;
        }

        public FlexiGridHeader(string headerDisplay, string columnName, int columnWidth, bool isSortable, bool isHidden, string headerTextAlign, string textAlign, string headerStyle)
        {
            this.HeaderDisplay = headerDisplay;
            this.ColumnName = columnName;
            this.ColumnWidth = columnWidth;
            this.IsSortable = isSortable;
            this.IsHidden = isHidden;
            this.HeaderTextAlign = headerTextAlign;
            this.TextAlign = textAlign;
            this.HeaderStyle = headerStyle;
        }

        public FlexiGridHeader(string headerDisplay, string columnName, int columnWidth, bool isSortable, bool isHidden, string headerTextAlign, string textAlign, string headerStyle, FlexiGridDataType columnDataType)
        {
            this.HeaderDisplay = headerDisplay;
            this.ColumnName = columnName;
            this.ColumnWidth = columnWidth;
            this.IsSortable = isSortable;
            this.IsHidden = isHidden;
            this.HeaderTextAlign = headerTextAlign;
            this.TextAlign = textAlign;
            this.HeaderStyle = headerStyle;
            this.ColumnDataType = columnDataType;
        }

        public override string ToString()
        {
            return string.Format("{{ display: '{0}', name: '{1}', width: {2}, headerTextAlign: '{3}', itextAlign: '{4}', sortable: {5}, headerStyle: '{6}', hide: {7}, dataType: '{8}' }}", new object[] { this.HeaderDisplay, this.ColumnName, this.ColumnWidth, this.HeaderTextAlign, this.TextAlign, this.IsSortable.ToString().ToLower(), this.HeaderStyle, this.IsHidden.ToString().ToLower(), this.ColumnDataType });
        }

        public FlexiGridDataType ColumnDataType { get; set; }

        public string ColumnName { get; set; }

        public int ColumnWidth { get; set; }

        public string HeaderDisplay { get; set; }

        public string HeaderStyle { get; set; }

        public string HeaderTextAlign { get; set; }

        public bool IsHidden { get; set; }

        public bool IsSortable { get; set; }

        public string TextAlign { get; set; }
    }
}

