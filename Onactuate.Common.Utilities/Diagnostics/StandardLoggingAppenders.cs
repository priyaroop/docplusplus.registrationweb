using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Core;
using log4net.Layout;

namespace Onactuate.Common.Utilities.Diagnostics
{
    public class StandardLoggingAppenders
    {
        //TODO: Consider external config for some or all of these
        private const string StandardColoredConsoleAppenderName = "StandardColoredConsoleAppender";
        private const string StandardRollingFileAppenderName = "StandardRollingFileAppender";
        private const string StandardAdoNetAppenderName = "StandardAdoNetAppender";
        private const string StandardErrorMemoryAppenderName = "StandardErrorMemoryAppender";
        private const string StandardMemoryAppenderName = "StandardMemoryAppender";
        private const string StandardPatternLayout = "%date %-5level %message %exception %newline";
        private static string _fileLoggingBasePath = @"C:\LogFiles\";
        private const string TextFileExtension = ".txt";

        #region Public Static Properties

        public static ColoredConsoleAppender StandardColoredConsoleAppender
        {
            get
            {
                return
                    (ColoredConsoleAppender)
                    LogManager.GetRepository().GetAppenders().FirstOrDefault(
                        l => l.Name == StandardColoredConsoleAppenderName);
            }
        }

        public static RollingFileAppender StandardRollingFileAppender
        {
            get
            {
                return
                    (RollingFileAppender)
                    LogManager.GetRepository().GetAppenders().FirstOrDefault(
                        l => l.Name == StandardRollingFileAppenderName);
            }
        }

        public static AdoNetAppender StandardAdoNetAppender
        {
            get
            {
                return
                    (AdoNetAppender)
                    LogManager.GetRepository().GetAppenders().FirstOrDefault(
                        l => l.Name == StandardAdoNetAppenderName);
            }
        }

        public static MemoryAppender StandardErrorMemoryAppender
        {
            get
            {
                return
                    (MemoryAppender)
                    LogManager.GetRepository().GetAppenders().FirstOrDefault(
                        l => l.Name == StandardErrorMemoryAppenderName);
            }
        }

        public static MemoryAppender StandardMemoryAppender
        {
            get
            {
                return
                    (MemoryAppender)
                    LogManager.GetRepository().GetAppenders().FirstOrDefault(
                        l => l.Name == StandardMemoryAppenderName);
            }
        }

        #endregion

        #region Public Static Methods

        public static ColoredConsoleAppender CreateStandardColoredConsoleAppender()
        {
            //Note: We are not allowing multiple appenders with the same name even if log4net is allowing this
            if (StandardColoredConsoleAppender != null)
            {
                throw new InvalidOperationException("Unable to create StandardColoredConsoleAppender - it already exists!");
            }

            ColoredConsoleAppender coloredConsoleAppender = new ColoredConsoleAppender
                                                                {
                                                                    Name = StandardColoredConsoleAppenderName,
                                                                    Threshold = Level.All,
                                                                    Layout = new PatternLayout(StandardPatternLayout)
                                                                };

            //setup mappings (formatting for each log level)
            coloredConsoleAppender.AddMapping(new ColoredConsoleAppender.LevelColors
                                                  {
                                                      Level = Level.Fatal,
                                                      ForeColor = ColoredConsoleAppender.Colors.Purple |
                                                                  ColoredConsoleAppender.Colors.HighIntensity
                                                  });

            coloredConsoleAppender.AddMapping(new ColoredConsoleAppender.LevelColors
                                                  {
                                                      Level = Level.Error,
                                                      ForeColor = ColoredConsoleAppender.Colors.Red |
                                                                  ColoredConsoleAppender.Colors.HighIntensity
                                                  });

            coloredConsoleAppender.AddMapping(new ColoredConsoleAppender.LevelColors
                                                  {
                                                      Level = Level.Warn,
                                                      ForeColor = ColoredConsoleAppender.Colors.Yellow |
                                                                  ColoredConsoleAppender.Colors.HighIntensity
                                                  });

            coloredConsoleAppender.AddMapping(new ColoredConsoleAppender.LevelColors
                                                  {
                                                      Level = Level.Info,
                                                      ForeColor = ColoredConsoleAppender.Colors.White |
                                                                  ColoredConsoleAppender.Colors.HighIntensity
                                                  });

            coloredConsoleAppender.AddMapping(new ColoredConsoleAppender.LevelColors
                                                  {
                                                      Level = Level.Debug,
                                                      ForeColor = ColoredConsoleAppender.Colors.White
                                                  });

            // log4net requires these to activate the appenders (TODO: consider doing this outside of this method for proper separation of concerns?)
            coloredConsoleAppender.ActivateOptions();
            BasicConfigurator.Configure(coloredConsoleAppender);

            return coloredConsoleAppender;
        }

        public static RollingFileAppender CreateStandardRollingFileAppender(string fileLoggingBasePath)
        {
            _fileLoggingBasePath = fileLoggingBasePath;
            return CreateStandardRollingFileAppender();
        }

        public static RollingFileAppender CreateStandardRollingFileAppender()
        {
            //Note: We are not allowing multiple appenders with the same name even if log4net is allowing this
            if (StandardRollingFileAppender != null)
            {
                throw new InvalidOperationException("Unable to create StandardRollingFileAppender - it already exists!");
            }

            RollingFileAppender rollingFileAppender = new RollingFileAppender
                                                          {
                                                              Name = StandardRollingFileAppenderName,
                                                              File = Path.Combine(_fileLoggingBasePath, Process.GetCurrentProcess().MainModule+TextFileExtension),
                                                              LockingModel = new FileAppender.MinimalLock(),
                                                              AppendToFile = true,
                                                              RollingStyle = RollingFileAppender.RollingMode.Size,
                                                              MaxSizeRollBackups = 100,
                                                              MaximumFileSize = "5000KB",
                                                              StaticLogFileName = true,
                                                              Layout = new PatternLayout(StandardPatternLayout)
                                                          };

            // log4net requires these to activate the appenders (TODO: consider doing this outside of this method for proper separation of concerns?)
            rollingFileAppender.ActivateOptions();
            BasicConfigurator.Configure(rollingFileAppender);

            return rollingFileAppender;
        }

        public static MemoryAppender CreateStandardErrorMemoryAppender()
        {
            //Note: We are not allowing multiple appenders with the same name even if log4net is allowing this
            if (StandardErrorMemoryAppender != null)
            {
                throw new InvalidOperationException("Unable to create StandardErrorMemoryAppender - it already exists!");
            }

            MemoryAppender memoryAppender = new MemoryAppender
            {
                Name = StandardErrorMemoryAppenderName,
                Threshold = Level.Error
            };

            // log4net requires these to activate the appenders (TODO: consider doing this outside of this method for proper separation of concerns?)
            memoryAppender.ActivateOptions();
            BasicConfigurator.Configure(memoryAppender);

            return memoryAppender;
        }


        public static MemoryAppender CreateStandardMemoryAppender()
        {
            //Note: We are not allowing multiple appenders with the same name even if log4net is allowing this
            if (StandardMemoryAppender != null)
            {
                throw new InvalidOperationException("Unable to create StandardMemoryAppender - it already exists!");
            }

            MemoryAppender memoryAppender = new MemoryAppender
            {
                Name = StandardMemoryAppenderName,
                Threshold = Level.All
            };

            // log4net requires these to activate the appenders (TODO: consider doing this outside of this method for proper separation of concerns?)
            memoryAppender.ActivateOptions();
            BasicConfigurator.Configure(memoryAppender);

            return memoryAppender;
        }

        public static void ThrowIfErrorsExists()
        {
            if (StandardErrorMemoryAppender == null)
            {
                throw new InvalidOperationException("Unable to access StandardErrorMemoryAppender - it does not exist!");
            }

            LoggingEvent[] events = StandardErrorMemoryAppender.GetEvents();
            if (events.Length > 0)
            {
                string[] errors = events.Select(e => e.GetLoggingEventData().Message).ToArray();
                string formattedErrors = events.Length + " errors encountered - details: " + string.Join("----", errors);
                throw new Exception(formattedErrors);
            }
        }

        public static AdoNetAppender CreateStandardAdoNetAppender(string databaseConnectionString, string loggingSprocName, Level loggingThresholdLevel)
        {
            AdoNetAppender adoNetAppender = CreateStandardAdoNetAppender(databaseConnectionString, loggingSprocName);
            adoNetAppender.Threshold = loggingThresholdLevel;
            return adoNetAppender;
        }

        public static AdoNetAppender CreateStandardAdoNetAppender(string databaseConnectionString, string loggingSprocName)
        {
            //Note: We are not allowing multiple appenders with the same name even if log4net is allowing this
            if (StandardAdoNetAppender != null)
            {
                throw new InvalidOperationException("Unable to create StandardAdoNetAppender - it already exists!");
            }

            AdoNetAppender adoNetAppender = new AdoNetAppender
                                                {
                                                    Name = StandardAdoNetAppenderName,
                                                    ConnectionType = "System.Data.SqlClient.Connection, System.Data, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089",
                                                    BufferSize = 1,
                                                    UseTransactions = false,
                                                    CommandType = CommandType.StoredProcedure,
                                                    CommandText = loggingSprocName,
                                                    ConnectionString = databaseConnectionString,
                                                    ReconnectOnError = true
                                                };

            //Add standard parameters
            //Level
            adoNetAppender.AddParameter(new AdoNetAppenderParameterSupportsNull
                                            {
                                                DbType = DbType.String,
                                                ParameterName = "@Level",
                                                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%level"))
                                            });

            //Date
            adoNetAppender.AddParameter(new AdoNetAppenderParameterSupportsNull
                                            {
                                                DbType = DbType.DateTime,
                                                ParameterName = "@Date",
                                                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%date{yyyy'-'MM'-'dd HH':'mm':'ss'.'fff}"))
                                            });

            //Thread
            adoNetAppender.AddParameter(new AdoNetAppenderParameterSupportsNull
                                            {
                                                DbType = DbType.String,
                                                ParameterName = "@Thread",
                                                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%thread"))
                                            });

            //Logger
            adoNetAppender.AddParameter(new AdoNetAppenderParameterSupportsNull
                                            {
                                                DbType = DbType.String,
                                                ParameterName = "@Logger",
                                                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%logger"))
                                            });

            //Message
            adoNetAppender.AddParameter(new AdoNetAppenderParameterSupportsNull
            {
                DbType = DbType.String,
                ParameterName = "@Message",
                Layout = new Layout2RawLayoutAdapter(new PatternLayout("%message"))
            });

            // log4net requires these to activate the appenders (TODO: consider doing this outside of this method for proper separation of concerns?)
            adoNetAppender.ActivateOptions();
            BasicConfigurator.Configure(adoNetAppender);

            return adoNetAppender;
        }

        #endregion
    }
}