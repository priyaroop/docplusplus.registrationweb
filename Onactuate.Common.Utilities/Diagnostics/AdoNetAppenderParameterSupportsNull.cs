using System;
using System.Data;
using log4net.Appender;
using log4net.Core;
using log4net.Util;

namespace Onactuate.Common.Utilities.Diagnostics
{
    public class AdoNetAppenderParameterSupportsNull : AdoNetAppenderParameter
    {
        public override void FormatValue(IDbCommand command, LoggingEvent loggingEvent)
        {
            IDbDataParameter param = (IDbDataParameter) command.Parameters[ParameterName];

            object formattedValue = Layout.Format(loggingEvent);

            if ((formattedValue == null) || (formattedValue.ToString() == SystemInfo.NullText))
            {
                formattedValue = DBNull.Value;
            }

            param.Value = formattedValue;
        }
    }
}